<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Ajax extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //if (!$this->session->userdata('login')) {
        //redirect(base_url().'main','refresh');
        //}else {
        $this->load->model('M_material');
        $this->load->model('M_almacen');
        $this->load->model('M_patinador');
        $this->load->model('M_admin');
        //}
    }
    public function obtener_excel()
    {
        $this->load->model('M_produccion');
        $this->load->helper('mysql_to_excel_helper');

        to_excel($this->M_produccion->get(), "Prueba Excel");
    }

    public function buscar_item()
    {
        $mat_id = $this->input->get_post('query');

        // echo $mat_id;

        try {
            $result = $this->M_patinador->buscar($mat_id);

            // echo $result;

            if ($result) {
                $descripcion = $result->row();

                echo $descripcion->material_descripcion;
            }

        } catch (Exception $e) {
            echo "Error";
        }

    }
    public function buscar_codigo()
    {
        $codigo = $this->input->get_post('query');

        //echo $mat_id;

        try {
            $result = $this->M_patinador->buscar2($codigo);

            if ($result) {
                $datos        = $result->row();
                $presentacion = $datos->presentacion;
                $cantidad     = $datos->sc_cantidad;
                $array        = array(0 => $presentacion, 1 => $cantidad);
                echo json_encode($array);
            }

        } catch (Exception $e) {
            echo "Error";
        }

    }

    public function detail_maquinao()
    {
        $maq_id = $this->input->get_post('maq_id');
        $i      = 0;

        // print_r($maq_id);

        try {
            $res_detail = $this->M_material->detail_maquinao($maq_id);

            print_r($res_detail);

            if ($res_detail) {
                $result = $res_detail->row();

                foreach ($res_detail->result() as $detail) {
                    $i++;
                    ?>
                    <tr>
                        <td><?=$i?></td>


                        <td ><?=$detail->material_id;?></td>
                        <td ><?=$detail->material_descripcion;?></td>
                        <td ><?=$detail->maquinao_cantidad;?></td>
                        <td><?=$detail->maquinao_date;?></td>
                        <td ><?=$detail->maquinao_estado;?></td>
                        <td ><?=$detail->maquinao_codbar;?></td>
                    </tr>
                <?php
}
            } else {}
        } catch (Exception $e) {

        }

    }
    public function detail_ot_seguimiento()
    {
        $i      = 1;
        $codbar = $this->input->post('obj');
        if ($codbar != null) {
            $result = $this->M_material->detail_ot_seguimiento($codbar);
            if ($result != false) {
                foreach ($result->result() as $row) {
                    echo '
            <tr>
                <td>' . $i++ . '</td>
                <td>' . $row->id_detalle_ot . '</td>
                <td id="item">' . $row->item . '</td>
                <td id="descripcion">' . $row->descripcion . '</td>
                <td id="cantidad">' . $row->cantidad_Kgs . ' Kgs</td>
                <td id="fecha">' . $row->fecha_asignacion . '</td>
                <td >' . $row->estado . ' </td>
                <td id="codbar">' . $row->cod_bar . ' </td>
                <td id="codbar">' . $row->maquina . ' </td>

                <td><a href="#editar" data-toggle="tab" ><button class="btn btn-success" data-id=' . $row->Id . ' data-id_detalle_ot=' . $row->id_detalle_ot . ' data-item=' . $row->item . ' data-descripcion=' . $row->descripcion . ' data-cantidad=' . $row->cantidad_Kgs . ' data-fecha=' . $row->fecha_asignacion . ' data-estado=' . $row->estado . '  data-codbar=' . $row->cod_bar . ' data-maquina=' . $row->maquina . '  id="editar_detailot" onclick="editar_detailot(this)">Editar</button></a></td>
            </tr>';
                }
            }

        }
    }
    public function ver_fin_ot()
    {
        $codbar = $this->input->get_post('codbar');

        try {
            $query_ot = $this->M_patinador->ver_fin_ot($codbar);

            if ($query_ot) {
                foreach ($query_ot->result() as $detail) {
                    echo $detail->seccion_nombre . "|" . $detail->material_id . "|" . $detail->material_descripcion . "|" . $detail->maquinao_cantidad . "|" . $detail->maquina_id . "|" . $detail->bodega_id . "|" . $detail->bodega_nombre . "|";

                }
            }
        } catch (Exception $e) {

        }
    }
    public function ver_fin_ot_detail()
    {
        $codbar = $this->input->get_post('codbar');

        try {
            $query_ot = $this->M_patinador->ver_fin_ot($codbar);
            $i        = 0;
            if ($query_ot) {
                foreach ($query_ot->result() as $detail) {
                    $i++;
                    ?>
                    <tr>
                    <td class="hidden"><?=$detail->maquinao_ot;?></td>

                    <td><?=$detail->maquinao_cantidad;?></td>
                    <td><?=$detail->bodega_nombre;?></td>
                    <td> <button type='button' data-seccion_nombre="<?=$detail->seccion_nombre;?>" data-material_id ="<?=$detail->material_id;?>"   data-material_descripcion="<?=$detail->material_descripcion;?>"  data-maquinao_cantidad="<?=$detail->maquinao_cantidad;?>" data-maquinao_ot="<?=$detail->maquinao_ot;?>" data-maquina_id="<?=$detail->maquina_id;?>" data-bodega_id="<?=$detail->bodega_id;?>" class="btn btn-default btn-sm finalizar_ot_detalle" data-id="<?=$detail->maquinao_id;?>" data-bodega_nombre="<?=$detail->bodega_nombre;?>" data-movimiento="<?=$detail->movimiento_id;?>" data-tipo="<?=$detail->tipo;?>">Pasar</button> </td>
                    </tr>
                   <?php

                }
            }
        } catch (Exception $e) {

        }
    }
    public function codbar_pdf(){
        $code_number=$this->input->get('codbar');
        $veces=$this->input->get('veces');
        $params = array(
                'codbar' => $code_number,
                'veces' =>  $veces
        );
        //$this->load->library('Barcode',$params);
        

        $txt_path = "\\\\200.100.9.11\\TestConductores\\";
        $txt_end = ";";
        $txt_sep = "|";
        $txt_file = "BarcodeConductores.txt";
        $txt_data = $code_number;
        $codes = array('codbar' => $code_number );
        
        //$txt_data .= $code_number;

        //Generamos el txt de todos los datos
        if (!$handle = fopen($txt_path.$txt_file, "w")) {
            echo "Cannot open file";
            exit;
        }

        if (fwrite($handle, utf8_decode($txt_data)) === FALSE) {
            echo "Cannot write to file";
            exit;
        }

        //echo $txt_data;

        fclose($handle);
        redirect(base_url().'c_almacen/codigos','refresh');
    }

    public function get_detail_und()
    {

        $movimiento_id = $this->input->get_post('movimiento_id');
        // $codebar=$this->input->get_post('codebar');
        $get_detail_und = $this->M_almacen->get_detail_und($movimiento_id);
        $i              = 1;
        $cantidad       = 0;
        foreach ($get_detail_und[1]->result() as $suma) {
            $cantidad = $suma->total;
        }
        foreach ($get_detail_und[0]->result() as $detail) {
            if ($detail->tipo == '1') {
                ?>
            <tr>
                <td><?=$i++;?></td>
                <td><?=$detail->num_unidad;?></td>
                <td><?=$detail->presentacion;?></td>
                <td><?=$detail->cantidad;?> Kgs</td>
                <td><?=$detail->num_unidad * $detail->cantidad;?> Kgs</td>
                <td><?=$detail->patinador;?></td>
                <td><?=$detail->dm_codbar;?></td>
                <?php
//
            } else {
                ?>
                <tr>
                <td><?=$i++;?></td>
                <td><?=$detail->num_unidad;?></td>
                <td><?=$detail->presentacion;?></td>
                <td><?=$detail->cantidad;?> Uds</td>
                <td><?=$detail->num_unidad * $detail->cantidad;?> Uds</td>
                <td><?=$detail->patinador;?></td>
                <td><?=$detail->dm_codbar;?></td>
                <?php

            }
        }
    }
    public function get_detail_und_detail()
    {
        $bandeja        = $this->input->get_post('bandeja');
        $get_detail_und = $this->M_almacen->get_detail_traslado_bandejas($bandeja);
        $i              = 0;
        foreach ($get_detail_und->result() as $detail) {?>
    <tr>
        <td><?=$i++;?></td>
        <td><?=$bandeja;?></td>
        <td><?=$detail->item;?></td>
        <td><?=$detail->descripcion;?></td>
        <td><?=$detail->bodega;?></td>
        <td><?=$detail->cantidad;?></td>
    </tr>
    <?php
}

    }
    public function get_detail_traslado()
    {
//TRASLADO DESDE LA BOD_PPAL A LA BOD_PROD
        $mat_id = $this->input->get_post('mat_id');

        $get_detail_und = $this->M_almacen->get_detail_traslado($mat_id, 1);

        $i = 1;

        foreach ($get_detail_und->result() as $detail) {?>
            <tr>
                <td><?=$i++;?></td>
                <td><?=$detail->num_unidad;?></td>
                <td><?=$detail->presentacion;?></td>
                <td><?=$detail->cantidad;?> Kgs</td>
                <td class="text-center">
                    <div class="col-sm-6 col-md-7">
                        <input type="number" class="input-md form-control text-center" name="und_nueva[<?=$detail->dm_id;?>]" value="0" required>
                    </div>
                </td>

                <input type="hidden" name="unds_actual[]" value="<?=$detail->num_unidad;?>">
                <input type="hidden" name="cantidad_und[]" value="<?=$detail->cantidad;?>">
                <input type="hidden" name="presentacion[]" value="<?=$detail->presentacion;?>">
            </tr>
        <?php }

    }

    public function get_detail_traslado_mod()
    {
        ?>
        <tr>
            <td>
            <input name = "codigo[]" class="form-control codigo"   required>
            </td>
            <td width="300">
                <input name="presentacion[]" class="form-control presentacion" readonly required>
            </td>
            <td width="70" class="hidden"><input type="number"  name="und_nueva[]" value="1" class="form-control text-center inp_unds " required></td>
            <td width="200"><input type="number" value="0" class="form-control text-center inp_cantidad_disponible" readonly></td>
            <td width="200"><input type="number"  name="cantidad_und[]" value="1" class="form-control text-center inp_cantidad" required></td>
            <td width="200"><label for="" class="form-control text-center lbl_subtotal">0</label></td>
            <td class="text-center"><span title="Quitar" class="btn btn-danger btn-sm remove_detail glyphicon glyphicon-remove"></span></td>
        </tr>

    <?php
}
    public function get_detail_traslado_mod2()
    { //TRASLADO DESDE LA BOD_PPAL A LA BOD_PROD MODIFICADO (14-06-16)
        ?>
        <tr>
            <td>
            <input name = "codigo2[]" class="form-control codigo"   required>
            </td>
            <td width="70" class="hidden"><input type="number" id="1" name="und_nueva[]" value="0" class="form-control text-center inp_unds2 hidden" required></td>

            <td width="200"><label for="" class="form-control text-center lbl_subtotal2">0</label></td>
            <td class="text-center"><span title="Quitar" class="btn btn-danger btn-sm remove_detail2 glyphicon glyphicon-remove"></span></td>
        </tr>
    <?php
}
    public function get_detail_traslado_prod()
    {
        $mat_id          = $this->input->get_post('mat_id');
        $mat_descripcion = $this->input->get_post('mat_descripcion');
        $stock_cantidad  = $this->input->get_post('mat_stock');
        $id              = $this->input->get_post('id');
        $movimiento      = $this->input->get_post('movimiento');
        $get_detail_und  = $this->M_almacen->get_detail_traslado($mat_id, 2, $movimiento);
        $tipo            = $this->input->get_post('tipo');
        $i               = 1;
        foreach ($get_detail_und->result() as $detail) {?>
            <tr>
                <td><input type="checkbox" class="selecionar" name="noselecionado"></td>
                 <td><?=$detail->codbar;?></td>
                <td><?=$detail->presentacion;?></td>
                <td><?=$detail->cantidad;?></td>
                <td class="text-center">
                    <div class="col-sm-12 col-md-12 text-center">
                        <input type="number" class="input-md  form-control text-center" max="1" min="0"  data-valor = "<?=$detail->num_unidad;?>"  id="validar_entrega" name="und_nueva[<?=$detail->dm_id;?>]" value="1" required readonly disabled>
                    </div>
                </td>
                <td><?=$detail->patinador;?></td>
                <input type="hidden" id="input_mat_id" name="mat_id" value="<?=$mat_id;?>" disabled>
                <input type="hidden" id="input_mat_descripcion" name="mat_descripcion" value="<?=$mat_descripcion;?>" disabled>
                <input type="hidden" id="input_stock_cantidad" name="stock_cantidad" value="<?=$stock_cantidad;?>" disabled>
                <input type="hidden" name="unds_actual[]" value="1" disabled>
                <input type="hidden" name="cantidad_und[]" value="<?=$detail->cantidad;?>" disabled>
                <input type="hidden" name="presentacion[]" value="<?=$detail->presentacion;?>" disabled>
                <input type="hidden" name="codbar[]" value="<?=$detail->codbar;?>" disabled>
                <input type="hidden" name="id" value="<?=$id;?>" disabled>
                <?php if ($tipo == 1) {?>
                <input type="hidden" id="tipo" name="tipo" value="1" disabled>
                 <?php
} else {?>
                <input type="hidden" id="tipo" name="tipo" value="2" disabled>
                <?php
}?>
            </tr>
        <?php }
    }

    public function get_detail_reintegro()
    {
        $movimiento           = $this->input->get_post('movimiento');
        $get_detail_reintegro = $this->M_material->get_detail_rein($movimiento);
        foreach ($get_detail_reintegro->result() as $detail) {?>
        <tr>
        <td><input type="checkbox" class="selecionar" name="noselecionado"></td>
        <td><?=$detail->presentacion;?></td>
        <td><?=$detail->cantidad;?></td>
        <td><?=$detail->codbar;?></td>
        <input type="hidden" name="presentacion[]" value="<?=$detail->presentacion?>" disabled>
        <input type="hidden" name="cantidad[]" value="<?=$detail->cantidad?>" disabled>
        <input type="hidden" name="codbar[]" value="<?=$detail->codbar?>" disabled>
        <input type="hidden" name="id[]" value="<?=$detail->dm_id?>" disabled>
        </tr>
        <?php }
    }
    // bombon
    public function fin_ot_send()
    {
        $user_id        = $this->session->userdata('usuario_id');
        $date_actual    = $this->fecha_actual();
        $bod_destino    = 1;
        $mat_cantidad   = 0;
        $get_detail_und = $this->M_patinador->get_detail_mq($codbar);

    }

    public function get_detail_traslado_mq()
    {
//TRASLADO DESDE UNA MAQUINA A OTRA_MAQUINA
        $codbar         = $this->input->get_post('codbar');
        $movimiento     = $this->input->get_post('movimiento');
        $get_detail_und = $this->M_patinador->get_detail_mq($codbar, $movimiento);
        $i              = 1;
        foreach ($get_detail_und->result() as $detail) {?>
            <tr>
                <td><?=$i++;?></td>
                <td><?=$detail->num_unidad;?></td>
                <td><?=$detail->presentacion;?></td>
                <td class="cantidad"><?=$detail->cantidad;?></td>
                <td class="text-center">
                    <div class="col-sm-6 col-md-7 text-center">
                        <input type="number" class="input-md form-control text-center" min="1" max="<?=$detail->num_unidad;?>" name="und_nueva[<?=$detail->dm_id;?>]" value="1" required readonly>
                    </div>
                </td>
                <td class="text-center">
                    <div class="text-center">
                        <input type="number" class="input-md form-control text-center" name="cantidad_und[]" min="1" max="<?=$detail->cantidad;?>" value="0" required>
                    </div>
                </td>
                <input type="hidden" id="input_mat_id" name="mat_id" value="<?=$detail->material_id;?>">
                <input type="hidden" id="input_mat_descripcion" name="mat_descripcion" value="<?=$detail->material_descripcion;?>">
                <input type="hidden" id="input_stock_cantidad" name="stock_cantidad" value="<?=$detail->cantidad;?>">
                <input type="hidden" name="presentacion[]" value="<?=$detail->presentacion;?>">
                <input type="hidden" name="bodega_reintegro_origen" value="<?=$detail->maquina_bod_id;?>">
                <input type="hidden" name="movimiento" value="<?=$movimiento?>">


            </tr>
        <?php }?>
            <input type="hidden" name="codbar" value="<?=$codbar;?>">
        <?php
}
    public function get_detail_traslado_mq2()
    {
//TRASLADO DESDE UNA MAQUINA A OTRA_MAQUINA
        $codbar         = $this->input->get_post('codbar');
        $movimiento     = $this->input->get_post('movimiento');
        $get_detail_und = $this->M_patinador->get_detail_mq($codbar, $movimiento);
        $i              = 1;
        foreach ($get_detail_und->result() as $detail) {?>
            <tr>
                <td><?=$i++;?></td>
                <td class="cantidad"><?=$detail->cantidad;?></td>
                <td><?=$detail->presentacion;?></td>
                <td class="text-center">
                    <div class="col-sm-6 col-md-7 text-center">
                        <input type="number" class="input-md form-control text-center" min="1" max="<?=$detail->cantidad;?>" name="cantidad_und[]" value="0" required>
                    </div>
                </td>

                <input type="hidden" id="input_mat_id" name="mat_id" value="<?=$detail->material_id;?>">
                <input type="hidden" id="input_mat_descripcion" name="mat_descripcion" value="<?=$detail->material_descripcion;?>">
                <input type="hidden" id="input_stock_cantidad" name="stock_cantidad" value="<?=$detail->cantidad;?>">
                <input type="hidden" name="presentacion[]" value="<?=$detail->presentacion;?>">
                <input type="hidden" name="bodega_reintegro_origen" value="<?=$detail->maquina_bod_id;?>">
                <input type="hidden" name="movimiento" value="<?=$movimiento?>">
                <input type="hidden" name="und_nueva[]" value="1">

            </tr>
        <?php }?>
            <input type="hidden" name="codbar" value="<?=$codbar;?>">
        <?php
}
    public function fecha_actual()
    {
        $date        = now();
        $z_horaria   = gmt_to_local($date, 'UM5', date('I', $date));
        $date_actual = mdate("%Y/%m/%d %H:%i:%s", $z_horaria);

        return $date_actual;
    }
    public function sincronizar_sqlserver()
    {
        $date_actual    = $this->fecha_actual();
        $get_detail_und = $this->M_almacen->sincronizar_sqlserver($date_actual);
        echo $get_detail_und;
    }
    public function edit_invt()
    {
        $id       = $this->input->get_post('id');
        $cantidad = $this->input->get_post('cantidad');
        $bodega   = $this->input->get_post('valbog');
        $result   = $this->M_admin->edit_inv($id, $cantidad, $bodega);
        if ($result != false) {
            echo 1;
        } else {
            echo 2;
        }

    }
    public function edit_alist()
    {
        $id          = $this->input->get_post('id');
        $cantidad    = $this->input->get_post('cantidad');
        $tipo        = $this->input->get_post('tipo');
        $movimiento  = $this->input->get_post('movimiento');
        $material    = $this->input->get_post('material');
        $stock       = $this->input->get_post('stock');
        $stock_nuevo = $this->input->get_post('stock_nuevo');
        $result      = $this->M_almacen->edit_alist($id, $cantidad, $tipo, $movimiento, $material, $stock, $stock_nuevo);
        if ($result != false) {
            echo 1;
        } else {
            echo 2;
        }

    }

    public function reg_material()
    {
        $codigo          = $this->input->post('codigo');
        $nombre_material = $this->input->post('nombre_material');
        $verificar       = $this->M_admin->verify($codigo);
        if ($verificar == true) {
            //si existe
            echo 0;
        } else {
            //no existe
            $result = $this->M_admin->reg_material($codigo, $nombre_material);
            if ($result == true) {
                $result1 = $this->M_admin->reg_invt($codigo, $nombre_material, 0, 2, 0);
                if ($result1 == true) {
                    echo 3;
                } else {
                    echo 2;
                }
            } else {
                echo 1;
            }
        }

    }

    public function reg_codigo()
    {
        $true           = 0;
        $false          = 0;
        $materiales     = $this->input->post('material[]');
        $presentaciones = $this->input->post('presentacion[]');
        $cantidades     = $this->input->post('cantidad[]');
        $codigos        = $this->input->post('codigo[]');

        for ($i = 0; $i < count($materiales); $i++) {

            $material     = $materiales[$i];
            $presentacion = $presentaciones[$i];
            $cantidad     = $cantidades[$i];
            $codigo       = $codigos[$i];

            $result = $this->M_material->reg_codigo($material, $presentacion, $cantidad, $codigo);
            if ($result == true) {
                $true = $true + 1;
            } else {
                $false = $false + 0;
            }
        }

        if ($true > 0) {
            echo $true;
        } else {
            echo $false;
        }

    }
    public function delete_codigo()
    {
        $id     = $this->input->post('id');
        $result = $this->M_material->delete_codigo($id);
        if ($result == true) {
            echo 2;
        } else {
            echo 1;
        }

    }
    public function edit_codigo()
    {
        $cantidad = $this->input->post('cantidad');
        $id       = $this->input->post('id');
        $result   = $this->M_material->edit_codigo($cantidad, $id);
        if ($result == true) {
            echo 2;
        } else {
            echo 1;
        }

    }
    public function alistar_prod()
    {
        $id          = $this->input->get_post('id');
        $pati        = $this->session->userdata('usuario_id');
        $material    = $this->input->get_post('material');
        $descripcion = $this->input->get_post('descripcion');
        $imprevistos = $this->input->get_post('imprevistos');
        $movimiento  = $this->input->get_post('movimiento');
        $date_actual = $this->fecha_actual();
        $user_id     = $this->session->userdata('usuario_id');
        $tipo        = $this->input->get_post('tipo');
        $alistar     = $this->M_almacen->alistar_prod($id, $pati, $date_actual, $user_id, $material, $descripcion, $imprevistos, $movimiento, $tipo);
        if ($alistar == 1) {
            echo 1;
        } else {
            echo 2;
        }
    }

    public function alistar_detalles()
    {
        $movimiento = $this->input->get_post('movimiento');
        $material   = $this->input->get_post('material');
        $alistar    = $this->M_almacen->alistar_detalles($movimiento);
        foreach ($alistar->result() as $detail) {?>
            <tr>
                <td><?=$detail->presentacion;?></td>
                <td class="cantidad" contenteditable><?=$detail->cantidad;?></td>
                <td class="subtotal"><?=$detail->cantidad;?></td>
                <td><button class="btn btn-primary btn-xs edit_alist" data-movimiento="<?=$movimiento;?>" data-material="<?$material;?>"  data-id="<?=$detail->dm_id;?>" >Editar</button></td>
            </tr>
            <?php
        }
    }

    public function alistar_detalles2()
    {
        $movimiento = $this->input->get_post('movimiento');
        $naterial   = $this->input->get_post('naterial');
        $alistar    = $this->M_almacen->alistar_detalles($movimiento);
        foreach ($alistar->result() as $detail) {?>
            <tr>
                <td><?=$detail->presentacion;?></td>
                <td class="cantidad2" contenteditable><?=$detail->cantidad;?></td>
                <td class="subtotal2"><?=$detail->cantidad;?></td>
                <td><button class="btn btn-primary btn-xs edit_alist2" data-movimiento="<?=$movimiento;?>" data-material="<?$material;?>" data-id="<?=$detail->dm_id;?>">Editar</button>
                </td>

            </tr>
            <?php
        }
    }

    public function existenciaMaterial()
    {
        $material = $this->input->get_post('material');
        $result   = $this->M_almacen->existenciaMaterial($material);
        
        if ($result != false && $result != null) {
            foreach ($result->result() as $row) {
                echo $row->material_id;
            }
        } else {
            echo "ErrorMat";
        }
    }

}
/* End of file buscar.php */
/* Location: ./application/controllers/buscar.php */