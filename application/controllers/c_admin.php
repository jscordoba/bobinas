<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class C_admin extends CI_Controller {
		
        

		public function __construct()
		{
			parent::__construct();
			
			if (!$this->session->userdata('login') || $this->session->userdata('tipo_id')!=1) {
				redirect(base_url().'main','refresh');
			}else {
				
				$this->load->model('M_admin');
				$this->load->view('head');
				$this->load->view('admin/nav_menu');
			}
		}
		
		public function index()
		{
			$this->load->view('admin/inicio');
			$this->load->view('footer');
		}

		public function new_user(){
			$data['list_seccion'] = $this->M_admin->list_seccion();

			$this->load->view('admin/new_user',$data);			
			$this->load->view('footer');

		}

		public function reg_user(){
			$new_user = $this->M_admin->new_user();

			if ($new_user=="ok") {
				?><script>alert("Usuario Creado...!");</script><?php

				redirect(base_url().'c_admin/list_user','refresh');
			}
			else if($new_user == "usuario") {
				?><script>alert("Nombre Usuario Existente...!");</script><?php

				redirect(base_url().'c_admin/new_user','refresh');
			}
			else if($new_user == "id") {
				?><script>alert("ID Usuario Existente...!");</script><?php

				redirect(base_url().'c_admin/new_user','refresh');
			}


			
			else{
				?><script>alert("Error al crear Usuario, favor verifique...");</script><?php

				redirect(base_url().'c_admin/new_user','refresh');
			}
		}
		public function delete_user(){
		 $data = $this->uri->segment(3);
		$delete_user=  $this->M_admin->delete_user($data);
		 
		 if ($delete_user =="ok") {
				?><script>alert("Usuario Eliminado...!");</script><?php

				redirect(base_url().'c_admin/list_user','refresh');
			}else{
				?><script>alert("Error al eliminar Usuario, favor verifique...");</script><?php

				redirect(base_url().'c_admin/list_user','refresh');
			}

		}

		public function list_user(){
			$data['ver_user'] = $this->M_admin->ver_user();
			$data['list_seccion'] = $this->M_admin->list_seccion();

			$this->load->view('admin/list_user',$data);
			$this->load->view('footer');

		}
		public function set_inv(){
			$data['list_inv'] = $this->M_admin->list_inv();
			$this->load->view('admin/set_inv',$data);
			$this->load->view('footer');

		}

		public function new_station(){
			$data['list_seccion'] = $this->M_admin->list_seccion();

			$this->load->view('admin/new_station',$data);			
			$this->load->view('footer');
		}
		public function editar1(){
			$data['list_seccion'] = $this->M_admin->list_seccion();

			$this->load->view('admin/editar',$data);			
			$this->load->view('footer');
		}
         public function editar_usuario(){
   


			try {
				
			$editar_usuario =  $this->M_admin->editar1();
 
			if ($editar_usuario=="ok") {
					?><script>alert("Usuario Editado...!");</script><?php
					
					redirect(base_url().'c_admin/list_user','refresh');
				}else{
					?><script>alert("Error al editar Usuario, favor verifique...");</script><?php

					redirect(base_url().'c_admin/list_user','refresh');
				}
				} catch (Exception $e) {
				
			}
			// 	$this->load->view('admin/editar_usuario',$data);			
			// $this->load->view('footer');
		}
		public function reg_station(){
			try {
				$new_user = $this->M_admin->new_station();

				if ($new_user=="ok") {
					?><script>alert("Maquina Creada...!");</script><?php
					
					redirect(base_url().'c_admin/new_station','refresh');
				}
				else if($new_user == "maquina") {
				?><script>alert("Nombre Maquina Existente...!");</script><?php

				redirect(base_url().'c_admin/new_station','refresh');
			}
				else{
					?><script>alert("Error al crear Maquina, favor verifique...");</script><?php

					redirect(base_url().'c_admin/list_station','refresh');
				}
				
			} catch (Exception $e) {
				
			}
		}
        	public function delete_station(){
		 $data = $this->uri->segment(3);
		$delete_station=  $this->M_admin->delete_station($data);
		 
		 if ($delete_station =="ok") {
				?><script>alert("Maquina Eliminada...!");</script><?php

				redirect(base_url().'c_admin/list_station','refresh');
			}else{
				?><script>alert("Error al eliminar Maquina, favor verifique...");</script><?php

				redirect(base_url().'c_admin/list_station','refresh');
			}

		}
public function editar2(){
			$data['list_seccion'] = $this->M_admin->list_seccion();

			$this->load->view('admin/editar2',$data);			
			$this->load->view('footer');
		}
         public function editar_station(){
   


			try {
				
			$editar_maquina =  $this->M_admin->editar2();
 
			if ($editar_maquina=="ok") {
					?><script>alert("Maquina Editada...!");</script><?php
					
					redirect(base_url().'c_admin/list_station','refresh');
				}else{
					?><script>alert("Error al editar Maquina, favor verifique...");</script><?php

					redirect(base_url().'c_admin/list_station','refresh');
				}
				} catch (Exception $e) {
				
			}
			// 	$this->load->view('admin/editar_usuario',$data);			
			// $this->load->view('footer');
		}
		public function new_section(){

			$list_seccion=  $this->M_admin->list_seccion();
			$this->load->view('admin/new_section');			
			$this->load->view('footer');
			
		}
public function reg_section(){
			try {
				$new_section = $this->M_admin->new_section();

				if ($new_section=="ok") {
					?><script>alert("Sección Creada...!");</script><?php
					
					redirect(base_url().'c_admin/list_section','refresh');
				}
				else if($new_section == "seccion") {
				?><script>alert("Nombre Seccion Existente...!");</script><?php

				redirect(base_url().'c_admin/new_section','refresh');
			}
				else{
					?><script>alert("Error al crear Maquina, favor verifique...");</script><?php

					redirect(base_url().'c_admin/list_section','refresh');
				}
				
			} catch (Exception $e) {
				
			}
		}
		public function list_section(){
			try {
				$data['list_section'] = $this->M_admin->list_seccion();

				if ($data['list_section']) {
					$this->load->view('admin/list_section', $data);
					$this->load->view('footer');
				}
				
			} catch (Exception $e) {
				
			}
		}
         	public function delete_section(){
		 $data = $this->uri->segment(3);
		$delete_section=  $this->M_admin->delete_section($data);
		 
		 if ($delete_section =="ok") {
				?><script>alert("Seccion Eliminada...!");</script><?php

				redirect(base_url().'c_admin/list_section','refresh');
			}else{
				?><script>alert("Error al eliminar Seccion, favor verifique...");</script><?php

				redirect(base_url().'c_admin/list_section','refresh');
			}

		}
		public function editar3(){
			$data['list_seccion'] = $this->M_admin->list_seccion();

			$this->load->view('admin/editar3',$data);			
			$this->load->view('footer');
		}
         public function editar_section(){
   


			try {
				
			$editar_seccion =  $this->M_admin->editar3();
 
			if ($editar_seccion=="ok") {
					?><script>alert("Sección Editada...!");</script><?php
					
					redirect(base_url().'c_admin/list_section','refresh');
				}else{
					?><script>alert("Error al editar Sección, favor verifique...");</script><?php

					redirect(base_url().'c_admin/list_section','refresh');
				}
				} catch (Exception $e) {
				
			}
			// 	$this->load->view('admin/editar_usuario',$data);			
			// $this->load->view('footer');
		}
		public function list_station(){
			$data['ver_maquina'] = $this->M_admin->ver_maquina();
			$data['list_seccion'] = $this->M_admin->list_seccion();

			$this->load->view('admin/list_station',$data);
			$this->load->view('footer');
		}


	
	}
	
	/* End of file c_admin.php */
	/* Location: ./application/controllers/c_admin.php */

 ?>