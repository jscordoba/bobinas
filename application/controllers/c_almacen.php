<?php

defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_get("America/Bogota");

class C_almacen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('login') || $this->session->userdata('tipo_id') != 3) {
            redirect(base_url() . 'main', 'refresh');
        } else {
            $this->load->model('M_almacen');
            $this->load->model('M_material');
            $this->load->view('head');
            $this->load->view('almacen/nav_menu');
            $this->load->helper(array('form', 'url'));

        }

    }
    public function index()
    {
        try {
            $data['movimientos']          = $this->M_almacen->v_movimientos();
            $data['indicador_pdt']        = $this->M_almacen->indicador("pdt");
            $data['indicador_asig']       = $this->M_almacen->indicador("asig");
            $data['indicador_sol_rein']   = $this->M_almacen->indicador("sol_rein");
            $data['indicador_recib_rein'] = $this->M_almacen->indicador("recib_rein");
            // $data['import_file']=$this->M_almacen->import_file();
            // $this->load->view('upload_form', array('error' => ' ' ));

        } catch (Exception $e) {
            $data['movimientos'] = "Error";
        }

        $this->load->view('almacen/inicio', $data);
        $this->load->view('footer');
    }

    public function fecha_actual()
    {
        $date        = now();
        $z_horaria   = gmt_to_local($date, 'UM5', date('I', $date));
        $date_actual = mdate("%Y/%m/%d %H:%i:%s", $z_horaria);

        return $date_actual;
    }

    public function stock()
    {
        $data['indicador_mat']  = $this->M_almacen->indicador("num_material");
        $data['indicador_mov']  = $this->M_almacen->indicador("num_mov");
        $data['indicador_date'] = $this->M_almacen->indicador("date_sinc");
        $data['indicador_sinc'] = $this->M_almacen->indicador("ult_sinc");

        $this->load->view('almacen/stock', $data);
        $this->load->view('footer');
    }
    public function import_file()
    {
        try {
            $config['upload_path']   = './uploads/';
            $config['allowed_types'] = 'csv';
            $config['max_size']      = 100;
            $config['max_width']     = 1024;
            $config['max_height']    = 768;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('import_file')) {
                $error = array('error' => $this->upload->display_errors());
                echo ' <script>alert("El archivo tiene que ser CSV...!");</script>';
                redirect(base_url() . 'c_almacen/import_stock', 'refresh');
            } else {
                $data        = array('upload_data' => $this->upload->data());
                $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                $file_path   = $upload_data['full_path'];
                $import_file = $this->M_almacen->import_file($file_path);
                echo ' <script>alert("' . $import_file . '");</script>';
                redirect(base_url() . 'c_almacen/import_stock', 'refresh');
            }
        } catch (Exception $e) {
            echo "Error";
        }
        $this->load->view('footer');
    }
    public function import_stock()
    {
        $data['import'] = $this->M_almacen->ver_stock_import();

        $this->load->view('almacen/import_stock', $data);
        $this->load->view('footer');
    }
    public function sincronizar()
    {
        // $data['import']=$this->M_almacen->ver_stock_import();

        $this->load->view('almacen/sincronizar');
        $this->load->view('footer');
    }
    public function stock_detail()
    {
        $data['stock'] = $this->M_almacen->ver_stock();

        $this->load->view('almacen/stock_detail', $data);
        $this->load->view('footer');
    }

    public function sol_pendientes()
    {
        $data['sol_pendientes'] = $this->M_almacen->sol_pendientes();
        $data['sol_alistar']    = $this->M_almacen->sol_alistar();

        $this->load->view('almacen/sol_pendientes', $data);
        $this->load->view('footer');
    }

    public function sol_entregados()
    {
        $data['sol_entregados'] = $this->M_almacen->sol_entregados();
        $this->load->view('almacen/sol_entregados', $data);
        $this->load->view('footer');
    }

    public function solicitudes()
    {
        $this->load->view('almacen/solicitudes');
        $this->load->view('footer');
    }

    public function sol_reintegros()
    {
        $data['sol_reintegros'] = $this->M_almacen->sol_reintegros();

        $this->load->view('almacen/sol_reintegros', $data);
        $this->load->view('footer');
    }

    public function reintegros_recibidos()
    {
        $data['hist_reintegros'] = $this->M_almacen->hist_reintegros();

        $this->load->view('almacen/rein_recibidos', $data);
        $this->load->view('footer');
    }

    // public function recibir_reintegros(){
    //     $this->load->view('almacen/recibir_reintegros');
    //     $this->load->view('footer');
    // }

    //-------------------REGISTRO DE TRASLADOS ENTRE (BOD PPAL HACIA BOD_PROD) y (BOD_PROD hacia BOD_MAQX) -----------------//
    public function traslado()
    {
        $date_actual        = $this->fecha_actual();
        $user_id            = $this->session->userdata('usuario_id');
        $sol_id             = $this->input->get_post('sol_id');
        $mat_id             = $this->input->get_post('mat_id');
        $mat_descripcion    = $this->input->get_post('mat_descrip');
        $mat_cantidad       = $this->input->get_post('mat_cant');
        $chk_reintegro      = $this->input->get_post('chk_reintegro');
        $und_nueva          = $this->input->get_post('und_nueva[]'); //Lista de unidades que se van a asignar
        $unds_actual        = $this->input->get_post('unds_actual[]'); //lista de unidades que hay en existencia (OBSOLETO)
        $cantidad_und       = $this->input->get_post('cantidad_und[]');
        $presentacion       = $this->input->get_post('presentacion[]');
        $stock_cantidad     = $this->input->get_post('stock_cantidad');
        $tipo               = $this->input->get_post('tipo');
        $imprevistos        = $this->input->get_post('imprevistos');
        $codigo             = $this->input->get_post('codigo');
        $bod_maquina_origen = 1;
        $bod_maquina_fin    = 2;
        try {
            $reg_traslado = $this->M_almacen->reg_traslado($sol_id, $mat_id, $mat_cantidad, $mat_descripcion, $date_actual, $und_nueva, $unds_actual, $stock_cantidad, $cantidad_und, $presentacion, "almacen", $bod_maquina_origen, $bod_maquina_fin, $user_id, $tipo, $imprevistos, $codigo);
            if ($reg_traslado[0] == "ok") {
                ?>
					<script>alert("Material Listo...!");</script>
				<?php
redirect(base_url() . 'c_almacen/sol_pendientes', 'refresh');
            }

        } catch (Exception $e) {
            echo "Error en el Traslado.";
        }

    }
    public function traslado2()
    {
        $date_actual        = $this->fecha_actual();
        $user_id            = $this->session->userdata('usuario_id');
        $sol_id             = $this->input->get_post('sol_id');
        $mat_id             = $this->input->get_post('mat_id');
        $mat_descripcion    = $this->input->get_post('mat_descrip');
        $mat_cantidad       = $this->input->get_post('mat_cant');
        $chk_reintegro      = $this->input->get_post('chk_reintegro');
        $und_nueva          = $this->input->get_post('und_nueva[]'); //Lista de unidades que se van a asignar
        $unds_actual        = $this->input->get_post('unds_actual[]'); //lista de unidades que hay en existencia (OBSOLETO)
        $cantidad_und       = 0;
        $stock_cantidad     = $this->input->get_post('stock_cantidad');
        $tipo               = $this->input->get_post('tipo2');
        $imprevistos        = $this->input->get_post('imprevistos');
        $codigo             = $this->input->get_post('codigo2');
        $presentacion       = $this->input->get_post('presentacion2[]');
        $bod_maquina_origen = 1;
        $bod_maquina_fin    = 2;
        try {
            $reg_traslado = $this->M_almacen->reg_traslado2($sol_id, $mat_id, $mat_cantidad, $mat_descripcion, $date_actual, $und_nueva, $unds_actual, $stock_cantidad, $cantidad_und, "almacen", $bod_maquina_origen, $bod_maquina_fin, $user_id, $tipo, $imprevistos, $codigo, $presentacion);
            if ($reg_traslado[0] == "ok") {
                ?>
					<script>alert("Material Listo...!");</script>
				<?php
redirect(base_url() . 'c_almacen/sol_pendientes', 'refresh');
            }

        } catch (Exception $e) {
            echo "Error en el Traslado.";
        }

    }

    //-------------------REGISTRO DE TRASLADO ENTRE BOD PRODUCCION HACIA BOD PPAL-----------------//
    public function traslado_prod()
    {
        $date_actual = $this->fecha_actual();
        $user_id     = $this->session->userdata('usuario_id');
        $sol_rein_id = $this->input->get_post('inp_solicitudr_id');
        $mat_id      = $this->input->get_post('inp_material_id');
        $mat_desc    = $this->input->get_post('inp_mmaterial_descripcion');
        $mat_cant    = $this->input->get_post('inp_solicitudr_cantidad');
        $tipo        = $this->input->get_post('tipo');
        $bodega      = $this->input->get_post('bodega');
        $patinador   = $this->input->get_post('patinador');
        try {
            $reg_traslado = $this->M_almacen->reg_traslado_prod($sol_rein_id, $mat_id, $mat_desc, $mat_cant, $date_actual, $tipo, $bodega, $patinador);
            if ($reg_traslado == "ok") {
                ?>
					<script>alert("Reintegro exitoso...!");</script>
				<?php
redirect(base_url() . 'c_almacen/sol_reintegros', 'refresh');
            }
        } catch (Exception $e) {
            echo "Error en el Traslado.";
        }
    }
    public function n_entrada()
    {
        $date_actual   = $this->fecha_actual();
        $material_id   = $this->input->get_post('stock_material_id');
        $material_desc = $this->input->get_post('stock_material_desc');
        $material_cant = $this->input->get_post('stock_material_cant');
        $usuario_id = $this->session->userdata('usuario_id');

        //echo "$material_id - $material_desc - $material_cant";

        $reg_mat = $this->M_almacen->reg_mat($material_id, $material_desc, $material_cant);

        if ($reg_mat == "mat_new") {
            //echo "Antes de ingresar a: new_cant_material";
            $reg_movimiento = $this->M_almacen->new_cant_material($material_id, $material_desc, $material_cant, $date_actual);

            if ($reg_movimiento == "ins_ok") {
                //echo "$reg_movimiento";
                redirect(base_url() . 'c_almacen/stock_detail', 'refresh');
            }
        } elseif ($reg_mat == "mat_exist") {
            //echo "Antes de ingresar a: upd_cant_material";
            $reg_movimiento = $this->M_almacen->upd_cant_material($material_id, $material_desc, $material_cant, $date_actual);

            if ($reg_movimiento == "upd_ok") {
                //echo "$reg_movimiento";
                redirect(base_url() . 'c_almacen/stock_detail', 'refresh');
            }
        } else {
            echo "Error...";
        }

    }
    public function estaciones()
    {
        $data['estaciones'] = $this->M_almacen->ver_estaciones();

        $this->load->view('almacen/estaciones', $data);
        $this->load->view('footer');

    }

    public function codigos()
    {
        $data['list_codigos']    = $this->M_material->list_codigos();
        $data['list_materiales'] = $this->M_material->list_materiales();
        $this->load->view('almacen/codigos', $data);
        $this->load->view('footer');

    }
}

/* End of file c_almacen.php */
/* Location: ./application/controllers/c_almacen.php */