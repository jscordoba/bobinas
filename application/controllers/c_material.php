<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_material extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('login') || $this->session->userdata('tipo_id') != 4) {
            redirect(base_url() . 'main', 'refresh');
        } else {
            $this->load->model('M_material');
            $this->load->view('head');
            $this->load->view('materiales/nav_menu');
        }
    }

    public function fecha_actual()
    {
        $date        = now();
        $z_horaria   = gmt_to_local($date, 'UM5', date('I', $date));
        $date_actual = mdate("%Y/%m/%d %H:%i:%s", $z_horaria);

        return $date_actual;
    }

    public function index()
    {
        try {
            $data['movimientos']          = $this->M_material->v_movimientos();

            $data['indicador_pdt']        = $this->M_material->indicador("pdt");
            $data['indicador_asig']       = $this->M_material->indicador("asig");
            $data['indicador_sol_mat']    = $this->M_material->indicador("sol_mat");
            $data['indicador_sol_resp']   = $this->M_material->indicador("sol_resp");
            $data['indicador_maq_reint']  = $this->M_material->indicador("maq_reint");
            $data['indicador_prod_reint'] = $this->M_material->indicador("prod_reint");
            $data['indicador_ot_act']     = $this->M_material->indicador("ot_act");
            $data['indicador_ot_fin']     = $this->M_material->indicador("ot_fin");

        } catch (Exception $e) {
            $data['movimientos'] = "Error";
        }

        $this->load->view('materiales/inicio', $data);
        $this->load->view('footer');
    }

    //-- LIST VIEWS ---//
    public function stock_solicitudes()
    {
        $data['stock_solicitudes'] = $this->M_material->ver_stock_solicitudes();
        //$data ['stock'] = $this->M_material->ver_stock();
        $data['maquinas']        = $this->M_material->ver_estaciones();
        $data['stock_reintegro'] = $this->M_material->ver_stock_reintegro();

        $this->load->view('materiales/stock_solicitudes', $data);
        $this->load->view('footer');
    }

    public function ordenes_trabajo()
    {
        $data['ordenes_activas']     = $this->M_material->ver_ordenes('ACTIVO');
        $data['ordenes_finalizadas'] = $this->M_material->ver_ordenes('FINALIZADO');

        $this->load->view('materiales/ordenes_trabajo', $data);
        $this->load->view('footer');

    }

    public function sol_pendientes()
    {
        $data['stock_solicitudes'] = $this->M_material->ver_stock_solicitudes();

        $this->load->view('materiales/sol_pendientes', $data);
        $this->load->view('footer');

    }

    public function list_reintegros_ppal()
    {
        $data['reintegros_ppal'] = $this->M_material->reintegros_ppal();

        $this->load->view('materiales/list_reintegros_ppal', $data);
        $this->load->view('footer');

    }

    public function list_reintegros_mq()
    {
        $data['reintegros_mq'] = $this->M_material->reintegros_mq();

        $this->load->view('materiales/list_reintegros_mq', $data);
        $this->load->view('footer');

    }
    public function reporte()
    {
        $data['reporte'] = $this->M_material->reporte();

        $this->load->view('materiales/reporte', $data);
        $this->load->view('footer');
    }
    public function reporte2()
    {
        $data['reporte2'] = $this->M_material->reporte2();

        $this->load->view('materiales/repo_estaciones', $data);
        $this->load->view('footer');
    }
    public function reporte3()
    {
        $data['reporte3'] = $this->M_material->reporte3();

        $this->load->view('materiales/repo_reintegros', $data);
        $this->load->view('footer');
    }
    public function reporte4()
    {
        $data['reporte4'] = $this->M_material->reporte4();

        $this->load->view('materiales/patinador_almacen', $data);
        $this->load->view('footer');
    }
    public function new_reintegro()
    {
        $data['stock_reintegro'] = $this->M_material->ver_stock_reintegro();

        $this->load->view('materiales/new_reintegro', $data);
        $this->load->view('footer');

    }
    public function codigos()
    {
        $data['list_codigos']    = $this->M_material->list_codigos();
        $data['list_materiales'] = $this->M_material->list_materiales();
        $this->load->view('materiales/codigos', $data);
        $this->load->view('footer');

    }
    //-- INSERTS ---//
    public function reg_material()
    {
        $tipo = $this->uri->segment(3);
        if ($tipo == '1') {
            $material_id          = $this->input->get_post('material_id');
            $material_descripcion = $this->input->get_post('input_material_descripcion');
            $material_cantidad    = $this->input->get_post('material_cantidad');
            $observacion          = $this->input->get_post('observacion');
            $tipo                 = "1";
        } else if ($tipo == '2') {
            $material_id          = $this->input->get_post('material_id2');
            $material_descripcion = $this->input->get_post('input_material_descripcion2');
            $material_cantidad    = $this->input->get_post('material_cantidad2');
            $observacion          = $this->input->get_post('observacion2');

            $tipo = "2";
        }
        $date_actual = $this->fecha_actual();

        try {
            $reg_material = $this->M_material->reg_sol_material($material_id, $material_descripcion, $material_cantidad, $date_actual, $tipo, $observacion);
            echo "<script>alert('Su codigo de solicitud es " . $reg_material . "');</script>";
            redirect(base_url() . 'c_material/', 'refresh');
        } catch (Exception $e) {
            echo "Error en Registro de Solicitud.";
        }
    }
    public function sol_reintegro_alm()
    {
        $date_actual           = $this->fecha_actual();
        $mat_id                = $this->input->get_post('input_matid_rein_alm');
        $mat_disponible        = $this->input->get_post('input_cantmat_rein_alm');
        $mat_reintegro         = $this->input->get_post('cantidad_rein_alm');
        $observacion_reintegro = $this->input->get_post('observacion_ralm');
        $movimiento            = $this->input->get_post('movimiento');
        $codigo                = $this->input->get_post('codigo');
        $tipo                  = $this->input->get_post('tipo');
        if (isset($mat_id) && isset($mat_disponible) && isset($mat_reintegro)) {
            try {
                $reg_sol = $this->M_material->reg_sol_reintegro($mat_id, $mat_disponible, $mat_reintegro, $observacion_reintegro, $date_actual, $movimiento, $codigo, $tipo);

                if ($reg_sol == "ok") {
                    ?>
							<script>alert("Solicitud enviada...!");</script>
						<?php

                    redirect(base_url() . 'c_material/new_reintegro', 'refresh');
                }
            } catch (Exception $e) {
                redirect('c_materiales/list_reintegros', 'refresh');
                echo "Error en el Registro de la solicitud.";
            }
        }
    }
    public function estaciones()
    {
        $data['estaciones'] = $this->M_material->ver_estaciones();
        $this->load->view('materiales/estaciones', $data);
        $this->load->view('footer');
    }
    public function repo_estaciones()
    {
        $this->load->view('materiales/repo_estaciones');
        $this->load->view('footer');

    }

    public function repo_reintegros()
    {
        $this->load->view('materiales/repo_reintegros');
        $this->load->view('footer');

    }
    public function new_detailot()
    {

        $data['materiales'] = $this->M_material->reg_detailot();

        $this->load->view('materiales/estaciones');
        $this->load->view('footer');
    }
    public function reg_detailot()
    {
        $new_user = $this->M_material->new_detailot();

        if ($new_user == "ok") {
            ?><script>alert("Orden Creada...!");</script><?php

            redirect(base_url() . 'c_material/estaciones', 'refresh');
        } else {
            ?><script>alert("Error al crear Orden, favor verifique...");</script><?php

            redirect(base_url() . 'c_material/estaciones', 'refresh');
        }

    }

    public function edit_detailot()
    {

        $data['estaciones'] = $this->M_material->ver_estaciones();

        $this->load->view('material/estaciones', $data);
        $this->load->view('footer');
    }
    public function editar_detailot()
    {

        try {

            $edit_detailot = $this->M_material->edit_detailot();

            if ($edit_detailot == "ok") {
                ?><script>alert("Detalle Editado...!");</script><?php

                redirect(base_url() . 'c_material/estaciones', 'refresh');
            } else {
                ?><script>alert("Error al editar Detalle, favor verifique...");</script><?php

                redirect(base_url() . 'c_material/estaciones', 'refresh');
            }
        } catch (Exception $e) {

        }
        //     $this->load->view('admin/editar_usuario',$data);
        // $this->load->view('footer');
    }

     public function import_file_material_maxivo()
    {
        try {
            $config['upload_path']   = './solicitudes/';
            $config['allowed_types'] = 'csv';
            $config['max_size']      = 5120;
            $config['max_width']     = 1024;
            $config['max_height']    = 768;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                echo ' <script>alert("El archivo tiene que ser CSV...!");</script>';
                redirect(base_url() . 'c_material/', 'refresh');
            } else {
                $data        = array('upload_data' => $this->upload->data());
                $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                $file_path   = $upload_data['full_path'];
                $import_file = $this->M_material->import_file($file_path);

                echo ' <script>alert("' . $import_file['correctos'] . ' solicitudes ingresadas.\\n'.$import_file['error'].' solicitudes no procesadas.");
                    </script>';
                redirect(base_url() . 'c_material/', 'refresh');
            }
        } catch (Exception $e) {
            echo "Error";
        }
        
        $this->load->view('footer');
    }
    
    public function reintegrar()
    {
        $presentacion = $this->input->get_post('presentacion[]');
        $id           = $this->input->get_post('id[]');
        $cantidad     = $this->input->get_post('cantidad[]');
        $date_actual  = $this->fecha_actual();
        $observacion  = $this->input->get_post('observacion_ralm');
        $codigo       = $this->input->get_post('codbar[]');
        $material     = $this->input->get_post('material_rein_normal');
        $descripcion  = $this->input->get_post('descripcion_rein_normal');
        $movimiento   = $this->input->get_post('movimiento_rein_normal');
        try {
            $reg_sol = $this->M_material->reg_sol_reintegro_normal($material, $cantidad, $observacion, $date_actual, $movimiento, $codigo, $id);
            if ($reg_sol == "ok") {
                ?>
							<script>alert("Solicitud enviada...!");</script>
						<?php

                redirect(base_url() . 'c_material/new_reintegro', 'refresh');
            }
        } catch (Exception $e) {
            redirect('c_materiales/list_reintegros', 'refresh');
            echo "Error en el Registro de la solicitud.";
        }

    }
    /* End of file c_materiales.php */
    /* Location: ./application/controllers/c_materiales.php */
}

?>