<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_patinador extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('login') || $this->session->userdata('tipo_id') != 2) {
            redirect(base_url() . 'main', 'refresh');
        } else {
            $this->load->model('M_patinador');
            $this->load->model('M_almacen');

            $this->load->view('head');
            $this->load->view('patinador/nav_menu');
        }
    }

    public function index()
    {
        try {
            $this->load->model('M_material');

            $data['movimientos'] = $this->M_patinador->v_movimientos();

            $data['indicador_ot_act'] = $this->M_material->indicador("ot_act");
            $data['indicador_ot_fin'] = $this->M_material->indicador("ot_fin");
            $data['indicador_pdt']    = $this->M_material->indicador("pdt");
            $data['indicador_asig']   = $this->M_material->indicador("asig");

        } catch (Exception $e) {
            $data['movimientos'] = "Error";
        }

        $this->load->view('patinador/inicio', $data);
        $this->load->view('footer');
    }

    public function fecha_actual()
    {
        $date        = now();
        $z_horaria   = gmt_to_local($date, 'UM5', date('I', $date));
        $date_actual = mdate("%Y/%m/%d %H:%i:%s", $z_horaria);

        return $date_actual;
    }

    public function stock_solicitudes()
    {
        $data['stock_solicitudes'] = $this->M_patinador->ver_stock_solicitudes();
        $data['maquinas']          = $this->M_patinador->ver_estaciones();
        $data['stock_reintegro']   = $this->M_patinador->ver_stock_reintegro();
        $data['stock_reintegro2']  = $this->M_patinador->ver_stock_reintegro2();
        $data['sol_alistar']       = $this->M_almacen->sol_alistar();
        $this->load->view('patinador/stock_solicitudes', $data);
        $this->load->view('footer');
    }
    public function finalizar_ot()
    {
        $data['maquinas'] = $this->M_patinador->ver_estaciones();

        $this->load->view('patinador/fin_ot', $data);
        $this->load->view('footer');

    }

    //----------- TRASLADO DE PROD A MAQUINA----------------
    public function traslado()
    {
        $ot_asignar         = $this->input->get_post('ot_asignar'); //SOL_OT
        $mat_id             = $this->input->get_post('mat_id');
        $mat_cantidad       = $this->input->get_post('mat_cantidad');
        $mat_descripcion    = $this->input->get_post('mat_descripcion');
        $date_actual        = $this->fecha_actual();
        $und_nueva          = $this->input->get_post('und_nueva[]');
        $unds_actual        = $this->input->get_post('unds_actual[]');
        $stock_cantidad     = $this->input->get_post('stock_cantidad');
        $cantidad_und       = $this->input->get_post('cantidad_und[]');
        $presentacion       = $this->input->get_post('presentacion[]');
        $bod_maquina_origen = 2;
        $bod_maquina        = $this->input->get_post('asig_maquina');
        $bod_maquina_fin    = $bod_maquina;
        $user_id            = $this->session->userdata('usuario_id');
        $patinador          = $this->input->get_post('patinador');
        $perfil             = "patinador";
        $codigo             = $this->input->get_post('codbar[]');
        $id                 = $this->input->get_post('id');
        $tipo               = $this->input->get_post('tipo');
        // echo "valor  ".json_encode($tipo); die();
        try {
            @$reg_traslado = $this->M_patinador->reg_traslado($ot_asignar, $mat_id, $mat_cantidad, $mat_descripcion, $date_actual, $und_nueva, $unds_actual, $stock_cantidad, $cantidad_und, $presentacion, $perfil, $bod_maquina_origen, $bod_maquina_fin, $user_id, $patinador, $codigo, $id, $tipo);
            if ($reg_traslado == "ok") {
                ?>
					<script>alert("Asignación con Éxito...!");</script>
				<?php
redirect(base_url() . 'c_patinador/stock_solicitudes', 'refresh');
            }
        } catch (Exception $e) {
            echo "Error en el Traslado.";
        }
    }
    public function traslado2()
    {
        $ot_asignar         = $this->input->get_post('ot_asignar'); //SOL_OT
        $mat_id             = $this->input->get_post('mat_id');
        $stock_cantidad     = $this->input->get_post('stock_cantidad');
        $mat_descripcion    = $this->input->get_post('mat_descripcion');
        $date_actual        = $this->fecha_actual();
        $perfil             = "patinador";
        $bod_maquina_origen = 2;
        $bod_maquina        = $this->input->get_post('asig_maquina');
        $bod_maquina_fin    = $bod_maquina;
        $user_id            = $this->session->userdata('usuario_id');
        $patinador          = $this->input->get_post('patinador');
        $codigo             = $this->input->get_post('codbar');
        $id                 = $this->input->get_post('id');
        $cantidad_bandeja   = $this->input->get_post('cantidad_bandeja');
        $id_detalle         = $this->input->get_post('id_detalle');

        try {
            $reg_traslado = $this->M_patinador->reg_traslado_bandeja($ot_asignar, $mat_id, $mat_descripcion, $date_actual, $stock_cantidad, $perfil, $bod_maquina_origen, $bod_maquina_fin, $user_id, $patinador, $codigo, $id, $cantidad_bandeja, $id_detalle);

            if ($reg_traslado == "ok") {
                ?>
					<script>alert("Asignación con Éxito...!");</script>
				<?php
redirect(base_url() . 'c_patinador/stock_solicitudes', 'refresh');
            }
        } catch (Exception $e) {
            echo "Error en el Traslado.";
        }
    }
    public function traslado_mq()
    {
        $date_actual             = $this->fecha_actual();
        $user_id                 = $this->session->userdata('usuario_id');
        $mat_id                  = $this->input->get_post('mat_id');
        $mat_descripcion         = $this->input->get_post('mat_descripcion');
        $bod_maquina_destino     = $this->input->get_post('bod_maquina_destino');
        $mat_cantidad            = $this->input->get_post('mat_cantidad');
        $und_nueva               = $this->input->get_post('und_nueva[]');
        $unds_actual             = $this->input->get_post('unds_actual[]');
        $cantidad_und            = $this->input->get_post('cantidad_und[]');
        $presentacion            = $this->input->get_post('presentacion[]');
        $stock_cantidad          = $this->input->get_post('stock_cantidad');
        $chk_bodega              = $this->input->get_post('chk_bodega');
        $bodega_reintegro_origen = $this->input->get_post('bodega_reintegro_origen');
        $movimientos             = $this->input->get_post('movimiento');
        if ($chk_bodega == "bodega") {
            $ot_codbar                = $this->input->get_post('codbar');
            $perfil                   = "bodega";
            $bodega_reintegro_destino = 1;
            $codigo                   = $this->input->get_post('codbar');
        } elseif ($chk_bodega == "maquina") {
            $ot_codbar                = $this->input->get_post('ot_asignar');
            $perfil                   = "maquina";
            $bodega_reintegro_destino = $this->input->get_post('bodega_reintegro_destino');
            $codigo                   = $this->input->get_post('codbar');
        }
        $tipo = 1;
        try {
            $reg_traslado = $this->M_patinador->reg_traslado2($ot_codbar, $mat_id, $mat_cantidad, $mat_descripcion, $date_actual, $und_nueva, $unds_actual, $stock_cantidad, $cantidad_und, $presentacion, $perfil, $bodega_reintegro_origen, $bodega_reintegro_destino, $user_id, $movimientos, $codigo, $tipo);
            if ($reg_traslado == "ok") {
                ?>
					<script>alert("Asignación con Éxito...!");</script>
				<?php
redirect(base_url() . 'c_patinador/stock_solicitudes', 'refresh');
            }
        } catch (Exception $e) {
            echo "Error en el Traslado.";
        }
    }
    public function traslado_mq2()
    {
        $date_actual             = $this->fecha_actual();
        $user_id                 = $this->session->userdata('usuario_id');
        $mat_id                  = $this->input->get_post('mat_id');
        $mat_descripcion         = $this->input->get_post('mat_descripcion');
        $bod_maquina_destino     = $this->input->get_post('bod_maquina_destino');
        $mat_cantidad            = $this->input->get_post('mat_cantidad');
        $und_nueva               = $this->input->get_post('und_nueva[]');
        $unds_actual             = $this->input->get_post('unds_actual[]');
        $cantidad_und            = $this->input->get_post('cantidad_und[]');
        $presentacion            = $this->input->get_post('presentacion[]');
        $stock_cantidad          = $this->input->get_post('stock_cantidad');
        $chk_bodega              = $this->input->get_post('chk_bodega2');
        $bodega_reintegro_origen = $this->input->get_post('bodega_reintegro_origen');
        $movimientos             = $this->input->get_post('movimiento');
        if ($chk_bodega == "bodega") {
            $ot_codbar                = $this->input->get_post('codbar');
            $perfil                   = "bodega";
            $bodega_reintegro_destino = 1;
            $codigo                   = $this->input->get_post('codbar');
        } elseif ($chk_bodega == "maquina") {
            $ot_codbar                = $this->input->get_post('ot_asignar');
            $perfil                   = "maquina";
            $bodega_reintegro_destino = $this->input->get_post('bodega_reintegro_destino');
            $codigo                   = $this->input->get_post('codbar');
        }
        $tipo = 2;
        try {
            $reg_traslado = $this->M_patinador->reg_traslado2($ot_codbar, $mat_id, $mat_cantidad, $mat_descripcion, $date_actual, $und_nueva, $unds_actual, $stock_cantidad, $cantidad_und, $presentacion, $perfil, $bodega_reintegro_origen, $bodega_reintegro_destino, $user_id, $movimientos, $codigo, $tipo);
            if ($reg_traslado == "ok") {
                ?>
					<script>alert("Asignación con Éxito...!");</script>
				<?php
redirect(base_url() . 'c_patinador/stock_solicitudes', 'refresh');
            }
        } catch (Exception $e) {
            echo "Error en el Traslado.";
        }
    }
    //----------- TRASLADO DE MAQUINA A ALMACEN (BOD_PPAL) POR REINTEGRO----------------
    public function traslado_prod()
    {
        $date_actual     = $this->fecha_actual();
        $user_id         = $this->session->userdata('usuario_id');
        $codbar          = $this->input->get_post('input_codbar');
        $mat_id          = $this->input->get_post('input_mat_rid');
        $mat_descripcion = $this->input->get_post('input_mat_rdescripcion');
        $maquina_origen  = $this->input->get_post('input_maq_origen');
        $bod_destino     = 2;
        $mat_cantidad    = $this->input->get_post('input_cantidad_reintegro');
        if (isset($codbar) && isset($mat_id) && isset($mat_descripcion) && isset($maquina_origen) && isset($mat_cantidad)) {
            try {
                $reg_traslado = $this->M_patinador->reg_traslado_prod($codbar, $mat_id, $maquina_origen, $mat_cantidad, $mat_descripcion, $date_actual, $user_id);
                if ($reg_traslado == "ok") {
                    ?>
						<script>alert("Reintegro exitoso...!");</script>
					<?php
redirect(base_url() . 'c_patinador/finalizar_ot', 'refresh');
                }

            } catch (Exception $e) {
                echo "Error en el Traslado.";
            }
        } else {
            ?>
				<script>alert("Sin información...");</script>
			<?php
redirect(base_url() . 'c_patinador/finalizar_ot', 'refresh');
        }
    }

    //----------- TRASLADO DE MAQUINA A BOD_PPAL SIN REINTEGRO----------------

    public function traslado_prod_sinReintegro()
    {
        $date_actual     = $this->fecha_actual();
        $user_id         = $this->session->userdata('usuario_id');
        $codbar          = $this->input->get_post('input_codbar');
        $mat_id          = $this->input->get_post('input_mat_rid');
        $mat_descripcion = $this->input->get_post('input_mat_rdescripcion');
        $maquina_origen  = $this->input->get_post('bodega_reintegro_origen');
        $id              = $this->input->get_post('id');
        $tipo            = $this->input->get_post('tipo');

        $bod_destino  = 1;
        $mat_cantidad = 0;

        if (isset($codbar) && isset($mat_id) && isset($mat_descripcion) && isset($maquina_origen) && isset($mat_cantidad) && isset($id)) {
            //echo "codbar: $codbar<br>mat_id: $mat_id<br>desc: $mat_descripcion<br>origen: $maquina_origen<br>cantidad: $mat_cantidad";

            try {
                $reg_traslado = $this->M_patinador->reg_traslado_prod($codbar, $mat_id, $maquina_origen, $bod_destino, $mat_cantidad, $mat_descripcion, $date_actual, $user_id, $id, $tipo);

                if ($reg_traslado == "ok") {
                    ?>
						<script>alert("Reintegro exitoso...!");</script>
					<?php
redirect(base_url() . 'c_patinador/finalizar_ot', 'refresh');
                }

            } catch (Exception $e) {
                echo "Error en el Traslado.";
            }
        } else {
            ?>
				<script>alert("Sin información...");</script>
			<?php
redirect(base_url() . 'c_patinador/finalizar_ot', 'refresh');
        }
    }
    public function traslado_prod_sinReintegro2()
    {
        $date_actual     = $this->fecha_actual();
        $user_id         = $this->session->userdata('usuario_id');
        $codbar          = $this->input->get_post('input_codbar');
        $mat_id          = $this->input->get_post('input_mat_rid');
        $mat_descripcion = $this->input->get_post('input_mat_rdescripcion');
        $maquina_origen  = $this->input->get_post('bodega_reintegro_origen');
        $mat_cantidad    = 0;

        if (isset($codbar) && isset($mat_id) && isset($mat_descripcion) && isset($maquina_origen) && isset($mat_cantidad)) {
            //echo "codbar: $codbar<br>mat_id: $mat_id<br>desc: $mat_descripcion<br>origen: $maquina_origen<br>cantidad: $mat_cantidad";

            try {
                $reg_traslado = $this->M_patinador->reg_traslado_prod($codbar, $mat_id, $maquina_origen, $mat_cantidad, $mat_descripcion, $date_actual, $user_id);

                if ($reg_traslado == "ok") {
                    ?>
						<script>alert("Reintegro exitoso...!");</script>
					<?php
redirect(base_url() . 'c_patinador/finalizar_ot', 'refresh');
                }

            } catch (Exception $e) {
                echo "Error en el Traslado.";
            }
        } else {
            ?>
				<script>alert("Sin información...");</script>
			<?php
redirect(base_url() . 'c_patinador/finalizar_ot', 'refresh');
        }
    }

}

/* End of file c_patinador.php */
/* Location: ./application/controllers/c_patinador.php */