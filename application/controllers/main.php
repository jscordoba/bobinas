<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Main extends CI_Controller {

		public function __construct()
		{
			parent::__construct();
			$this->load->view('head');
			/* SEGUN SESION CARGA EL MENU DEL USUARIO */
			
			//$this->load->view('patinador/nav_menu');
			/*
			$this->load->view('patinador/nav_menu');
			$this->load->view('almacen/nav_menu');
			$this->load->view('produccion/nav_menu');
			$this->load->view('admin/nav_menu');
			*/
		}
	
		public function index()
		{
			$this->load->view('login');

			/* SEGUN SESION CARGA PANTALLA DE INICIO DE USUARIO */
			/*
			$this->load->view('patinador/inicio');
			$this->load->view('almacen/inicio');
			$this->load->view('produccion/inicio');
			$this->load->view('admin/inicio');


			*/
			$this->load->view('footer');
		}

		public function login(){//LOGUEO AL APLICATIVO
			// echo "ok";
			$this->load->model('M_main');

			$log_user=$this->input->get_post('usuario');
			$log_clave=$this->input->get_post('password');
			
			// echo "Usuario: ".$log_user."<br>Clave: ".$log_clave;

			$log=$this->M_main->m_login($log_user,$log_clave);

			if ($log!=null) {//si se encuentra usuario y clave coinciden en la BD
				//echo "nombre: ".$log->idusuario;
				
				$sesion_login = array(//se crea el array asociativo de sesion
										'login' => true,
										'tipo_id' => $log->tipou_id,
										'tipo_perfil' => $log->tipou_perfil,
										'usuario_id' => $log->usuario_id,
										'usuario_user' =>$log->usuario_user,
										'usuario_nombre' =>$log->usuario_nombre
									);

				$this->session->set_userdata($sesion_login);
				$this->session->userdata('usuario_id');

				//echo " - sesion: ".$this->session->userdata('login');;
				if ($this->session->userdata('tipo_id')==1) {//administrador
					redirect(base_url().'c_admin/','refresh');
				}elseif ($this->session->userdata('tipo_id')==2) {//patinador
                    
					redirect(base_url().'c_patinador/','refresh');
				}elseif ($this->session->userdata('tipo_id')==3) {//almacen	
					redirect(base_url().'c_almacen/','refresh');
				}elseif ($this->session->userdata('tipo_id')==4) {//materiales	
					redirect(base_url().'c_material/','refresh');
				}
			}else{

				$resu="Usuario y/o Clave Incorrecto.";
				
				redirect(base_url().'main','refresh');
			}
		}

		public function logout(){
			$this->session->sess_destroy();

			redirect('main','refresh');
		}
	
	}
	
	/* End of file main.php */
	/* Location: ./application/controllers/main.php */
?>