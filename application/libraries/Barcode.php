<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH."third_party/code128/code128.php");

class Barcode
{
	protected $ci;

	public function __construct($params)
	{
        $this->ci =& get_instance();
        $pdf=new PDF_Code128();
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);
		$code=$params['codbar'];
		$veces=$params['veces'];
		$posicion = 20 ;
		for ($i=0 ; $i < $veces ; $i++) { 
			if (fmod($i, 8) == 0) {
				$posicion = 20;
			}
		$pdf->Code128(50,$posicion,$code,100,25);
		$posicion = $posicion + 40;
		$pdf->SetXY(57,$posicion);
        // $pdf->Ln();

		$pdf->Write(-25,'Magnetron S.A.S  Codigo: "'.$code.'"');
       
}
		ob_end_clean();
		$pdf->Output();
	}
}

/* End of file Barcode.php */
/* Location: ./application/libraries/Barcode.php */
