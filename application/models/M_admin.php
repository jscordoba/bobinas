<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

	public function list_seccion(){
		try {
			$query_list = $this->db->get('seccion');
			
			return $query_list;

		} catch (Exception $e) {
			return null;			
		}
	}

	public function ver_user(){
		try {
			$query_user="SELECT us.usuario_id,us.usuario_nombre,us.usuario_user,us.usuario_clave,se.seccion_id,se.seccion_nombre,tu.tipou_perfil, us.estado
						FROM usuario us
						JOIN seccion se ON se.seccion_id=us.seccion_id
						JOIN tipo_usuario tu ON tu.tipou_id=us.tipou_id ";

			return $this->db->query($query_user);
			
		} catch (Exception $e) {
			return null;
		}
	}
	public function delete_user($data){
		
try {
	$query_user = "UPDATE usuario set estado = 2 where usuario_id = '$data' ";
		if ($this->db->query($query_user)) {
				return "ok";
			}
} catch (Exception $e) {
	
}
    
	}
		public function editar1(){

    $id = $this->input->get_post  =$this->input->get_post('id_user');
    $nombre = $this->input->get_post  =$this->input->get_post('nombre_user');
    $usuario = $this->input->get_post  =$this->input->get_post('usuario_user');
    $clave = $this->input->get_post  =$this->input->get_post('clave_user');
    $seccion = $this->input->get_post  =$this->input->get_post('user_seccion');
    $tipo = $this->input->get_post  =$this->input->get_post('tipo_user');
    $estado = $this->input->get_post  =$this->input->get_post('estado');


    try {
			$query_ins="UPDATE usuario set  usuario_nombre = '$nombre', usuario_user = '$usuario', usuario_clave = '$clave', 
			seccion_id = '$seccion' , tipou_id = '$tipo' , estado = '$estado' where usuario_id = '$id'  ";
			
			if ($this->db->query($query_ins)) {
				return "ok";
			}
		} catch (Exception $e) {
			
		}
    

	}

	public function ver_maquina(){
		try {
			$query_user="SELECT mq.maquina_id,mq.maquina_nombre,se.seccion_nombre, mq.estado
						FROM maquina mq
						JOIN seccion se ON se.seccion_id=mq.seccion_id ";

			return $this->db->query($query_user);
			
		} catch (Exception $e) {
			return null;
		}
	}
	public function delete_station($data){
		
try {
	$query_user = "UPDATE maquina set estado = 2 where maquina_id = '$data' ";
		if ($this->db->query($query_user)) {
				return "ok";
			}
} catch (Exception $e) {
	
}
    
	}
public function editar2(){

    $id = $this->input->get_post  =$this->input->get_post('id_usuario');
    $nombre = $this->input->get_post  =$this->input->get_post('nombre_user');
    $seccion = $this->input->get_post  =$this->input->get_post('user_seccion');
    $estado = $this->input->get_post  =$this->input->get_post('estado');

    

    try {
			$query_ins="UPDATE maquina set  maquina_nombre = '$nombre', seccion_id = '$seccion' , estado=  '$estado' where maquina_id = '$id'  ";
			
			if ($this->db->query($query_ins)) {
				return "ok";
			}
		} catch (Exception $e) {
			
		}
    

	}
	public function new_user(){
		$id_usuario=$this->input->get_post('id_usuario');
		$nombre_usuario=$this->input->get_post('nombre_usuario');
		$usuario=$this->input->get_post('usuario');
		$clave=$this->input->get_post('clave');
		$tipo_user=$this->input->get_post('tipo_user');
		$user_seccion=$this->input->get_post('user_seccion');
		$estado = 1;
		$numero_filas = $this->db->query("SELECT * from usuario where usuario_user = '$usuario'")->num_rows();
		$numero_filas2 = $this->db->query("SELECT * from usuario where usuario_id = '$id_usuario'")->num_rows();

		
       

		 if ($numero_filas > 0) { 

                        return "usuario";
                }
                else  if ($numero_filas2 > 0) { 

                        return "id";
                }

                else{

		try {
			$query_ins="INSERT INTO usuario VALUES ($id_usuario,'$nombre_usuario',$user_seccion,$tipo_user,'$usuario','$clave', '$estado')";
			
			if ($this->db->query($query_ins)) {
				return "ok";
			}
		} catch (Exception $e) {
			
		}
	}
}

	public function new_station(){
		$maquina_nombre=$this->input->get_post('maquina_nombre');
		$maquina_seccion=$this->input->get_post('maquina_seccion');
		$estado = 1;
		$numero_filas = $this->db->query("SELECT * from maquina where maquina_nombre = '$maquina_nombre'")->num_rows();
		
       

		 if ($numero_filas > 0) { 

                        return "maquina";
                }
                else{

		try {
			$query_ins="INSERT INTO maquina VALUES (null,'$maquina_nombre',$maquina_seccion,$estado)";
			
			if ($this->db->query($query_ins)) {
				return "ok";
			}
		} catch (Exception $e) {
			
		}

	}
}
public function delete_section($data){
		
try {
	$query_user = "UPDATE seccion set estado = 2 where seccion_id = '$data' ";
		if ($this->db->query($query_user)) {
				return "ok";
			}
} catch (Exception $e) {
	
}
    

	}
	public function editar3(){

    $id = $this->input->get_post  =$this->input->get_post('id_seccion');
    $nombre = $this->input->get_post  =$this->input->get_post('nombre_seccion');
    $estado = $this->input->get_post  =$this->input->get_post('estado');

    

    try {
			$query_ins="UPDATE seccion set  seccion_nombre = '$nombre' , estado = '$estado' where seccion_id = '$id'  ";
			
			if ($this->db->query($query_ins)) {
				return "ok";
			}
		} catch (Exception $e) {
			
		}
    

	}
	public function new_section(){
		
		$maquina_nombre=$this->input->get_post('maquina_nombre');
		$estado = 1;
		$numero_filas = $this->db->query("SELECT * from seccion where seccion_nombre = '$maquina_nombre'")->num_rows();
		
       

		 if ($numero_filas > 0) { 

                        return "seccion";
                }
                else{
		try {
			$query_ins="INSERT INTO seccion VALUES (null,'$maquina_nombre', '$estado')";
			
			if ($this->db->query($query_ins)) {
				return "ok";
			}
		} catch (Exception $e) {
			
		}
	}


}
	public function list_inv(){
		try {
			$query_user="SELECT s.*, b.bodega_nombre from stock_material s
						JOIN bodega b on b.bodega_id = s.bodega_id
						 ";

			return $this->db->query($query_user);
			
		} catch (Exception $e) {
			return null;
		}
	}


	public function edit_inv($id, $cantidad,$bodega)
	{
		try {
			  $data = array('stockm_cantidad'=>$cantidad, 'bodega_id'=>$bodega);
			$this->db->where('stockm_id', $id); 
        	return $this->db->update('stock_material',$data);
		} catch (Exception $e) {
			return false;
		}
	}

	public function verify($codigo){
		$this->db->where('material_id',$codigo);
		$result = $this->db->get('material');
		if ($result->num_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}

	public function reg_material($codigo, $nombre){
		$data = array('material_id' => $codigo ,'material_descripcion' => $nombre );
		return $this->db->insert('material', $data);
	}

	public function reg_invt($codigo,$nombre,$cantidad,$bodega,$movimiento){
		$data = array('material_id'=>$codigo,'material_descripcion'=>$nombre,'stockm_cantidad'=>$cantidad, 'bodega_id'=>$bodega, 'movimiento_id'=>$movimiento);
		return $this->db->insert('stock_material', $data);
	}

}


	
/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */