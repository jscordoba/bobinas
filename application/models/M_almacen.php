﻿<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_almacen extends CI_Model
{

    public function indicador($tipo_indicador)
    {
        switch ($tipo_indicador) {
            case 'pdt':
                $query = "SELECT COUNT(*) as num_sol
                        FROM solicitud_material sol
                        WHERE sol.solicitudm_estado = 'PENDIENTE' ";
                break;
            case 'asig':
                $query = "SELECT COUNT(*) as num_sol
                        FROM solicitud_material sol
                        WHERE sol.solicitudm_estado='ENTREGADO'";
                break;
            case 'sol_rein':
                $query = "SELECT COUNT(*) as num_sol
                        FROM solicitud_reintegro sol
                        WHERE sol.solicitudr_estado='PENDIENTE'";
                break;
            case 'recib_rein':
                $query = "SELECT COUNT(*) as num_sol
                        FROM movimiento_material mov
                        WHERE mov.bodega_id_origen!=3
                        AND mov.bodega_id_destino=1";
                break;
            case 'num_material':
                $query = "SELECT COUNT(*) as num_sol
                        FROM stock_material where bodega_id = 1";
                break;
            case 'num_mov':
                $query = "SELECT COUNT(*) as num_sol
                        FROM movimiento_material mm
                        WHERE mm.bodega_id_origen=1 OR mm.bodega_id_destino=1";
                break;
            case 'date_sinc':
                $query = "SELECT MAX(mm.movimiento_date) as num_sol
                        FROM movimiento_material mm
                        WHERE mm.bodega_id_origen=3
                        AND mm.bodega_id_destino=1";
                break;
            case 'ult_sinc':
                $query = "SELECT fecha as num_sol FROM ult_sinc where id = 1";
                break;

            default:
                break;
        }

        try {
            $query     = $this->db->query($query);
            $indicador = $query->row();
            return $indicador->num_sol;

        } catch (Exception $e) {
            return null;
        }
    }
    public function ver_stock_import()
    {
        try {
            $query_stock = $this->db->query("SELECT *,(SELECT COUNT(movimiento_id)
                                                     FROM detalle_movimiento
                                                     WHERE movimiento_id=sm.movimiento_id
                                                     AND cantidad!=0) as unidades
                                            FROM stock_material sm where bodega_id = 1
                                            ");

            return $query_stock;
        } catch (Exception $e) {
            return null;
        }
    }
    public function import_file($ruta)
    {
        $count                 = 0;
        $insertadas_material   = 0;
        $actualizadas_material = 0;
        $insertadas_stock      = 0;
        $actualizadas_stock    = 0;
        $insertadas_bandejas   = 0;
        $actualizadas_bandejas = 0;
        $fp                    = fopen($ruta, 'r') or die("can't open file");
        $ots                   = array();
        while ($csv_line = fgetcsv($fp)) {
            array_push($ots, $csv_line[0]);
        }
        $save = array_unique($ots);
        foreach ($save as $row) {
            $key         = (int) $row;
            $descripcion = 'BANDEJA - ' . $key;
            $material    = $this->db->query("SELECT material_id FROM material where material_id = '$key' ")->num_rows();

            if ($material > 0) {
                $actualizadas_material++;
                $sql = $this->db->query("UPDATE material set material_descripcion = '$descripcion' where material_id = '$key'");
                if ($sql) {

                    $stockm_verificacion = $this->db->query("SELECT stockm_id from stock_material where material_id = '$key' and bodega_id = 1")->num_rows();

                    if ($stockm_verificacion > 0) {

                        $actualizadas_stock++;
                        $sql1 = $this->db->query("UPDATE stock_material set material_descripcion = '$descripcion'");
                        if ($sql1) {
                            $fp1 = fopen($ruta, 'r') or die("can't open file");
                            while ($csv_line2 = fgetcsv($fp1, 5000, ",")) {
                                $insert_csv2                         = array();
                                $insert_csv2['stockm_id']            = $csv_line2[0];
                                $insert_csv2['material_id']          = $csv_line2[1];
                                $insert_csv2['material_descripcion'] = $csv_line2[2];
                                $insert_csv2['bodega']               = $csv_line2[3];
                                $insert_csv2['cantidad']             = $csv_line2[4];
                                $stockm_id                           = $insert_csv2['stockm_id'];
                                $material_id                         = $insert_csv2['material_id'];
                                $material_descripcion                = $insert_csv2['material_descripcion'];
                                $bodega                              = $insert_csv2['bodega'];
                                $cantidad                            = $insert_csv2['cantidad'];
                                if ((int) $stockm_id == $key) {
                                    $id                   = (int) $stockm_id;
                                    $bandeja_verificacion = $this->db->query("SELECT id from bandejas where descripcion = '$material_descripcion' and id_material = '$id' ")->num_rows();
                                    if ($bandeja_verificacion > 0) {
                                        $actualizadas_bandejas++;
                                        $sql2 = $this->db->query("UPDATE bandejas set bodega = '$bodega', cantidad = $cantidad where descripcion = '$material_descripcion' and id_material = '$id'  ");
                                    } else {

                                        $insertadas_bandejas++;
                                        $sql2 = $this->db->query("INSERT INTO bandejas VALUES (NULL, '$material_id', '$material_descripcion', '$bodega',$cantidad, $id)");
                                    }
                                }
                            }
                        }
                    } else {
                        $insertadas_stock++;
                        $sql = $this->db->query("INSERT INTO stock_material VALUES (NULL, '$key' , '$descripcion', '1','1','0')");
                        if ($sql) {
                            $fp = fopen($ruta, 'r') or die("can't open file");
                            while ($csv_line2 = fgetcsv($fp, 5000, ",")) {
                                $insert_csv2                         = array();
                                $insert_csv2['stockm_id']            = $csv_line2[0];
                                $insert_csv2['material_id']          = $csv_line2[1];
                                $insert_csv2['material_descripcion'] = $csv_line2[2];
                                $insert_csv2['bodega']               = $csv_line2[3];
                                $insert_csv2['cantidad']             = $csv_line2[4];
                                $stockm_id                           = $insert_csv2['stockm_id'];
                                $material_id                         = (int) $insert_csv2['material_id'];
                                $material_descripcion                = $insert_csv2['material_descripcion'];
                                $bodega                              = $insert_csv2['bodega'];
                                $cantidad                            = $insert_csv2['cantidad'];
                                if ((int) $stockm_id == $key) {
                                    $id                   = (int) $stockm_id;
                                    $bandeja_verificacion = $this->db->query("SELECT id from bandejas where descripcion = '$material_descripcion' and id_material = '$id' ")->num_rows();
                                    if ($bandeja_verificacion > 0) {
                                        $actualizadas_bandejas++;
                                        $sql2 = $this->db->query("UPDATE bandejas set bodega = '$bodega', cantidad = $cantidad where descripcion = '$material_descripcion' and id_material = '$id'  ");
                                    } else {

                                        $insertadas_bandejas++;
                                        $sql2 = $this->db->query("INSERT INTO bandejas VALUES (NULL, '$material_id', '$material_descripcion', '$bodega',$cantidad, $id)");
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $insertadas_material++;
                $sql = $this->db->query("INSERT INTO material VALUES('$key','$descripcion')");
                if ($sql) {
                    $stockm_verificacion = $this->db->query("SELECT stockm_id from stock_material where material_id = '$key' and bodega_id = 1")->num_rows();
                    if ($stockm_verificacion > 0) {
                        $actualizadas_stock++;
                        $sql = $this->db->query("UPDATE stock_material set material_descripcion = '$descripcion'");
                        if ($sql) {
                            $fp = fopen($ruta, 'r') or die("can't open file");
                            while ($csv_line2 = fgetcsv($fp, 5000, ",")) {
                                $insert_csv2                         = array();
                                $insert_csv2['stockm_id']            = $csv_line2[0];
                                $insert_csv2['material_id']          = $csv_line2[1];
                                $insert_csv2['material_descripcion'] = $csv_line2[2];
                                $insert_csv2['bodega']               = $csv_line2[3];
                                $insert_csv2['cantidad']             = $csv_line2[4];
                                $stockm_id                           = $insert_csv2['stockm_id'];
                                $material_id                         = (int) $insert_csv2['material_id'];
                                $material_descripcion                = $insert_csv2['material_descripcion'];
                                $bodega                              = $insert_csv2['bodega'];
                                $cantidad                            = $insert_csv2['cantidad'];
                                if ((int) $stockm_id == $key) {
                                    $id                   = (int) $stockm_id;
                                    $bandeja_verificacion = $this->db->query("SELECT id from bandejas where descripcion = '$material_descripcion' and id_material = '$id' ")->num_rows();
                                    if ($bandeja_verificacion > 0) {
                                        $actualizadas_bandejas++;
                                        $sql2 = $this->db->query("UPDATE bandejas set bodega = '$bodega', cantidad = $cantidad where descripcion = '$material_descripcion' and id_material = '$id'  ");
                                    } else {

                                        $insertadas_bandejas++;
                                        $sql2 = $this->db->query("INSERT INTO bandejas VALUES (NULL, '$material_id', '$material_descripcion', '$bodega',$cantidad, $id)");
                                    }
                                }
                            }
                        }
                    } else {
                        $insertadas_stock++;
                        $sql = $this->db->query("INSERT INTO stock_material VALUES (NULL, '$key' , '$descripcion', '1','1','0')");

                        if ($sql) {
                            $fp = fopen($ruta, 'r') or die("can't open file");
                            while ($csv_line2 = fgetcsv($fp, 5000, ",")) {
                                $insert_csv2                         = array();
                                $insert_csv2['stockm_id']            = $csv_line2[0];
                                $insert_csv2['material_id']          = $csv_line2[1];
                                $insert_csv2['material_descripcion'] = $csv_line2[2];
                                $insert_csv2['bodega']               = $csv_line2[3];
                                $insert_csv2['cantidad']             = $csv_line2[4];
                                $stockm_id                           = $insert_csv2['stockm_id'];
                                $material_id                         = (int) $insert_csv2['material_id'];
                                $material_descripcion                = $insert_csv2['material_descripcion'];
                                $bodega                              = $insert_csv2['bodega'];
                                $cantidad                            = $insert_csv2['cantidad'];
                                if ((int) $stockm_id == $key) {
                                    $id                   = (int) $stockm_id;
                                    $bandeja_verificacion = $this->db->query("SELECT id from bandejas where descripcion = '$material_descripcion' and id_material = '$id' ")->num_rows();
                                    if ($bandeja_verificacion > 0) {
                                        $actualizadas_bandejas++;
                                        $sql2 = $this->db->query("UPDATE bandejas set bodega = '$bodega', cantidad = $cantidad where descripcion = '$material_descripcion' and id_material = '$id'  ");
                                    } else {

                                        $insertadas_bandejas++;
                                        $sql2 = $this->db->query("INSERT INTO bandejas VALUES (NULL, '$material_id', '$material_descripcion', '$bodega',$cantidad, $id)");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        fclose($fp) or die("can't close file");
        return "Material : Insertadas : " . $insertadas_material . " Actualizadas : " . $actualizadas_material . " Stock : Insertadas : " . $insertadas_stock . " Actualizadas : " . $actualizadas_stock . " Insertadas Bandejas : " . $insertadas_bandejas . " Actualizadas : " . $actualizadas_bandejas;
    }
    public function ver_stock()
    {
        try {
            $query_stock = $this->db->query("SELECT *,(SELECT COUNT(movimiento_id)
                                                     FROM detalle_movimiento
                                                     WHERE movimiento_id=sm.movimiento_id
                                                     AND cantidad!=0) as unidades
                                            FROM stock_material sm
                                            WHERE sm.bodega_id=1");

            return $query_stock;
        } catch (Exception $e) {
            return null;
        }
    }

    public function sol_pendientes()
    {
        try {
            $query_sol = $this->db->query("SELECT mat.material_id,mat.material_descripcion, sol.solicitudm_id,sol.solicitudm_cantidad,(
                                                                                                                                        SELECT ( CASE WHEN SUM(stockm_cantidad) = 0  || SUM(stockm_cantidad) = null
                                                                                                                                                    THEN 0 
                                                                                                                                                    ELSE SUM(stockm_cantidad) 
                                                                                                                                                END) AS stock
                                                                                                                                        FROM stock_material 
                                                                                                                                        WHERE   material_id = sol.material_id
                                                                                                                                        AND     bodega_id = 1 ) as stockm_cantidad, 
                                        sol.usuario_id_solicitante,sol.solicitudm_date,sol.solicitudm_estado, sol.tipo, sol.imprevistos
                                        FROM solicitud_material sol
                                        JOIN material mat ON sol.material_id=mat.material_id
                                        WHERE sol.solicitudm_estado='PENDIENTE' AND sol.solicitudm_cantidad > 0


                                        /*SELECT st.material_id,st.material_descripcion,st.stockm_cantidad,st.bodega_id,sol.solicitudm_id,sol.solicitudm_cantidad,sol.usuario_id_solicitante,sol.solicitudm_date,sol.solicitudm_estado, sol.tipo, sol.imprevistos
                                        FROM solicitud_material sol
                                        JOIN stock_material st ON st.material_id=sol.material_id
                                        WHERE sol.solicitudm_estado='PENDIENTE' AND sol.solicitudm_cantidad > 0
                                        AND st.bodega_id=1
                                        */");

            return $query_sol;

        } catch (Exception $e) {
            return null;
        }
    }
    public function sol_alistar()
    {
        try {
            $query_sol = $this->db->query("
                                        SELECT mat.material_id, mat.material_descripcion, sol.solicitudm_cantidad as stockm_cantidad,sol.solicitudm_id,sol.solicitudm_cantidad,sol.usuario_id_solicitante,sol.solicitudm_date,sol.solicitudm_estado, sol.tipo, sol.imprevistos, sol.movimiento_id, sol.id_detalle
                                        FROM solicitud_material_temp sol
                                        JOIN material mat ON sol.material_id=mat.material_id
                                        WHERE sol.solicitudm_estado='PENDIENTE'
                                        
                                        /*SELECT st.material_id,st.material_descripcion,st.stockm_cantidad,st.bodega_id,sol.solicitudm_id,sol.solicitudm_cantidad,sol.usuario_id_solicitante,sol.solicitudm_date,sol.solicitudm_estado, sol.tipo, sol.imprevistos, sol.movimiento_id, sol.id_detalle
                                                                FROM solicitud_material_temp sol
                                        JOIN stock_material st ON st.material_id=sol.material_id
                                        WHERE sol.solicitudm_estado='PENDIENTE'
                                        AND st.bodega_id=1
                                        */");

            return $query_sol;

        } catch (Exception $e) {
            return null;
        }
    }
    public function alistar_prod($id, $pati, $date_actual, $user_id, $mat_id, $mat_descripcion, $imprevistos, $movimiento, $tipo)
    {

        try {
            $bod_inicio = 1;
            $bod_fin    = 2;
            try {
                $t_cantidad   = 0;
                $cantidad     = 0;
                $query_origen = $this->db->query("SELECT *
                            FROM detalle_material
                            WHERE id_detalle =$id
                            ");
                foreach ($query_origen->result() as $row) {
                    $cantidad += $t_cantidad + $row->cantidad;
                }

                $query_ins_mov = "INSERT INTO movimiento_material VALUES (NULL,$mat_id,$cantidad,$user_id,$bod_inicio,$bod_fin,'$date_actual',$tipo)";
                $this->db->query($query_ins_mov);
                $mov_id         = $this->db->insert_id();
                $total_cantidad = 0;
                $veces          = 0;
            } catch (Exception $e) {
                return null;
            }
            try {
                $total_cantidad = $cantidad;
                $query_sol      = $this->db->query("UPDATE solicitud_material_temp sm
                                            INNER JOIN detalle_material dma
                                            ON dma.id_detalle = sm.solicitudm_id
                                            INNER JOIN detalle_movimiento dmo
                                            ON dmo.id_detalle = sm.solicitudm_id
                                            SET sm.solicitudm_estado = 'ENTREGADO',
                                                sm.solicitudm_cantidad = 0,
                                                sm.movimiento_id = '$mov_id',
                                                dma.patinador = '$pati',
                                                dma.movimiento_id = '$mov_id',
                                                dmo.patinador = '$pati',
                                                dmo.movimiento_id = '$mov_id'
                                            WHERE sm.solicitudm_id = '$id'");
            } catch (Exception $e) {
                echo "Error de insersión en detalle_movimiento.";
            }
            $query_origen = "SELECT stock_material.stockm_cantidad
                            FROM stock_material
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_inicio";
            $query_destino = "SELECT stock_material.stockm_cantidad
                            FROM stock_material
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_fin";
            $stock_origen  = $this->db->query($query_origen);
            $stock_destino = $this->db->query($query_destino);

            if ($stock_origen->num_rows() > 0) {
                $row1         = $stock_origen->row();
                $stock_origen = $row1->stockm_cantidad;

                if ($stock_origen >= $total_cantidad) {

                    $new_stock_origen = $stock_origen - $total_cantidad;
                    $query3           = "UPDATE stock_material
                            SET stock_material.stockm_cantidad=$new_stock_origen
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_inicio";
                    $update_stock = $this->db->query($query3);
                }

                if (($stock_destino->num_rows() > 0)) {
                    $row2              = $stock_destino->row();
                    $stock_destino     = $row2->stockm_cantidad;
                    $new_stock_destino = $stock_destino + $total_cantidad;
                    $query4            = "UPDATE stock_material
                            SET stock_material.stockm_cantidad=$new_stock_destino,
                                stock_material.movimiento_id=$mov_id
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_fin";
                    $stock = $this->db->query($query4);

                } else {
                    $query5 = "INSERT INTO stock_material
                            VALUES (NULL,$mat_id,'$mat_descripcion',$total_cantidad, $bod_fin,$mov_id)";
                    $stock = $this->db->query($query5);
                }
            } else {
                //return null;
            }
            $query6 = "INSERT INTO stock_material_temp
                            VALUES (NULL,$mat_id,'$mat_descripcion',$total_cantidad,$bod_fin, $mov_id)";
            $this->db->query($query6);
            $query7  = "SELECT * FROM solicitud_material_temp WHERE solicitudm_estado = 'PENDIENTE' AND id_detalle = $movimiento ";
            $validar = $this->db->query($query7);
            if (($validar->num_rows() > 0)) {
                return 1;
            } else {
                $query8 = "UPDATE solicitud_material SET solicitudm_estado = 'ENTREGADO' WHERE solicitudm_id = '$movimiento'";
                if ($this->db->query($query8)) {
                    return 1;
                } else {
                    return 2;
                }
            }
        } catch (Exception $e) {
            return 2;
        }
    }
    public function alistar_detalles($movimiento)
    {
        $movimiento = $movimiento;

        try {
            $query_sol = $this->db->query("SELECT dm.* FROM detalle_material dm
                                            where dm.id_detalle = $movimiento");
            return $query_sol;
        } catch (Exception $e) {
            return null;
        }
    }
    public function sol_entregados()
    {
        try {
            $query_sol = $this->db->query("SELECT DISTINCT ma.material_id,ma.material_descripcion,mov.* ,sm.imprevistos, sm.solicitudm_estado, sm2.solicitudm_date as fecha
                                            FROM movimiento_material mov
                                            JOIN material ma ON ma.material_id=mov.material_id
                                            JOIN solicitud_material_temp sm ON sm.movimiento_id =mov.movimiento_id
                                            JOIN solicitud_material sm2 on sm2.solicitudm_id = sm.id_detalle
                                            WHERE mov.bodega_id_origen=1 and sm.solicitudm_estado = 'ENTREGADO'
                                            ORDER BY mov.movimiento_id DESC");

            return $query_sol;
        } catch (Exception $e) {
            return null;
        }
    }

    public function sol_reintegros()
    {
        try {
            $query_sol = $this->db->query("SELECT DISTINCT ma.material_id,ma.material_descripcion,sr.* ,mov.bodega_id_destino,de.patinador, sr.movimiento_id
                                            FROM solicitud_reintegro sr
                                            JOIN material ma ON ma.material_id=sr.material_id
                                            JOIN movimiento_material mov ON mov.movimiento_id= sr.movimiento_id
                                            JOIN detalle_material de ON de.movimiento_id=sr.movimiento_id
                                            WHERE sr.solicitudr_estado='PENDIENTE'
                                            ORDER BY sr.solicitudr_id DESC");

            return $query_sol;
        } catch (Exception $e) {
            return null;
        }
    }

    public function hist_reintegros()
    {
        try {
            $query_rein = $this->db->query("SELECT ma.material_id,ma.material_descripcion,mov.*
                                            FROM movimiento_material mov
                                            JOIN material ma ON ma.material_id=mov.material_id
                                            WHERE mov.bodega_id_origen!=3
                                            AND mov.bodega_id_destino=1
                                            ORDER BY mov.movimiento_id DESC");
            return $query_rein;
        } catch (Exception $e) {
            return null;
        }
    }

    public function generarCodigo($b, $c, $d)
    {
        $fecha = intval(preg_replace('/[^0-9]+/', '', $d), 10);
        $key   = $b . $c . $fecha;
        return $key;
    }

    public function reg_traslado($sol_codbar_ot, $mat_id, $mat_cantidad, $mat_descripcion, $date_actual, $und_nueva, $unds_actual, $stock_cantidad, $cantidad_und, $presentacion, $perfil, $bod_maquina_origen, $bod_maquina_fin, $user_id, $tipo, $imprevistos, $codigo)
    {
        $bod_inicio = $bod_maquina_origen;
        $bod_fin    = $bod_maquina_fin;
        $user_id = $this->session->userdata('usuario_id');
        
        try {
            $c          = 0;
            $t_cantidad = 0;
            foreach ($und_nueva as $detalle_id => $unds_nueva) {
//recorrer array de unidades seleccionadas
                $t_cantidad += $cantidad_und[$c++] * $unds_nueva;
            }
            foreach ($und_nueva as $detalle_id => $unds_nueva) {
//recorrer array de unidades seleccionadas
                $unidad_nueva[] = $unds_nueva;
                $detail_id[]    = $detalle_id;
            }
            $total_cantidad = 0;
            $veces          = 0;
            $query7         = "UPDATE solicitud_material SET  imprevistos = '$imprevistos', solicitudm_cantidad = solicitudm_cantidad - $t_cantidad  where solicitudm_id = $sol_codbar_ot ";
            $this->db->query($query7);
            $query8 = "INSERT INTO solicitud_material_temp VALUES (null,$mat_id,$t_cantidad,$user_id,'$date_actual','PENDIENTE',null ,$tipo,'Sin imprevistos',$sol_codbar_ot)";
            $this->db->query($query8);
            $mov_id = $this->db->insert_id();

            for ($i = 0; $i < count($und_nueva); $i++) {
                //recorrer todos los array y calcular e insertar nuevo detalle movimiento
                try {
                    $res_unds = $unds_actual[$i] - $unidad_nueva[$i]; // sacamos el valor de las unidades que quedan al trasladar a otra bod
                    $total_cantidad += $cantidad_und[$i] * $unidad_nueva[$i];
                    $ins_detail = "INSERT INTO detalle_movimiento
                                    VALUES(null,null,$unidad_nueva[$i],$cantidad_und[$i],'$presentacion[$i]',NULL, '$codigo[$i]', '1', $mov_id)";
                    $ins_detail_mat = "INSERT INTO  detalle_material
                                    VALUES(null,null,$unidad_nueva[$i],$cantidad_und[$i],'$presentacion[$i]',NULL, '$codigo[$i]', '1', $mov_id)";
                    $stock_codigo = "UPDATE stock_codigo
                                    SET sc_cantidad = 0
                                    WHERE codbar = '$codigo[$i]' ";
                    $update1     = $this->db->query($stock_codigo);
                    $insert      = $this->db->query($ins_detail);
                    $insert_dmat = $this->db->query($ins_detail_mat);
                    $veces       = $veces + $unidad_nueva[$i];
                } catch (Exception $e) {
                    echo "Error de insersión en detalle_movimiento.";
                }
            }
            return array('ok');
        } catch (Exception $e) {
            return null;
        }
    }
    public function reg_traslado2($sol_codbar_ot, $mat_id, $mat_cantidad, $mat_descripcion, $date_actual, $und_nueva, $unds_actual, $stock_cantidad, $cantidad_und, $perfil, $bod_maquina_origen, $bod_maquina_fin, $user_id, $tipo, $imprevistos, $codigo, $presentacion)
    {
        $bod_inicio = $bod_maquina_origen;
        $bod_fin    = $bod_maquina_fin;
        try {
            $c          = 0;
            $t_cantidad = 0;
            foreach ($und_nueva as $detalle_id => $unds_nueva) {
//recorrer array de unidades seleccionadas
                $t_cantidad += $cantidad_und[$c++] * $unds_nueva;
            }
            foreach ($und_nueva as $detalle_id => $unds_nueva) {
//recorrer array de unidades seleccionadas
                $unidad_nueva[] = $unds_nueva;
                $detail_id[]    = $detalle_id;
            }
            $total_cantidad = 0;
            $veces          = 0;
            $query7         = "UPDATE solicitud_material SET  imprevistos = '$imprevistos', solicitudm_cantidad = solicitudm_cantidad - $unds_nueva  where solicitudm_id = $sol_codbar_ot ";
            $this->db->query($query7);
            $query8 = "INSERT INTO solicitud_material_temp VALUES (null,$mat_id,$unds_nueva,$user_id,'$date_actual','PENDIENTE',null ,2,'Sin imprevistos',$sol_codbar_ot)";
            $this->db->query($query8);
            $mov_id = $this->db->insert_id();

            for ($i = 0; $i < count($und_nueva); $i++) {
                //recorrer todos los array y calcular e insertar nuevo detalle movimiento
                try {
                    $res_unds = $unds_actual[$i] - $unidad_nueva[$i]; // sacamos el valor de las unidades que quedan al trasladar a otra bod
                    $total_cantidad += $cantidad_und[$i] * $unidad_nueva[$i];
                    $ins_detail = "INSERT INTO detalle_movimiento
                                    VALUES(null,null,1,$unidad_nueva[$i],'$presentacion[$i]',NULL, '$codigo[$i]', '1', $mov_id)";
                    $ins_detail_mat = "INSERT INTO  detalle_material
                                    VALUES(null,null,1,$unidad_nueva[$i],'$presentacion[$i]',NULL, '$codigo[$i]', '1', $mov_id)";
                    $stock_codigo = "UPDATE stock_codigo
                                    SET sc_cantidad = 0
                                    WHERE codbar = '$codigo[$i]' ";
                    $update1     = $this->db->query($stock_codigo);
                    $insert      = $this->db->query($ins_detail);
                    $insert_dmat = $this->db->query($ins_detail_mat);
                    $veces       = $veces + $unidad_nueva[$i];
                } catch (Exception $e) {
                    echo "Error de insersión en detalle_movimiento.";
                }
            }
            return array('ok');
        } catch (Exception $e) {
            return null;
        }
    }

    //-------------- TRASLADA UNIDADES DESDE BOD_PROD A BOD_PPAL -----------------------------///
    public function reg_traslado_prod($sol_rein_id, $mat_id, $mat_desc, $mat_cant, $date_actual, $tipo, $bodega, $patinador)
    {
        $user_id = $this->session->userdata('usuario_id');
        try {

            //-------------- INSERTA EN LA TABLA MOVIMIENTOS GENERALES DE MATERIAL  -----------------------------///
            $query_ins_mov = "INSERT INTO movimiento_material VALUES (NULL,$mat_id,$mat_cant,$user_id,$bodega,1,'$date_actual', $tipo)";

            $this->db->query($query_ins_mov);

            $mov_id = $this->db->insert_id();

            for ($i = 0; $i < count($mat_cant); $i++) {
                if ($tipo == 2) {
                    try {

                        $upd_detail = "UPDATE detalle_movimiento
                                    SET num_unidad=$mat_cant
                                    WHERE dm_id=0";

                        $ins_detail = "INSERT INTO detalle_movimiento
                                    VALUES(null,$mov_id,$mat_cant,0,'REINTEGRO BANDEJA','$patinador',null, '3', null)";

                        $ins_detail_mat = "INSERT INTO  detalle_material
                                    VALUES(null,$mov_id,$mat_cant,0,'REINTEGRO BANDEJA','$patinador',null, '3', null)";

                        $update      = $this->db->query($upd_detail);
                        $insert      = $this->db->query($ins_detail);
                        $insert_dmat = $this->db->query($ins_detail_mat);

                    } catch (Exception $e) {
                        echo "Error de insersión en detalle_movimiento.";
                    }
                } else {
                    try {

                        $upd_detail = "UPDATE detalle_movimiento
                                    SET num_unidad=$mat_cant
                                    WHERE dm_id=0";

                        $ins_detail = "INSERT INTO detalle_movimiento
                                    VALUES(null,$mov_id,0,$mat_cant,'REINTEGRO NORMAL','$patinador', null, '3', null)";

                        $ins_detail_mat = "INSERT INTO  detalle_material
                                    VALUES(null,$mov_id,0,$mat_cant,'REINTEGRO NORMAL','$patinador',null, '3', null)";

                        $update      = $this->db->query($upd_detail);
                        $insert      = $this->db->query($ins_detail);
                        $insert_dmat = $this->db->query($ins_detail_mat);

                    } catch (Exception $e) {
                        echo "Error de insersión en detalle_movimiento.";
                    }
                }

            }
            $mov_id = $this->db->insert_id();

            //-------------- ACTUALIZA EL STOCK DE MATERIAL EN BOD DESTINO (BOD_PPAL) -----------------------------///
            //---------------------------- BUSCAR CANTIDAD DE ITEM DESTINO ----------------------------------//
            
            $query_destino = "SELECT stock_material.stockm_cantidad
                            FROM stock_material
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=1";
            $stock_destino = $this->db->query($query_destino);
            
            //---------------------------- SE SUMAN CANTIDADES EN STOCK Y SE ACTUALIZA STOCK DESTINO (BOD_PPAL) ----------------------------------//
           
            $stock_destino = $stock_destino->row_array();
            $new_stock     = $stock_destino['stockm_cantidad'] + $mat_cant;
            $query4        = "UPDATE stock_material
                    SET stock_material.stockm_cantidad=$new_stock,
                        stock_material.movimiento_id=$mov_id
                    WHERE stock_material.material_id=$mat_id
                    AND stock_material.bodega_id=1";
            $stock = $this->db->query($query4);

            //-------------- ACTUALIZA EL STOCK DE MATERIAL EN BOD ORIGEN (BOD_PROD) -----------------------------///
            //---------------------------- BUSCAR CANTIDAD DE ITEM ORIGEN ----------------------------------//
            
            $query_origen = "SELECT stock_material.stockm_cantidad,stock_material.movimiento_id
                            FROM stock_material
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bodega";
            $stock_origen = $this->db->query($query_origen);
            
            //---------------------------- SE SUMAN CANTIDADES EN STOCK Y SE ACTUALIZA STOCK DESTINO (BOD_PPAL) ----------------------------------//

            $stock_origen     = $stock_origen->row_array();
            $movimiento       = $stock_origen['movimiento_id'];
            $new_stock_origen = $stock_origen['stockm_cantidad'] - $mat_cant;
            $query5           = "UPDATE stock_material
                    SET stock_material.stockm_cantidad=$new_stock_origen,
                        stock_material.movimiento_id=$mov_id
                    WHERE stock_material.material_id=$mat_id
                    AND stock_material.bodega_id=$bodega";
            $stock  = $this->db->query($query5);
            $query6 = "UPDATE stock_material_temp
                    SET stock_material_temp.stockm_cantidad=0,
                        stock_material_temp.movimiento_id=$mov_id
                    WHERE stock_material_temp.material_id=$mat_id
                    AND stock_material_temp.bodega_id=$bodega";
            $this->db->query($query6);
            
            //-------------- ACTUALIZAR EL ESTADO DE LA SOLICITUD DE REINTEGRO A FINALIZADA-----------------------------///
            
            $query_upd_sol = "UPDATE solicitud_reintegro
                            SET solicitudr_estado = 'FINALIZADO' , movimiento_id=$mov_id
                            WHERE solicitud_reintegro.solicitudr_id = $sol_rein_id;";
            $this->db->query($query_upd_sol);
            return "ok";
        } catch (Exception $e) {
            return null;
        }
    }
    //OBTENER CONTENIDO DE TABLA INDEX DE MOVIMIENTOS
    public function v_movimientos()
    {
        try {
            $query_mv = "SELECT mat.material_id,mat.material_descripcion,(SELECT bodega_nombre FROM bodega WHERE bodega_id=mv.bodega_id_origen) as bodega_origen,(SELECT bodega_nombre FROM bodega WHERE bodega_id=mv.bodega_id_destino) as bodega_destino, mv.movimiento_date,mv.movimiento_cantidad,mv.tipo
                        FROM movimiento_material mv
                        LEFT JOIN material mat ON mat.material_id=mv.material_id
                        LEFT JOIN bodega bod ON bod.bodega_id=mv.bodega_id_origen
                        WHERE mv.bodega_id_origen=1 || mv.bodega_id_destino=1
                        ORDER BY movimiento_id DESC
                        LIMIT 500";

            return $this->db->query($query_mv);
        } catch (Exception $e) {
            return null;
        }
    }

    //OBTENER CONTENIDO PARA TABLA DE DETALLES EN ORDENES DE TRABAJO
    public function get_detail_und($movimiento_id)
    {
        try {
            $query_mw2 = $this->db->query("SELECT sum(detalle_material.num_unidad) as total
                                        FROM detalle_material
                                        JOIN movimiento_material ON detalle_material.movimiento_id=movimiento_material.movimiento_id
                                        WHERE detalle_material.num_unidad!=0
                                        AND detalle_material.movimiento_id=$movimiento_id");
            $query_mv = $this->db->query("SELECT detalle_material.*, movimiento_material.*
                                        FROM detalle_material
                                        JOIN movimiento_material ON detalle_material.movimiento_id=movimiento_material.movimiento_id
                                        WHERE detalle_material.num_unidad!=0
                                        AND detalle_material.movimiento_id=$movimiento_id ");

            return array($query_mv, $query_mw2);
        } catch (Exception $e) {
            return null;
        }
    }

    //OBTENER DETALLE PARA ASIGNACION DE MATERIAL A UNA MAQUINA EN PATINADOR
    public function get_detail_traslado($mat_id, $bod, $movimiento)
    {
        try {
            // echo $mat_id."-".$bod."-".$movimiento; die();
            $query_mv = $this->db->query("SELECT *
                                        FROM detalle_movimiento
                                        JOIN movimiento_material ON detalle_movimiento.movimiento_id=movimiento_material.movimiento_id
                                        JOIN stock_codigo ON stock_codigo.codbar = detalle_movimiento.dmo_codbar
                                        WHERE detalle_movimiento.num_unidad!=0
                                        AND movimiento_material.bodega_id_destino=$bod
                                        AND movimiento_material.material_id=$mat_id
                                        AND movimiento_material.movimiento_id = $movimiento
                                        AND detalle_movimiento.movimiento_id = $movimiento ");

            return $query_mv;
        } catch (Exception $e) {
            return false;
        }
    }

    public function ver_maquina()
    {
        try {
            $query_estacion = $this->db->query("SELECT ma.maquina_id,ma.maquina_nombre,se.seccion_nombre
                                                FROM maquina ma
                                                JOIN seccion se ON se.seccion_id=ma.seccion_id where ma.estado = 1");

            return $query_estacion;
        } catch (Exception $e) {
            return null;
        }
    }

    //REGISTRAR UN NUEVO MATERIAL DE ENTRADA AL STOCK
    public function reg_mat($material_id, $material_desc, $material_cant)
    {
        try {

            //---------------------------- BUSCAR CANTIDAD DE ITEM DESTINO ----------------------------------//
            $query_exist = "SELECT *
                        FROM material
                        WHERE material_id=$material_id";

            $result_exist = $this->db->query($query_exist);

            if ($result_exist->num_rows() == 0 or $result_exist->num_rows() == null) {
                //-------------- INSERTA EN LA TABLA MATERIAL COMO UN MATERIAL NUEVO --------------------///
                
                $query_ins_mat = "INSERT INTO material VALUES ($material_id,'$material_desc')";
                $this->db->query($query_ins_mat);

                //echo "mat_new";
                return "mat_new";
            } else {
                //echo "mat_exist";
                return "mat_exist";
            }

        } catch (Exception $e) {
            return "$e";
        }
    }

    //REGISTRAR NUEVA CANTIDAD INSERTADA PARA EL ITEM REGISTRADO ANTERIORMENTE
    public function new_cant_material($material_id, $material_desc, $material_cant, $date_actual)
    {
        $user_id = $this->session->userdata('usuario_id');

        try {
            //-------------- INSERTA EN LA TABLA MOVIMIENTOS GENERALES DE MATERIAL  -----------------------------///
            $query_ins_mov = "INSERT INTO movimiento_material VALUES (NULL,$material_id,$material_cant,$user_id,3,1,'$date_actual','1')";

            $this->db->query($query_ins_mov);

            $mov_id = $this->db->insert_id();

            //---------------------------- SE ACTUALIZA STOCK DESTINO (ALMACEN) ----------------------------------//

            $query5 = "INSERT INTO stock_material
                    VALUES (NULL,$material_id,'$material_desc',$material_cant, 1 ,$mov_id)";

            $stock = $this->db->query($query5);

            //echo "ins_ok";
            return "ins_ok";

        } catch (Exception $e) {
            return "$e";
        }
    }

    //REGISTRAR NUEVA CANTIDAD ACTUALIZADA PARA EL ITEM REGISTRADO ANTERIORMENTE
    public function upd_cant_material($material_id, $material_desc, $material_cant, $date_actual)
    {
        $user_id = $this->session->userdata('usuario_id');

        try {
            //---------------------------- BUSCAR CANTIDAD DE ITEM DESTINO ----------------------------------//

            //-------------- INSERTA EN LA TABLA MOVIMIENTOS GENERALES DE MATERIAL  -----------------------------///
            $query_ins_mov = "INSERT INTO movimiento_material VALUES (NULL,$material_id,$material_cant,$user_id,3,1,'$date_actual','1')";

            $mov_id = $this->db->insert_id();

            $this->db->query($query_ins_mov);
            $query_destino = "SELECT stock_material.stockm_cantidad
                            FROM stock_material
                            WHERE stock_material.material_id=$material_id
                            AND stock_material.bodega_id=1";

            $stock_destino   = $this->db->query($query_destino);
            $row_stock       = $stock_destino->row();
            $stockm_cantidad = $row_stock->stockm_cantidad;
            
            //---------------------------- SE ACTUALIZA STOCK DESTINO (ALMACEN) ----------------------------------//
            $new_stock = $stockm_cantidad + $material_cant;
            $query4    = "UPDATE stock_material
                    SET stock_material.stockm_cantidad=$new_stock,
                        stock_material.movimiento_id=$mov_id
                    WHERE stock_material.material_id=$material_id
                    AND stock_material.bodega_id=1";

            $stock = $this->db->query($query4);
            return "upd_ok";
        } catch (Exception $e) {
            return "$e";
        }
    }

    public function cargar_material()
    {
        $query_ins = "INSERT INTO movimiento_material";
        $query_ins = "INSERT INTO detalle_movimiento";
        $query_ins = "INSERT INTO stock_material";
    }

    public function ver_estaciones()
    {
        try {
            $query_estacion = $this->db->query("SELECT ma.maquina_id,ma.maquina_nombre,se.seccion_nombre
                                                FROM maquina ma
                                                JOIN seccion se ON se.seccion_id=ma.seccion_id where ma.estado =1
                                                ");

            return $query_estacion;
        } catch (Exception $e) {
            return null;
        }
    }
    public function sincronizar_sqlserver($date_actual)
    {
        $serverName     = "200.100.9.9"; //serverName\instanceName
        $connectionInfo = array("Database" => "E10Produccion", "UID" => "Desarrollo", "PWD" => "desarrollo");
        $conn           = sqlsrv_connect($serverName, $connectionInfo);

        if ($conn === false) {
            die(print_r(sqlsrv_errors(), true));
        } else {
            $sql   = "SELECT * FROM ConsultaExistenciasConductores";
            $stmt  = sqlsrv_query($conn, $sql);
            $stmt1 = sqlsrv_query($conn, $sql);

            if ($stmt === false) {
                die(print_r(sqlsrv_errors(), true));
            } else {

                $actualizadas = 0;
                $insertadas   = 0;

                try {
                    while ($row_insert = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
                        
                        $Mysql_read   = "SELECT * FROM material WHERE material_id = '" . $row_insert['PartNum'] . "'";
                        $Mysql_result = $this->db->query($Mysql_read);
                        
                        if ($Mysql_result->num_rows() > 0) {
                            $SqlserverUpdate        = "UPDATE material SET material_descripcion='" . $row_insert['PartDescription'] . "' WHERE material_id='" . $row_insert['PartNum'] . "'";
                            $Sqlserver_resultUpdate = $this->db->query($SqlserverUpdate);
                            $actualizadas++;

                        } else {
                            $SqlserverInsert        = "INSERT INTO material VALUES ('" . $row_insert['PartNum'] . "','" . $row_insert['PartDescription'] . "')";
                            $Sqlserver_resultInsert = $this->db->query($SqlserverInsert);
                            $insertadas++;
                        }
                    }
                    // ============================================================================================
                    /*
                    while ($row = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
                        $sql2 = $this->db->query("SELECT * from stock_material where material_id = '" . $row['PartNum'] . "' and bodega_id = 1  ")->num_rows();
                        
                        if ($sql2 > 0) {
                            $query = "UPDATE stock_material SET material_id = '" . $row['PartNum'] . "' , material_descripcion = '" . $row['PartDescription'] . "' , stockm_cantidad = '" . $row['Suma'] . "'  where material_id = '" . $row['PartNum'] . "' ";
                            $stock = $this->db->query($query);
                            $actualizadas++;
                        } else {
                            $query = "INSERT INTO stock_material values (null, '" . $row['PartNum'] . "','" . $row['PartDescription'] . "','" . $row['Suma'] . "' , 1 , 0)";
                            $stock = $this->db->query($query);
                            $insertadas++;
                        }
                    }*/

                    $ult_sinc = $this->db->query("UPDATE ult_sinc set fecha = '$date_actual' where id = 1");
                    return "Actualizadas : " . $actualizadas . " Insertadas : " . $insertadas;

                } catch (Exception $e) {
                    return $e;
                }
            }
        }
    }
    public function get_detail_traslado_bandejas($bandeja)
    {
        try {
            $sql    = "SELECT * FROM bandejas WHERE id_material = '$bandeja' ";
            $result = $this->db->query($sql);
            return $result;
        } catch (Exception $e) {
            return $e;
        }

    }

    public function edit_alist($id, $cantidad, $tipo, $movimiento, $material, $stock, $stock_nuevo)
    {
        // echo "movimiento ".$movimiento." cantidad  ".$cantidad."  stock_nuevo  ".$stock_nuevo."  id  ".$id;die();
        if ($tipo == 1) {
            try {
                // echo $stock_nuevo; die();
                $query_sol = $this->db->query("UPDATE detalle_material dm
                                            INNER JOIN detalle_movimiento dmo
                                            ON dmo.dm_id = dm.dm_id
                                            INNER JOIN solicitud_material_temp smtt
                                            ON smtt.solicitudm_id = '$movimiento'
                                            SET dm.cantidad = '$cantidad',
                                                dmo.cantidad = '$cantidad',
                                                smtt.solicitudm_cantidad = smtt.solicitudm_cantidad + '$stock_nuevo'
                                            WHERE dm.dm_id = '$id'");
                return $query_sol;
            } catch (Exception $e) {
                return false;
            }
        } else {
            try {
                $query_sol = $this->db->query("UPDATE detalle_material dm
                                            INNER JOIN detalle_movimiento dmo
                                            ON dmo.dm_id = dm.dm_id
                                            INNER JOIN solicitud_material_temp smtt
                                            ON smtt.solicitudm_id = '$movimiento'
                                            SET dm.cantidad = '$cantidad',
                                                dmo.cantidad = '$cantidad',
                                                smtt.solicitudm_cantidad = smtt.solicitudm_cantidad + '$stock_nuevo'
                                            WHERE dm.dm_id = '$id'");
                return $query_sol;

            } catch (Exception $e) {
                return false;
            }
        }

    }

    public function existenciaMaterial($material)
    {
        // return $material;
        try {
            // $data = $this->db->query("SELECT * FROM stock_material WHERE material_id='$material' AND bodega_id = 1 ");
                    $this->db->where('material_id', $material);
            $data = $this->db->get('material'); // solo busca qu el material exista

            if ($data->num_rows() > 0) {
                return $data;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return $e;
        }
    }
}

/* End of file M_almacen.php */
/* Location: ./application/models/M_almacen.php */
