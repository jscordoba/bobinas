<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_get("America/Bogota");

class M_material extends CI_Model
{
    public function indicador($tipo_indicador)
    {

        switch ($tipo_indicador) {
            case 'pdt':
                $query = "SELECT COUNT(*) as num_sol
                        FROM solicitud_material sol
                        WHERE sol.solicitudm_estado ='PENDIENTE' ";
                break;
            case 'asig':
                $query = "SELECT COUNT(*) as num_sol
                        FROM solicitud_material_temp sol
                        WHERE sol.solicitudm_estado='ENTREGADO'";
                break;
            case 'sol_mat':
                $query = "SELECT COUNT(*) as num_sol
                        FROM solicitud_material sol";
                break;
            case 'sol_resp':
                $query = "SELECT COUNT(*) as num_sol
                        FROM solicitud_material_temp sol
                        WHERE sol.solicitudm_estado='ENTREGADO'";
                break;
            case 'maq_reint':
                $query = "SELECT COUNT(*) as num_sol
                        FROM movimiento_material mm
                        WHERE mm.bodega_id_origen NOT IN(2,3)
                        AND mm.bodega_id_destino=1";
                break;
            case 'prod_reint':
                $query = "SELECT COUNT(*) as num_sol
                        FROM movimiento_material mm
                        WHERE mm.bodega_id_origen=2
                        AND mm.bodega_id_destino=1";
                break;
            case 'ot_act':
                $query = "SELECT COUNT(*) as num_sol
                        FROM maquina_ot mo
                        WHERE mo.maquinao_estado='ACTIVO'";
                break;
            case 'ot_fin':
                $query = "SELECT COUNT(*) as num_sol
                        FROM maquina_ot mo
                        WHERE mo.maquinao_estado='FINALIZADO'";
                break;
            default:
                break;
        }

        try {
            $query     = $this->db->query($query);
            $indicador = $query->row();
            return $indicador->num_sol;

        } catch (Exception $e) {
            return null;
        }
    }

    public function list_station2()
    {
        try {
            $query_list = $this->db->get('maquina');

            return $query_list;

        } catch (Exception $e) {
            return null;
        }
    }
    public function ver_stock_solicitudes()
    {
        try {
            $query_sol = $this->db->query("SELECT mt.material_id,mt.material_descripcion,sol.solicitudm_id,sol.solicitudm_cantidad,sol.solicitudm_date,sol.solicitudm_estado , sol.tipo
                                        FROM solicitud_material sol
                                        JOIN material mt ON mt.material_id=sol.material_id
                                        WHERE sol.solicitudm_estado = 'PENDIENTE' AND sol.solicitudm_cantidad > 0 ");
            return $query_sol;

        } catch (Exception $e) {
            return null;
        }
    }
    public function get_detail_rein($movimiento)
    {
        try {
            $query = $this->db->query("SELECT dm.*, sc.codbar FROM detalle_movimiento dm
                                       JOIN stock_codigo sc ON sc.codbar = dm.dmo_codbar
                                       where dm.movimiento_id=$movimiento");
            return $query;
        } catch (Exception $e) {
            return null;
        }

    }
    public function reporte()
    {
        try {
            $query_sol = $this->db->query("SELECT DISTINCT dm.patinador,dm.dm_codbar,dm.cantidad as cantidad2,mov.* , ma.material_descripcion, sm.solicitudm_estado
                                            FROM movimiento_material mov
                                            JOIN detalle_material dm ON dm.movimiento_id=mov.movimiento_id
                                            JOIN material ma ON ma.material_id = mov.material_id
                                            JOIN solicitud_material_temp sm ON sm.movimiento_id =mov.movimiento_id
                                            WHERE mov.bodega_id_destino=2 and sm.solicitudm_estado = 'ENTREGADO'
                                            ORDER BY mov.movimiento_id DESC");

            return $query_sol;
        } catch (Exception $e) {
            return null;
        }
    }
    public function reporte2()
    {
        try {
            $query_sol = $this->db->query("SELECT DISTINCT dm.patinador,dm.dm_codbar,mov.* ,bo.bodega_nombre, ma.material_descripcion
                                            FROM movimiento_material mov
                                            JOIN detalle_material dm ON dm.movimiento_id=mov.movimiento_id
                                            JOIN material ma ON ma.material_id = mov.material_id
                                            JOIN bodega bo ON bo.bodega_id = mov.bodega_id_destino
                                            WHERE mov.bodega_id_origen=2
                                            AND mov.bodega_id_destino NOT IN(1,2,3)
                                            ORDER BY mov.movimiento_id DESC");

            return $query_sol;
        } catch (Exception $e) {
            return null;
        }
    }
    public function reporte3()
    {
        try {
            $query_sol = $this->db->query("SELECT DISTINCT dm.patinador,dm.dm_codbar,mov.* ,bo.bodega_nombre, ma.material_descripcion
                                            FROM movimiento_material mov
                                            JOIN detalle_material dm ON dm.movimiento_id=mov.movimiento_id
                                            JOIN material ma ON ma.material_id = mov.material_id
                                            JOIN bodega bo ON bo.bodega_id = mov.bodega_id_destino
                                            WHERE mov.bodega_id_origen NOT IN (1,2,3)
                                            AND mov.bodega_id_destino NOT IN(1,2,3)
                                            ORDER BY mov.movimiento_id DESC");

            return $query_sol;
        } catch (Exception $e) {
            return null;
        }
    }
    public function reporte4()
    {
        try {
            $query_sol = $this->db->query("SELECT DISTINCT dm.patinador,dm.dm_codbar,mov.* ,bo.bodega_nombre, ma.material_descripcion
                                            FROM movimiento_material mov
                                            JOIN detalle_material dm ON dm.movimiento_id=mov.movimiento_id
                                            JOIN material ma ON ma.material_id = mov.material_id
                                            JOIN bodega bo ON bo.bodega_id = mov.bodega_id_destino
                                            WHERE mov.bodega_id_origen NOT IN (1,2,3)
                                            AND mov.bodega_id_destino=1
                                            ORDER BY mov.movimiento_id DESC");

            return $query_sol;
        } catch (Exception $e) {
            return null;
        }
    }
    public function ver_stock_reintegro()
    {
        try {
            $query_stock = $this->db->query("SELECT sm.material_id,sm.material_descripcion,sm.stockm_cantidad,sr.solicitudr_id,sr.solicitudr_estado,sr.solicitudr_date,sr.solicitudr_observacion,sr.solicitudr_cantidad, sm.movimiento_id, mov.tipo
                                            FROM stock_material_temp sm
                                            LEFT JOIN solicitud_reintegro sr ON sr.movimiento_id=sm.movimiento_id
                                            JOIN movimiento_material mov ON mov.movimiento_id=sm.movimiento_id
                                            WHERE sm.bodega_id=2
                                            AND sm.stockm_cantidad!=0
                                            ");
            return $query_stock;
        } catch (Exception $e) {
            return null;
        }
    }

    public function reintegros_mq()
    {
        try {
            $query_reintegros = $this->db->query("SELECT ma.material_id, ma.material_descripcion,mm.movimiento_cantidad,mm.bodega_id_origen,bod.bodega_nombre,mm.movimiento_date ,mm.tipo
                                                    FROM movimiento_material mm
                                                    JOIN material ma ON ma.material_id=mm.material_id
                                                    JOIN bodega bod ON bod.bodega_id=mm.bodega_id_origen
                                                    LEFT JOIN maquina_ot mo ON mo.movimiento_id=mm.movimiento_id
                                                    WHERE mm.bodega_id_destino!=3
                                                    AND mm.bodega_id_origen NOT IN(1,2,3)
                                                    AND mo.maquinao_estado = 'FINALIZADO'
                                                ");
            return $query_reintegros;
        } catch (Exception $e) {
            return null;
        }
    }

    public function reintegros_ppal()
    {
        try {
            $query_reintegros = $this->db->query("SELECT *
                                                    FROM movimiento_material mm
                                                    JOIN material ma ON ma.material_id=mm.material_id
                                                    WHERE mm.bodega_id_destino=1
                                                    AND mm.bodega_id_origen=2");

            return $query_reintegros;
        } catch (Exception $e) {
            return null;
        }
    }

    public function ver_ordenes($estado)
    {
        try {
            $query_ordenes = $this->db->query("SELECT mo.maquinao_id,ma.material_id,ma.material_descripcion,mo.maquinao_cantidad,mm.movimiento_cantidad,mq.maquina_id,mq.maquina_nombre,mm.movimiento_date,mo.maquinao_estado,mo.movimiento_id,mo.maquina_bod_id, mm.bodega_id_destino,mm.tipo
                                                FROM maquina_ot mo
                                                JOIN material ma ON ma.material_id=mo.material_id
                                                JOIN bodega bod ON bod.bodega_id=mo.maquina_bod_id
                                                JOIN maquina mq ON mq.maquina_nombre=bod.bodega_nombre
                                                JOIN movimiento_material mm ON mm.movimiento_id=mo.movimiento_id
                                                WHERE mo.maquinao_estado='$estado'
                                                ORDER BY mm.movimiento_date DESC");

            return $query_ordenes;
        } catch (Exception $e) {
            return null;
        }
    }

    public function ver_estaciones()
    {
        try {
            $query_estacion = $this->db->query("SELECT ma.maquina_id,ma.maquina_nombre,se.seccion_nombre
                                                FROM maquina ma
                                                JOIN seccion se ON se.seccion_id=ma.seccion_id where ma.estado =1");

            return $query_estacion;
        } catch (Exception $e) {
            return null;
        }
    }

    ///-------- INSERTS ---------////

    public function reg_sol_material($material_id, $material_descripcion, $material_cantidad, $date_actual, $tipo, $observacion)
    {
        $user_id = $this->session->userdata('usuario_id');

        try {
            $query_ins2 = "INSERT INTO solicitud_material VALUES (null,$material_id,$material_cantidad,$user_id,'$date_actual','PENDIENTE',NULL,$tipo,'$observacion')";
            $this->db->query($query_ins2);
            $codigo = $this->db->insert_id();
            return $codigo;
        } catch (Exception $e) {
            return null;
        }
    }

    public function v_movimientos()
    {
        try {
            $query_mv = "SELECT mat.material_id,mat.material_descripcion,(SELECT bodega_nombre FROM bodega  WHERE bodega_id=mv.bodega_id_origen) as bodega_origen,
            (SELECT bodega_nombre FROM bodega   WHERE bodega_id=mv.bodega_id_destino)
            as bodega_destino, mv.movimiento_date,mv.movimiento_cantidad , mv.tipo
            FROM movimiento_material mv
            LEFT JOIN material mat ON mat.material_id=mv.material_id
            LEFT JOIN bodega bod ON bod.bodega_id=mv.bodega_id_origen
            WHERE mv.bodega_id_origen!=1 AND mv.bodega_id_origen!=3 ORDER BY mv.movimiento_date DESC LIMIT 500";
            return $this->db->query($query_mv);
        } catch (Exception $e) {
            return null;
        }
    }

    public function reg_sol_reintegro($mat_id, $mat_disponible, $mat_reintegro, $observacion_reintegro, $date_actual, $movimiento, $codigo, $tipo)
    {
        try {
            //-------------- INSERTA EN LA TABLA SOLICITUD_REINTEGRO PARA VALIDACION DE ALMACEN -----------------------------///
            $query_ins = "INSERT INTO solicitud_reintegro VALUES (null,$mat_id,$mat_reintegro,'$observacion_reintegro','$date_actual','PENDIENTE','$movimiento', '$codigo','$tipo')";

            if ($this->db->query($query_ins)) {
                return "ok";
            }

        } catch (Exception $e) {
            return null;
        }
    }
    public function reg_sol_reintegro_normal($mat_id, $mat_disponible, $observacion_reintegro, $date_actual, $movimiento, $codigo, $id)
    {
        $cantidad = 0;
        for ($i = 0; $i < count($id); $i++) {
            $query = "UPDATE detalle_movimiento SET movimiento_id = '0' WHERE dm_id= $id[$i] ";
            $this->db->query($query);
            try {
                //-------------- INSERTA EN LA TABLA SOLICITUD_REINTEGRO PARA VALIDACION DE ALMACEN -----------------------------///
                $query_ins = "INSERT INTO solicitud_reintegro VALUES (null,$mat_id,$mat_disponible[$i],'$observacion_reintegro','$date_actual','PENDIENTE','$movimiento', '$codigo[$i]','1')";
                if ($this->db->query($query_ins)) {
                    // return "ok";
                }
            } catch (Exception $e) {
                return null;
            }
        }

    }

    public function detail_maquinao($maq_id)
    {
        try {
            $query_maq = "SELECT mq.maquina_id,mq.maquina_nombre, mo.maquinao_id,mo.maquinao_cantidad,ma.material_id,ma.material_descripcion,mo.maquinao_codbar,mo.maquinao_date,mo.maquinao_estado
                        FROM maquina_ot mo
                        JOIN material ma ON ma.material_id=mo.material_id
                        JOIN bodega bod ON bod.bodega_id=mo.maquina_bod_id
                        JOIN maquina mq ON mq.maquina_nombre=bod.bodega_nombre
                        WHERE mq.maquina_id=$maq_id
                        AND mo.maquinao_estado!='FINALIZADO'";

            $exec = $this->db->query($query_maq);

            if ($exec->num_rows() > 0) {
                return $exec;
            }
        } catch (Exception $e) {
            return null;
        }
    }
    public function detail_ot_seguimiento($codbar)
    {
        try {
            $this->db->where('cod_bar', $codbar);
            $query = $this->db->get('detalle_ot');
            if ($query->num_rows() > 0) {
                return $query;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return $e;
        }
    }
    public function new_detailot()
    {
        $ot          = $this->input->get_post = $this->input->get_post('number_ot');
        $item        = $this->input->get_post = $this->input->get_post('item');
        $descripcion = $this->input->get_post = $this->input->get_post('descripcion');
        $cantidad    = $this->input->get_post = $this->input->get_post('cantidad');
        $fecha       = $this->input->get_post = $this->input->get_post('fecha');
        $codbar      = $this->input->get_post = $this->input->get_post('codbar');
        $maquina     = $this->input->get_post = $this->input->get_post('maquina');

        try {
            $query_ins = "INSERT INTO detalle_ot VALUES (null,'$ot', '$item', '$descripcion' , '$cantidad' , '$fecha' , 'ACTIVO' , '$codbar','$maquina')";

            if ($this->db->query($query_ins)) {
                return "ok";
            }
        } catch (Exception $e) {
            return $e;
        }
    }
    public function edit_detailot()
    {

        $id          = $this->input->get_post = $this->input->get_post('id_detailot');
        $id2         = $this->input->get_post = $this->input->get_post('id_detalle_ot');
        $item        = $this->input->get_post = $this->input->get_post('item');
        $descripcion = $this->input->get_post = $this->input->get_post('descripcion');
        $cantidad    = $this->input->get_post = $this->input->get_post('cantidad');
        $fecha       = $this->input->get_post = $this->input->get_post('fecha');
        $estado      = $this->input->get_post = $this->input->get_post('estado');

        $maquina = $this->input->get_post = $this->input->get_post('maquina');

        try {
            $query_ins = "UPDATE detalle_ot set  id_detalle_ot = '$id2', item = '$item', descripcion = '$descripcion',
            cantidad_uds = '$cantidad' , fecha_asignacion = '$fecha', maquina = '$maquina', estado = '$estado'  where Id = '$id'  ";

            if ($this->db->query($query_ins)) {
                return "ok";
            }
        } catch (Exception $e) {

        }

    }
    public function fecha_actual()
    {
        $date        = now();
        $z_horaria   = gmt_to_local($date, 'UM5', date('I', $date));
        $date_actual = mdate("%Y/%m/%d %H:%i:%s", $z_horaria);

        return $date_actual;
    }

    public function import_file($ruta)
    {
        $user_id = $this->session->userdata('usuario_id');
        $handle = fopen($ruta, "r");
        $i_correctos  = 0;
        $i_error      = 0;

        try {
            while (($filesop = fgetcsv($handle, 5000, ";")) !== false) {
                $material_id            = $filesop[0];
                $solicitudm_cantidad    = $filesop[1];
                $usuario_id_solicitante = $user_id;//(int) $filesop[2];
                $solicitudm_date        = $this->fecha_actual();
                $solicitudm_estado      = "PENDIENTE";//$filesop[3];
                $movimiento_id          = NULL;//$filesop[4];
                $tipo                   = 1;//$filesop[5];
                $imprevistos            = $filesop[2];

                $this->db->where('material_id', $material_id);
                $exist_material = $this->db->get('material');
               
                if ($exist_material->num_rows() > 0 ) {
                    $data = array(
                        'material_id' => $material_id, 
                        'solicitudm_cantidad' => $solicitudm_cantidad, 
                        'usuario_id_solicitante' => $usuario_id_solicitante, 
                        'solicitudm_date' => $solicitudm_date, 
                        'solicitudm_estado' => $solicitudm_estado, 
                        'movimiento_id' => $movimiento_id, 
                        'tipo' => $tipo, 
                        'imprevistos' => $imprevistos
                        );

                    $this->db->insert('solicitud_material', $data);
                    $i_correctos = $i_correctos + 1;
                }else{
                    echo '  <script>
                                alert("Sin registro de Material: \\n' . $material_id .'");
                            </script>';
                            
                    $i_error = $i_error + 1;
                }
                
            }

            $resultado = array('correctos' => $i_correctos,
                                'error' => $i_error
                                );

            return $resultado;
            
        } catch (Exception $e) {
            echo ' <script>alert("Error Inesperado: \\n' . $e . '");
                    </script>';
                    
            redirect(base_url() . 'c_material/', 'refresh');
        }

    }
    public function list_codigos()
    {
        try {
            $codigos = $this->db->query("SELECT * 
                                        FROM stock_codigo sc
                                        JOIN stock_material sm ON sm.stockm_id = sc.id_detalle
                                        WHERE sc.sc_cantidad != 0 
                                        ORDER BY sc.id DESC
                                        ");

            return $codigos;
        } catch (Exception $e) {
            return null;
        }
    }
    public function list_materiales()
    {
        try {
            $materiales = $this->db->query("SELECT * FROM stock_material where bodega_id = 1 ");
            return $materiales;
        } catch (Exception $e) {
            return null;
        }
    }

    //ERROR PROBLEMA CON VALOR DE MATERIAL => TABLA RECIBE ID_DETALLE DEL STOCK_MATERIAL
    public function reg_codigo($material, $presentacion, $cantidad, $codigo)
    {
        $bodega_id=1; //BODEGA ALMACEN
        $movimiento_id = null; //MOVIMIENTO INICIAL

        try {
            $this->db->where('material_id', $material);
            $this->db->select('material_id, material_descripcion');
            $material_q = $this->db->get('material');

            $descripcion = $material_q->row();

            $nuevo_stock = $this->db->query("INSERT INTO stock_material VALUES (null , $material , '$descripcion->material_descripcion' , $cantidad , $bodega_id, null)");

            $stockm_id = $this->db->insert_id();

            $stmt = $this->db->query("INSERT INTO stock_codigo VALUES (null , $stockm_id , '$presentacion' , $cantidad , '$codigo')");

            if ($stmt) {
                return true;
            } else {
                return false;
            }

        } catch (Exception $e) {
            return false;
        }
    }
    public function delete_codigo($id)
    {
        try {
            $this->db->where('id', $id);
            $res_stock = $this->db->get('stock_codigo');
            $detalle = $res_stock->row();

            $codigo1 = $this->db->query("DELETE FROM stock_codigo WHERE id = $id ");
            $codigo2 = $this->db->query("DELETE FROM stock_material WHERE stockm_id = $detalle->id_detalle ");

            if ($codigo1 and $codigo2) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
    public function edit_codigo($cantidad, $id)
    {

        try {

            $codigo = $this->db->query("UPDATE stock_codigo SET sc_cantidad = $cantidad where id = $id ");
            if ($codigo) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

}

/* End of file M_patinador.php */
/* Location: ./application/models/M_patinador.php */
