<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_patinador extends CI_Model
{

    public function v_movimientos()
    {
        try {
            $query_mv = "SELECT mat.material_id,mat.material_descripcion,(SELECT bodega_nombre FROM bodega WHERE bodega_id=mv.bodega_id_origen) as bodega_origen,(SELECT bodega_nombre FROM bodega WHERE bodega_id=mv.bodega_id_destino) as bodega_destino, mv.movimiento_date,mv.movimiento_cantidad
                        FROM movimiento_material mv
                        LEFT JOIN material mat ON mat.material_id=mv.material_id
                        LEFT JOIN bodega bod ON bod.bodega_id=mv.bodega_id_origen
                        WHERE mv.bodega_id_origen!=1 AND mv.bodega_id_origen!=3
                        ORDER BY mv.movimiento_date DESC
                        LIMIT 500";

            return $this->db->query($query_mv);
        } catch (Exception $e) {
            return null;
        }
    }

    public function ver_estaciones()
    {
        try {
            $query_estacion = $this->db->query("SELECT ma.maquina_id,ma.maquina_nombre,se.seccion_nombre,bod.bodega_id
                                                FROM maquina ma
                                                JOIN bodega bod ON bod.bodega_nombre=ma.maquina_nombre
                                                JOIN seccion se ON se.seccion_id=ma.seccion_id where ma.estado=1");

            return $query_estacion;
        } catch (Exception $e) {
            return null;
        }
    }

    public function ver_fin_ot($codbar)
    {
        try {
            $query_mv = "SELECT  se.seccion_nombre,mt.material_id,mt.material_descripcion,mo.maquinao_ot,mo.maquinao_cantidad,mq.maquina_id,bod.bodega_id,bod.bodega_nombre ,mo.maquinao_id, mo.movimiento_id,mov.tipo
                        FROM maquina_ot mo
                        JOIN bodega bod ON bod.bodega_id=mo.maquina_bod_id
                        JOIN maquina mq ON mq.maquina_nombre=bod.bodega_nombre
                        JOIN seccion se on se.seccion_id=mq.seccion_id
                        JOIN material mt ON mt.material_id=mo.material_id
                        JOIN movimiento_material mov ON mov.movimiento_id=mo.movimiento_id
                        WHERE mo.maquinao_codbar='$codbar'
                        AND mo.maquinao_estado='ACTIVO' ";
            return $this->db->query($query_mv);
        } catch (Exception $e) {
            return null;
        }
    }

    public function buscar($mat_id)
    {
        // echo $mat_id;
        try {
            $query_mat = "SELECT material_descripcion FROM stock_material WHERE material_id=$mat_id";
            $busq      = $this->db->query($query_mat);

            if ($busq->num_rows() > 0) {
                return $busq;
            }

        } catch (Exception $e) {
            return null;
        }
    }
    public function buscar2($codigo)
    {
        try {
            $query_mat = "SELECT * FROM stock_codigo WHERE codbar=$codigo and sc_cantidad != 0 ";
            $busq      = $this->db->query($query_mat);

            if ($busq->num_rows() > 0) {
                return $busq;
            }

        } catch (Exception $e) {
            return null;
        }
    }

    public function ver_stock_solicitudes()
    {
        try {
            $query_sol = $this->db->query("SELECT mt.material_id,mt.material_descripcion,sol.solicitudm_id,sol.solicitudm_cantidad,sol.solicitudm_date,sol.solicitudm_estado ,sol.tipo
                                        FROM solicitud_material sol
                                        JOIN material mt ON mt.material_id=sol.material_id
                                        WHERE sol.solicitudm_cantidad > 0 and sol.solicitudm_estado = 'PENDIENTE' ");
            return $query_sol;

        } catch (Exception $e) {
            return null;
        }
    }

    public function ver_stock_reintegro()
    {
        try {

            $query_stock = $this->db->query("SELECT DISTINCT sm.*,sr.solicitudr_estado, mo.tipo,i.imprevistos
                                            FROM stock_material_temp sm
                                            LEFT JOIN solicitud_reintegro sr ON sr.movimiento_id=sm.movimiento_id
                                            JOIN movimiento_material mo ON mo.movimiento_id=sm.movimiento_id
                                            JOIN solicitud_material_temp i ON i.movimiento_id = sm.movimiento_id
                                            JOIN detalle_material dm ON dm.movimiento_id = sm.movimiento_id
                                            WHERE sm.bodega_id=2
                                            AND sm.stockm_cantidad!=0
                                            ");

            return $query_stock;
        } catch (Exception $e) {
            return null;
        }
    }
    public function ver_stock_reintegro2()
    {
        try {
            $query_stock = $this->db->query("SELECT DISTINCT sm.*,sr.solicitudr_id,sr.solicitudr_estado,sr.solicitudr_date,sr.solicitudr_observacion,sr.solicitudr_cantidad, mo.tipo,i.imprevistos
                                            FROM stock_material_temp sm
                                            LEFT JOIN solicitud_reintegro sr ON sr.movimiento_id=sm.movimiento_id
                                            JOIN movimiento_material mo ON mo.movimiento_id=sm.movimiento_id
                                            JOIN solicitud_material_temp i ON i.movimiento_id = sm.movimiento_id
                                            JOIN detalle_material dm ON dm.movimiento_id = sm.movimiento_id
                                            WHERE sm.bodega_id=2 AND i.solicitudm_estado = 'ENTREGADO'
                                            AND sm.stockm_cantidad!=0
                                            ");

            return $query_stock;
        } catch (Exception $e) {
            return null;
        }
    }

    //--------------------REGISTRAR TRASLADO ENTRE LA MAQUINA HACIA LA BOD PRODUCCION -------------------------//
    public function reg_traslado_prod($codbar, $mat_id, $maquina_origen, $bod_destino, $mat_cantidad_reintegro, $mat_descripcion, $date_actual, $user_id, $id, $tipo)
    {
        // echo "esta es la maquina : ".$maquina_origen; die();

        try {
            //-------------- ACTUALIZA EL STOCK DE MATERIAL -----------------------------///

            //-------------- INSERTA EN LA TABLA MOVIMIENTOS GENERALES DE MATERIAL  -----------------------------///
            $query_ins_mov = "INSERT INTO movimiento_material VALUES (NULL,$mat_id,$mat_cantidad_reintegro,$user_id,$maquina_origen,1,'$date_actual','$tipo')";

            if ($this->db->query($query_ins_mov)) {

                $movimiento_id = $this->db->insert_id();

                //-------------- ACTUALIZAR EL ESTADO DE LA OT EN LA MAQUINA ASIGNADA  -----------------------------///
                $query_upd_ot = "UPDATE maquina_ot SET maquinao_estado = 'FINALIZADO', movimiento_id=$movimiento_id WHERE maquinao_codbar='$codbar' AND maquinao_id = $id";

                if ($this->db->query($query_upd_ot)) {
                    return "ok";
                } else {
                    return "Error al Insertar en Maquina_ot";
                }
            } else {
                return "Error al Insertar en movimiento_material";
            }

        } catch (Exception $e) {
            return null;
        }
    }

    public function get_detail_mq($codbar, $movimiento)
    {
        try {
            $sql = "SELECT maquinao_id,maquina_bod_id,maquinao_cantidad,maquinao_ot,maquinao_codbar,maquinao_date,maquinao_estado,dm_id,detalle_movimiento.movimiento_id,detalle_movimiento.dm_id,num_unidad,cantidad,presentacion,material.material_id,material_descripcion
                                        FROM maquina_ot
                                        JOIN detalle_movimiento ON detalle_movimiento.movimiento_id=maquina_ot.movimiento_id
                                        JOIN material ON material.material_id=maquina_ot.material_id
                                        WHERE maquina_ot.maquinao_codbar='$codbar'
                                        AND num_unidad!=0
                                        AND maquina_ot.movimiento_id = $movimiento
                                        AND maquina_ot.maquinao_estado = 'ACTIVO' ";
            $query_mv = $this->db->query($sql);

            return $query_mv;

        } catch (Exception $e) {
            return null;
        }
    }

    public function generarCodigo($longitud)
    {
        $key     = '';
        $pattern = '1234567890';
        $max     = strlen($pattern) - 1;
        for ($i = 0; $i < $longitud; $i++) {
            $key .= $pattern{mt_rand(0, $max)};
        }

        return $key;
    }
    public function reg_traslado($sol_codbar_ot, $mat_id, $mat_cantidad, $mat_descripcion, $date_actual, $und_nueva, $unds_actual, $stock_cantidad, $cantidad_und, $presentacion, $perfil, $bod_maquina_origen, $bod_maquina_fin, $user_id, $patinador, $codigo, $id, $tipo)
    {

        $bod_inicio      = $bod_maquina_origen;
        $bod_fin         = $bod_maquina_fin;
        $array_patinador = $this->session->userdata('usuario_id');
        try {
            $c          = 0;
            $t_cantidad = 0;
            foreach ($und_nueva as $detalle_id => $unds_nueva) {
//recorrer array de unidades seleccionadas
                $t_cantidad += $cantidad_und[$c++] * $unds_nueva;
            }

            foreach ($und_nueva as $detalle_id => $unds_nueva) {
//recorrer array de unidades seleccionadas
                $unidad_nueva[] = $unds_nueva;
                $detail_id[]    = $detalle_id;
            }
            $total_cantidad = 0;
            $veces          = 0;
            for ($i = 0; $i < count($und_nueva); $i++) {
                try {
                    if ($unidad_nueva[$i] != 0) {
                        $res_unds = $unds_actual[$i] - $unidad_nueva[$i];
                        $total_cantidad += $cantidad_und[$i] * $unidad_nueva[$i];
                        $query_ins_mov = "INSERT INTO movimiento_material VALUES (NULL,$mat_id,$cantidad_und[$i],$user_id,$bod_inicio,$bod_fin,'$date_actual', '$tipo')";
                        $this->db->query($query_ins_mov);
                        $mov_id     = $this->db->insert_id();
                        $upd_detail = "UPDATE detalle_movimiento
                                    SET num_unidad=$res_unds
                                    WHERE dm_id=$detail_id[$i]";
                        $ins_detail = "INSERT INTO detalle_movimiento
                                    VALUES(null,$mov_id,$unidad_nueva[$i],$cantidad_und[$i],'$presentacion[$i]','$array_patinador','$codigo[$i]', '2', null)";
                        $ins_detail_mat = "INSERT INTO  detalle_material
                                    VALUES(null,$mov_id,$unidad_nueva[$i],$cantidad_und[$i],'$presentacion[$i]','$array_patinador','$codigo[$i]', '2', null)";
                        $update       = $this->db->query($upd_detail);
                        $insert       = $this->db->query($ins_detail);
                        $insert_dmat  = $this->db->query($ins_detail_mat);
                        $veces        = $veces + $unidad_nueva[$i];
                        $query_ins_ot = "INSERT INTO maquina_ot VALUES (NULL,$bod_maquina_fin,$mat_id,$cantidad_und[$i],$sol_codbar_ot,'$codigo[$i]','$date_actual','ACTIVO',$mov_id)";
                        $this->db->query($query_ins_ot);

                    }
                } catch (Exception $e) {
                    echo "Error de insersión en detalle_movimiento.";
                }
            }
            $query_origen = "SELECT stock_material.stockm_cantidad
                            FROM stock_material
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_inicio";
            $query_destino = "SELECT stock_material.stockm_cantidad
                            FROM stock_material
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_fin";
            $stock_origen  = $this->db->query($query_origen);
            $stock_destino = $this->db->query($query_destino);
            if ($stock_origen->num_rows() > 0) {
                $row1         = $stock_origen->row();
                $stock_origen = $row1->stockm_cantidad;
                if ($stock_origen >= $total_cantidad) {
                    $new_stock_origen = $stock_origen - $total_cantidad;
                    $query3           = "UPDATE stock_material
                            SET stock_material.stockm_cantidad=$new_stock_origen
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_inicio";
                    $update_stock  = $this->db->query($query3);
                    $query_origen2 = "SELECT stock_material_temp.stockm_cantidad
                            FROM stock_material_temp
                            WHERE stock_material_temp.material_id=$mat_id
                            AND stock_material_temp.bodega_id=2
                            AND stock_material_temp.stockm_id = $id";
                    $stock_origen2     = $this->db->query($query_origen2);
                    $row2              = $stock_origen2->row();
                    $stock_origen2     = $row2->stockm_cantidad;
                    $new_stock_origen2 = $stock_origen2 - $total_cantidad;
                    $query_stock_temp  = "UPDATE `stock_material_temp` SET `stockm_cantidad` = $new_stock_origen2 WHERE stockm_id = $id";
                    $update_stock      = $this->db->query($query_stock_temp);
                }
                if (($stock_destino->num_rows() > 0)) {
                    $row2              = $stock_destino->row();
                    $stock_destino     = $row2->stockm_cantidad;
                    $new_stock_destino = $stock_destino + $total_cantidad;
                    $query4            = "UPDATE stock_material
                            SET stock_material.stockm_cantidad=$new_stock_destino,
                                stock_material.movimiento_id=$mov_id
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_fin";
                    $stock = $this->db->query($query4);

                } else {
                    $query5 = "INSERT INTO stock_material
                            VALUES (NULL,$mat_id,'$mat_descripcion',$total_cantidad, $bod_fin,$mov_id)";
                    $stock = $this->db->query($query5);
                }
            } else {
                return null;
            }
            return "ok";
        } catch (Exception $e) {
            return null;
        }
    }
    public function reg_traslado_bandeja($sol_codbar_ot, $mat_id, $mat_descripcion, $date_actual, $stock_cantidad, $perfil, $bod_maquina_origen, $bod_maquina_fin, $user_id, $patinador, $codigo, $id, $cantidad_bandeja, $id_detalle)
    {

        $bod_inicio      = $bod_maquina_origen;
        $bod_fin         = $bod_maquina_fin;
        $array_patinador = $this->session->userdata('usuario_id');
        $codigo          = $codigo[0];
        try {
            $c             = 0;
            $t_cantidad    = 0;
            $query_ins_mov = "INSERT INTO movimiento_material VALUES (NULL,$mat_id,$cantidad_bandeja,$user_id,$bod_inicio,$bod_fin,'$date_actual', '2')";
            $this->db->query($query_ins_mov);
            $mov_id         = $this->db->insert_id();
            $total_cantidad = 0;
            $veces          = 0;
            try {
                if ($cantidad_bandeja != 0) {

                    $upd_detail = "UPDATE detalle_movimiento
                                    SET num_unidad=$cantidad_bandeja
                                    WHERE dm_id = '$id_detalle'  ";
                    $ins_detail = "INSERT INTO detalle_movimiento
                                    VALUES(null,$mov_id,$cantidad_bandeja,null,'BANDEJA','$array_patinador','$codigo')";
                    $ins_detail_mat = "INSERT INTO  detalle_material
                                    VALUES(null,$mov_id,$cantidad_bandeja,null,'BANDEJA','$array_patinador','$codigo')";
                    $update      = $this->db->query($upd_detail);
                    $insert      = $this->db->query($ins_detail);
                    $insert_dmat = $this->db->query($ins_detail_mat);

                }
            } catch (Exception $e) {
                echo "Error de insersión en detalle_movimiento.";
            }
            $query_origen = "SELECT stock_material.stockm_cantidad
                            FROM stock_material
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_inicio";
            $query_destino = "SELECT stock_material.stockm_cantidad
                            FROM stock_material
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_fin";
            $stock_origen  = $this->db->query($query_origen);
            $stock_destino = $this->db->query($query_destino);
            if ($stock_origen->num_rows() > 0) {
                $row1         = $stock_origen->row();
                $stock_origen = $row1->stockm_cantidad;
                if ($stock_origen >= $total_cantidad) {
                    $new_stock_origen = $stock_origen - $cantidad_bandeja;
                    $query3           = "UPDATE stock_material
                            SET stock_material.stockm_cantidad=$new_stock_origen
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_inicio";
                    $update_stock  = $this->db->query($query3);
                    $query_origen2 = "SELECT stock_material_temp.stockm_cantidad
                            FROM stock_material_temp
                            WHERE stock_material_temp.material_id=$mat_id
                            AND stock_material_temp.bodega_id=2
                            AND stock_material_temp.stockm_id = $id";
                    $stock_origen2     = $this->db->query($query_origen2);
                    $row2              = $stock_origen2->row();
                    $stock_origen2     = $row2->stockm_cantidad;
                    $new_stock_origen2 = $stock_origen2 - $cantidad_bandeja;
                    $query_stock_temp  = "UPDATE `stock_material_temp` SET `stockm_cantidad` = $new_stock_origen2 WHERE stockm_id = $id";
                    $update_stock      = $this->db->query($query_stock_temp);
                }
                if (($stock_destino->num_rows() > 0)) {
                    $row2              = $stock_destino->row();
                    $stock_destino     = $row2->stockm_cantidad;
                    $new_stock_destino = $stock_destino + $cantidad_bandeja;
                    $query4            = "UPDATE stock_material
                            SET stock_material.stockm_cantidad=$new_stock_destino,
                                stock_material.movimiento_id=$mov_id
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_fin";
                    $stock = $this->db->query($query4);

                } else {
                    $query5 = "INSERT INTO stock_material
                            VALUES (NULL,$mat_id,'$mat_descripcion',$cantidad_bandeja, $bod_fin,$mov_id)";
                    $stock = $this->db->query($query5);
                }
            } else {
                return null;
            }
            $query_ins_ot = "INSERT INTO maquina_ot VALUES (NULL,$bod_maquina_fin,$mat_id,$cantidad_bandeja,$sol_codbar_ot,'$codigo','$date_actual','ACTIVO',$mov_id)";
            // echo "$query_ins_ot";
            if ($this->db->query($query_ins_ot)) {
                return "ok";
            }
        } catch (Exception $e) {
            return null;
        }
    }
    public function reg_traslado2($sol_codbar_ot, $mat_id, $mat_cantidad, $mat_descripcion, $date_actual, $und_nueva, $unds_actual, $stock_cantidad, $cantidad_und, $presentacion, $perfil, $bod_maquina_origen, $bod_maquina_fin, $user_id, $movimientos, $codigo, $tipo)
    {
        if ($perfil == "bodega") {
            $bod_inicio      = $bod_maquina_origen;
            $bod_fin         = 2;
            $array_patinador = $this->session->userdata('usuario_id');
            $movimiento      = $movimientos;

        } else if ($perfil == "maquina") {
            $bod_inicio      = $bod_maquina_origen;
            $bod_fin         = $bod_maquina_fin;
            $array_patinador = $this->session->userdata('usuario_id');
            $movimiento      = $movimientos;
        }
        try {
            $c          = 0;
            $t_cantidad = 0;
            if ($tipo == 1) {
                foreach ($und_nueva as $detalle_id => $unds_nueva) {
                    $t_cantidad += $cantidad_und[$c++] * $unds_nueva;
                }
            } else {
                $t_cantidad = $cantidad_und[$c++];
            }

            if ($perfil == "bodega") {
                //---------------ACTUALIZAR REGISTRO DE MAQUINA_OT "FINALIZARLO"----------------------------//
                $query_upd = "UPDATE maquina_ot
                                SET maquinao_estado='FINALIZADO'
                                WHERE maquinao_codbar='$sol_codbar_ot'
                                AND movimiento_id = $movimiento";
                $this->db->query($query_upd);

                //-------------- INSERTA EN LA TABLA MOVIMIENTOS GENERALES DE MATERIAL  -----------------------------///
                $query_ins_mov = "INSERT INTO movimiento_material VALUES (NULL,$mat_id,$t_cantidad,$user_id,$bod_inicio,$bod_fin,'$date_actual', '$tipo')";
                $this->db->query($query_ins_mov);
                $mov_id = $this->db->insert_id();
            } else if ($perfil == "maquina") {
                $query_ins_mov = "INSERT INTO movimiento_material VALUES (NULL,$mat_id,$t_cantidad,$user_id,$bod_inicio,$bod_fin,'$date_actual','$tipo')";
                $this->db->query($query_ins_mov);
                $mov_id    = $this->db->insert_id();
                $query_upd = "UPDATE maquina_ot
                                            SET maquinao_estado='FINALIZADO',
                                                movimiento_id=$mov_id
                                            WHERE maquinao_codbar='$codigo'
                                            AND movimiento_id = $movimiento";
                $this->db->query($query_upd);
                $query_ins_ot = "INSERT INTO maquina_ot VALUES (NULL,$bod_maquina_fin,$mat_id,$t_cantidad,$sol_codbar_ot,'$codigo','$date_actual','ACTIVO',$mov_id)";
                $this->db->query($query_ins_ot);

            }
            if ($tipo == 1) {
                foreach ($und_nueva as $detalle_id => $unds_nueva) {
                    $unidad_nueva[] = $unds_nueva;
                    $detail_id[]    = $detalle_id;
                }
            } else {
                $unidad_nueva[] = $und_nueva;
            }

            $total_cantidad = 0;
            $veces          = 0;
            for ($i = 0; $i < count($und_nueva); $i++) {
                try {
                    if ($perfil == "bodega") {
                        $upd_detail = "UPDATE detalle_movimiento
                                SET cantidad=$cantidad_und[$i], num_unidad = 1
                                WHERE dmo_codbar=$codigo and estado = '1'";
                        $update = $this->db->query($upd_detail);
                    }
                    $ins_detail = "INSERT INTO detalle_movimiento
                                    VALUES(null,$mov_id,1,$cantidad_und[$i],'$presentacion[$i]','$array_patinador','$codigo','3', null)";
                    $ins_detail_mat = "INSERT INTO  detalle_material
                                    VALUES(null,$mov_id,1,$cantidad_und[$i],'$presentacion[$i]','$array_patinador','$codigo','3', null)";

                    $insert      = $this->db->query($ins_detail);
                    $insert_dmat = $this->db->query($ins_detail_mat);
                    $veces       = $veces + 1;
                } catch (Exception $e) {
                    echo "Error de insersión en detalle_movimiento.";
                }
            }
            $query_origen = "SELECT stock_material.stockm_cantidad
                            FROM stock_material
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_inicio";
            $query_destino = "SELECT stock_material.stockm_cantidad
                            FROM stock_material
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_fin";
            $stock_origen  = $this->db->query($query_origen);
            $stock_destino = $this->db->query($query_destino);
            if ($stock_origen->num_rows() > 0) {
                $row1         = $stock_origen->row();
                $stock_origen = $row1->stockm_cantidad;
                if ($stock_origen >= $t_cantidad) {
                    $new_stock_origen = $stock_origen - $t_cantidad;
                    $query3           = "UPDATE stock_material
                            SET stock_material.stockm_cantidad=$new_stock_origen
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_inicio";
                    $update_stock = $this->db->query($query3);
                }
                if (($stock_destino->num_rows() > 0)) {
                    $row2              = $stock_destino->row();
                    $stock_destino     = $row2->stockm_cantidad;
                    $new_stock_destino = $stock_destino + $t_cantidad;
                    $query4            = "UPDATE stock_material
                            SET stock_material.stockm_cantidad=$new_stock_destino,
                                stock_material.movimiento_id=$mov_id
                            WHERE stock_material.material_id=$mat_id
                            AND stock_material.bodega_id=$bod_fin";
                    $query6 = "UPDATE stock_material_temp
                                SET stockm_cantidad = $new_stock_destino
                                WHERE material_id = $mat_id
                                AND bodega_id = $bod_fin";

                    $stock = $this->db->query($query4);
                    $stock = $this->db->query($query6);

                } else {
                    $query5 = "INSERT INTO stock_material
                            VALUES (NULL,$mat_id,'$mat_descripcion',$t_cantidad, $bod_fin,$mov_id)";
                    $stock = $this->db->query($query5);
                }
            } else {
                return null;
            }
            return "ok";
        } catch (Exception $e) {
            return null;
        }
    }

    public function ver_stock($code)
    {
        try {
            $query_stock = $this->db->query("SELECT *,(SELECT COUNT(movimiento_id) FROM detalle_movimiento WHERE movimiento_id=sm.movimiento_id AND cantidad!=0) as unidades FROM stock_material sm WHERE sm.bodega_id=1 AND sm.material_id = '$code'");

            return $query_stock;
        } catch (Exception $e) {
            return null;
        }
    }

}
