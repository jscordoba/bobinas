<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header righteous">Inicio</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           

            <!-- /.row  CASILLAS DE CONTADORES-->
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        	<h4 class="righteous">Usuarios</h4>
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge">7</div>
                                    <div>Pendientes</div>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge">26</div>
                                    <div>Entregados</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= base_url(); ?>c_admin/list_user">
                            <div class="panel-footer oswald">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
               
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                        	<h4 class="righteous">Estaciones</h4>
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge">7</div>
                                    <div>Activas</div>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge">2</div>
                                    <div>Inactivas</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= base_url(); ?>c_admin/list_station">
                            <div class="panel-footer oswald">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <!-- TABLA -->
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading righteous">
                            Historial de Movimientos
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body oswald">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover oswald" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Item</th>
                                            <th>Descripción</th>
                                            <th>Origen</th>
                                            <th>Destino</th>
                                            <th>Cantidad</th>
                                            <th>Fecha Movimiento</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
            <!-- /.row -->

    </div>
    <!-- /#wrapper -->
