<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header righteous">Listado de Secciones</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Secciones
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                        <thead>
		                            <tr>
		                            	<th>#</th>
		                                <th>ID</th>
		                                <th>Descripción</th>
		                                <th>Estado</th>
		                                <th>Acción</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        <?php 
	                            $i=1;
				            	foreach ($list_section->result() as $list_sectionr): ?>
					                <tr>
					                    <?php 
					                    $estado = $list_sectionr->estado;
					                    if ($estado == 1) {?>
					                    <td><?= $i++; ?></td>
					                    <td><?= $list_sectionr->seccion_id; ?></td>
					                    <td><?= $list_sectionr->seccion_nombre; ?></td>
					                    <td class="bg-success">ACTIVO</td>
					                    <?php
					                    }
					                    else{?>
					                     <td><?= $i++; ?></td>
					                    <td><?= $list_sectionr->seccion_id; ?></td>
					                    <td><?= $list_sectionr->seccion_nombre; ?></td>
					                    <td class="bg-danger">INACTIVO</td>
					                    <?php
					                    }
					                    ?>
					                    <td><a id="<?= $i ?>" onclick="pasar_valores(this)" data-seccion_id="<?= $list_sectionr->seccion_id ?>" data-seccion_nombre="<?= $list_sectionr->seccion_nombre ?>" class="" >Editar</a>/<a href="<?= base_url(); ?>c_admin/delete_section/<?= $list_sectionr->seccion_id ?>" class="">Eliminar</a></td>
					                </tr>
					            <?php endforeach ?>
		                        </tbody>
		                    </table>
		                </div>
		                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
		</div>
	</div>
</div>


<!-- Modal Nueva solicitud-->
<div class="modal fade" id="editar_seccion" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" id="">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title righteous" id="myModalLabel">
            Edición de Sección
        </h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" action="editar_section">
            <fieldset>
			
			<div class="col-md-12">
			<!-- Text input-->
	            <div class="form-group">
	              	<div class="col-md-12">
	              		<label class="control-label" for="nombre_user ">Id Sección</label>  
	                  	<input id="id_seccion" name="id_seccion" type="text" placeholder="" class="form-control input-md" required readonly>
	              </div>
	            </div>
				<!-- Text input-->
	            <div class="form-group">
	              	<div class="col-md-12">
	              		<label class="control-label" for="nombre_user ">Nombre</label>  
	                  	<input id="nombre_seccion" name="nombre_seccion" type="text" placeholder="" class="form-control input-md" required>
	              	</div>
	            </div>
	            <div class="form-group">
	              <div class="col-md-12">
	              <label class="control-label" for="nombre_user ">Estado</label>  
	              <select name="estado" id="estado" class="form-control">
	              	<option value="">Selecione....</option>
	              	<option value="1">ACTIVO</option>
	              	<option value="2">INACTIVO</option>

	              </select>
	              </div>
	            </div>
			</div>
			<div class="col-md-12">
			<button type="submit" class="btn btn-primary btn-block" id="btn_sol_material">Modificar</button>

			</div>

        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script >
	
	function pasar_valores(obj){
		$('#editar_seccion').modal('show');
		$("#id_seccion").val($(obj).attr('data-seccion_id'));
		$("#nombre_seccion").val($(obj).attr('data-seccion_nombre'));

	}
</script>
<!-- Fin Modal -->