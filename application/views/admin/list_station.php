<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header righteous">Listado de Máquinas</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Máquinas
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                        <thead>
		                            <tr>

		                            	<th>#</th>
		                                <th>ID</th>
		                                <th>Nombre</th>
		                                <th>Sección</th>
		                                <th>Estado</th>

		                                <th>Acción</th>
		                            </tr>
		                        </thead>
		                        <tbody>
	                            <?php 
	                            $i=1;
				            	foreach ($ver_maquina->result() as $maquinar): ?>
					                <tr>
					                     <?php 
					                    $estado = $maquinar->estado;
					                    if ($estado == 1) {?>
					                    <td><?= $i++; ?></td>
					                    <td><?= $maquinar->maquina_id; ?></td>
					                    <td><?= $maquinar->maquina_nombre; ?></td>
					                    <td><?= $maquinar->seccion_nombre; ?></td>
					                    <td class="bg-success">ACTIVO</td>
					                    <?php
					                    }
					                    else { ?>
					                     <td><?= $i++; ?></td>
					                    <td><?= $maquinar->maquina_id; ?></td>
					                    <td><?= $maquinar->maquina_nombre; ?></td>
					                    <td><?= $maquinar->seccion_nombre; ?></td>
					                    <td class="bg-danger">INACTIVO</td>
					                    <?php
					                    }	
					                    ?>

					                    <td><a onclick="pasar_valores(this)" id="<?= $i ?>"  class="" data-maquina_id = "<?= $maquinar->maquina_id ?>" data-maquina_nombre="<?= $maquinar->maquina_nombre ?>" data-maquina_seccion="<?= $maquinar->seccion_nombre ?>">Editar</a> / <a href="<?= base_url(); ?>c_admin/delete_station/<?= $maquinar->maquina_id ?>" class="">Eliminar</a></td>
					                </tr>
					            <?php endforeach ?>
		                        </tbody>
		                    </table>
		                </div>
		                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
		</div>
	</div>
</div>


<!-- Modal Nueva solicitud-->
<div class="modal fade" id="editar_estacion" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" id="">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title righteous" id="myModalLabel"><!-- Form Name -->
            Edición de Maquinas
        </h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" action="editar_station">
            <fieldset>
			
			<div class="col-md-12">
	            <div class="form-group">
	              <div class="col-md-12">
	              <label class="control-label" for="id_usuario ">Id Usuario</label>  
	                  <input id="id_usuario" name="id_usuario" type="number" placeholder="" class="form-control input-md" required readonly>
	              </div>
	            </div>
	            <div class="form-group">
	              <div class="col-md-12">
	              <label class="control-label" for="nombre_user ">Nombre</label>  
	                  <input id="nombre_user" name="nombre_user" type="text" placeholder="" class="form-control input-md" required>
	              </div>
	            </div>
	            <div class="form-group">
                	<div class="col-md-12">
	            	<label class="control-label" for="tipo_user">Sección</label>
		              	<select id="user_seccion" name="user_seccion" class="form-control col-md-10" required>
			                <option value="">Seleccione...</option>
			                <?php 
			                  foreach ($list_seccion->result() as $list_seccionr): ?>
			                    <option value="<?= $list_seccionr->seccion_id; ?>"><?= $list_seccionr->seccion_nombre; ?></option>
			                  <?php endforeach ?>
		             	</select>
		              	<span class="help-block">*Seleccione la Sección correspondiente</span>  
		             </div>
	            </div>
	            <div class="form-group">
                	<div class="col-md-12">
	            	<label class="control-label" for="tipo_user">Estado</label>
		              	<select id="estado" name="estado" class="form-control col-md-10" required>
			                <option value="">Seleccione...</option>
			               <option value="1">ACTIVO</option>
			               <option value="2">INACTIVO</option>

		             	</select>
		              	<span class="help-block">*Seleccione la Sección correspondiente</span>  
		             </div>
	            </div>
	        </div>
			<div class="col-md-12">
				<button type="submit" class="btn btn-primary btn-block" id="btn_sol_material">Modificar</button>
			</div>							
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
 <script >
 	function pasar_valores(obj){
 		$('#editar_estacion').modal('show');
		$("#id_usuario").val($(obj).attr('data-maquina_id'));
		$("#nombre_user").val($(obj).attr('data-maquina_nombre'));
		 $valor =  $(obj).attr('data-maquina_seccion');
		 if ($valor == "Bobinado") {
		$("#user_seccion").val("1");
	}
	 if ($valor == "Produccion") {
		$("#user_seccion").val("2");
	}
	 if ($valor == "Almacen") {
		$("#user_seccion").val("3");
	}
	 if ($valor == "Tics") {
		$("#user_seccion").val("6");
	}



 	}

 </script>
<!-- Fin Modal -->