<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
 

?>
 <div id="wrapper">
    <div id="page-wrapper">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header righteous">Listado de Usuarios</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                Usuarios
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                        <thead>
		                            <tr>
		                            	<th>#</th>
		                                <th>ID</th>
		                                <th>Nombre</th>
		                                <th>Usuario</th>
		                                <th>Clave</th>
		                                <th>Sección</th>
		                                <th>Tipo</th>
		                                <th>Estado</th>
		                                <th>Acción</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        <?php 
	                            $i=1;
				            	foreach ($ver_user->result() as $usuarior): ?>
					                <tr>
					                    <?php 
					                    $estado = $usuarior->estado;
					                    if ($estado == 1) {?>
					                    	
					                    <td><?= $i++; ?></td>
					                    <td><?= $usuarior->usuario_id; ?></td>
					                    <td><?= $usuarior->usuario_nombre; ?></td>
					                    <td><?= $usuarior->usuario_user; ?></td>
					                    <td><?= $usuarior->usuario_clave; ?></td>
					                    <td><?= $usuarior->seccion_nombre; ?></td>
					                    <td><?= $usuarior->tipou_perfil; ?></td>
					                    <td class="bg-success">ACTIVO</td>
					                    <?php
					                    }

					                    else{?>
					                    <td><?= $i++; ?></td>
					                    <td><?= $usuarior->usuario_id; ?></td>
					                    <td><?= $usuarior->usuario_nombre; ?></td>
					                    <td><?= $usuarior->usuario_user; ?></td>
					                    <td><?= $usuarior->usuario_clave; ?></td>
					                    <td><?= $usuarior->seccion_nombre; ?></td>
					                    <td><?= $usuarior->tipou_perfil; ?></td>
					                    <td class="bg-danger">INACTIVO</td>
					                    <?php
					                    }
					                    ?>
					                    
					                    <td class="text-center">
					                   
					                    	<a id="<?= $i ?>" onclick="prueba(this)" class=""  data-user_id="<?= $usuarior->usuario_id; ?>" data-user_nombre="<?= $usuarior->usuario_nombre; ?>" data-user_user="<?= $usuarior->usuario_user; ?>" data-clave_user="<?= $usuarior->usuario_clave; ?>" data-seccion_user="<?= $usuarior->seccion_id; ?>" data-perfil_user="<?= $usuarior->tipou_perfil; ?>">Editar</a> /
					                    	<a href="<?= base_url(); ?>c_admin/delete_user/<?= $usuarior->usuario_id ?>" class="" data-id_user="<?= $usuarior->usuario_id; ?>">Eliminar</a></td>
					                </tr>
					            <?php endforeach ?>
		                        </tbody>
		                    </table>
		                </div>
		                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
		</div>
	</div>
</div>


<!-- Modal Nueva solicitud-->
<div class="modal fade" id="editar_user" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" id="modal_detail">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title righteous" id="myModalLabel"><!-- Form Name -->
            Edición de Usuario
        </h4>
      </div>
      <div class="modal-body oswald">
      <form class="form-horizontal"  action="editar_usuario">
				<fieldset>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-12">
							<label class=" control-label" for="id_user ">Id</label>  
									<input id="id_user" name="id_user" type="number"  placeholder="" class="form-control input-md" readonly>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
							<label class=" control-label" for="nombre_user ">Nombre</label>  
									<input id="nombre_user" name="nombre_user" type="text" placeholder="" class="form-control input-md" required>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
								<div class="col-md-12">
								<label class="control-label" for="tipo_user">Sección</label>
									<select id="user_seccion" name="user_seccion" class="form-control col-md-6" required>
										<option value="">Seleccione...</option>
										<?php 
											foreach ($list_seccion->result() as $list_seccionr): ?>
												<option value="<?= $list_seccionr->seccion_id; ?>"><?= $list_seccionr->seccion_nombre; ?></option>
											<?php endforeach ?>
								</select>
								</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
							<label class="control-label" for="tipo_user">Tipo</label>
								<select id="tipo_user" name="tipo_user" class="form-control" required>
									<option value="">Seleccione...</option>
									<option value="2">Patinador</option>
									<option value="3">Almacén</option>
									<option value="5">Producción</option>
									<option value="1">Administrador</option>
									<option value="4">Materiales</option>

								</select>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
							<label class="control-label" for="tipo_user">Estado</label>
								<select id="estado" name="estado" class="form-control" required>
									<option value="">Seleccione...</option>
									<option value="1">ACTIVO</option>
									<option value="2">INACTIVO</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-12">
							<label class="control-label" for="usuario_user ">Usuario</label>  
									<input id="usuario_user" name="usuario_user" type="text" placeholder="" class="form-control input-md" required>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
							<label class="control-label" for="clave_user ">Clave</label>  
									<input id="clave_user" name="clave_user" type="password" placeholder="" class="form-control input-md" required>
							</div>
						</div>
					</div>
				</fieldset>
				<fieldset>
						<div class="form-group">
							<div class="col-md-12">
							<button type="submit" class="btn btn-primary btn-block" id="btn_sol_material">Modificar</button>
						</div>
				</fieldset>
      </form>
      </div>
    </div>
  </div>
</div>
<!-- <script type="text/javascript" src="..../controllers/prueba.js"></script> -->
<script >
	function prueba(obj){
		
		$('#editar_user').modal('show');
		$("#id_user").val($(obj).attr('data-user_id'));
		$("#nombre_user").val($(obj).attr('data-user_nombre'));
		$("#usuario_user").val($(obj).attr('data-user_user'));
		$("#clave_user").val($(obj).attr('data-clave_user'));
		$("#user_seccion").val($(obj).attr('data-seccion_user'));
		 $dura = $(obj).attr('data-perfil_user');
		 if ($dura == "Administrador") {
		 	$("#tipo_user").val("1");
		 }
		 	 if ($dura == "Patinador") {
		 	$("#tipo_user").val("2");
		 }	 
		  if ($dura == "Almacen") {
		 	$("#tipo_user").val("3");
		 }
		 if ($dura == "Materiales") {

		 	$("#tipo_user").val("4");
		 }
		 	
		  if ($dura == "Produccion") {

		 	$("#tipo_user").val("5");
		 }
		 

		
		



	}
</script>
<!-- Fin Modal -->
