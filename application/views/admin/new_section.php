<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="wrapper">
  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header righteous">Crear Sección</h1>
      </div>
      <!-- /.col-lg-12 -->
    </div>

    <div class="row">
      <div class="col-md-12">
		<form class="form-horizontal" action="reg_section">
			<fieldset>
			<!-- Text input-->
			<div class="form-group">
			  <div class="col-md-12">
			  <label class="control-label" for="maquina_nombre">Nombre de Sección</label>  
			  <input id="maquina_nombre" name="maquina_nombre" type="text" placeholder="Nombre de Sección" class="form-control input-md" required>
			  <span class="help-block">*Digite el Nombre de la Sección a Agregar</span>  
			  </div>
			</div>

			<!-- Button -->
			<div class="form-group">
			  <label class="control-label" for="reg_station"></label>
			  <div class="col-md-12">
			    <button id="btn_reg_station" name="btn_reg_station" class="btn btn-primary btn-block">Registrar</button>
			  </div>
			</div>

			</fieldset>
		</form>
      </div>
    </div>
  </div>
</div>