<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="wrapper">
  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header righteous">Crear Máquina</h1>
      </div>
      <!-- /.col-lg-12 -->
    </div>

    <div class="row oswald">
      <div class="col-md-12">
		<form class="form-horizontal" action="reg_station">
			<fieldset>
			<div class="form-group">
			  <div class="col-md-6">
			  <label class="control-label" for="maquina_nombre">Nombre</label>  
			  <input id="maquina_nombre" name="maquina_nombre" type="text" placeholder="Nombre de Maquina / identificación" class="form-control input-md" required="">
			  <span class="help-block">*Digite un identificador para la Maquina</span>  
			  </div>
	
			  <div class="col-md-6">
			  <label class="control-label" for="maquina_seccion">Sección</label>
			    <select id="maquina_seccion" name="maquina_seccion" class="form-control">
			    	<option value="">Seleccione...</option>
			      	<?php 
                  	foreach ($list_seccion->result() as $list_seccionr): ?>
                      <option value="<?= $list_seccionr->seccion_id; ?>"><?= $list_seccionr->seccion_nombre; ?></option>
                  	<?php endforeach ?>
			    </select>
			  </div>
			</div>
			</fieldset>
			<fieldset>
			<div class="form-group">
			  <label class="control-label" for="reg_station"></label>
			  <div class="col-md-12">
			    <button id="btn_reg_station" name="btn_reg_station" class="btn btn-primary btn-block">Registrar</button>
			  </div>
			</div>
			</fieldset>
		</form>
      </div>
    </div>
  </div>
</div>