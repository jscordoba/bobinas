<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="wrapper">
  <div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header righteous">Crear usuario</h1>
      </div>
      <!-- /.col-lg-12 -->
    </div>

    <div class="row oswald">
      <div class="col-md-12">
        <form class="form-horizontal" action="reg_user">
          <fieldset>
            <div class="col-md-4">
               <!-- Text input-->
              <div class="form-group">
                <div class="col-md-12">
                <label class="control-label" for="id_usuario">Identificación</label>  
                <input id="id_usuario" name="id_usuario" type="number" placeholder="Cedula de Ciudadania" class="form-control input-md" required>
                <!-- <span class="help-block">*Digite el número de Identidad</span>   -->
                </div>
              </div>
              <!-- Text input-->
              <div class="form-group">
                <div class="col-md-12">
                <label class="control-label" for="nombre_usuario">Nombre</label>  
                <input id="nombre_usuario" name="nombre_usuario" type="text" placeholder="Nombre Completo" class="form-control input-md" required>
                <!-- <span class="help-block">*Digite el Nombre completo</span>   -->
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <!-- Select Basic -->
              <div class="form-group">
                <div class="col-md-12">
                <label class="control-label" for="user_seccion">Sección</label>
                  <select id="user_seccion" name="user_seccion" class="form-control">
                    <option value="">Seleccione...</option>
                    <?php 
                      foreach ($list_seccion->result() as $list_seccionr): ?>
                        <option value="<?= $list_seccionr->seccion_id; ?>"><?= $list_seccionr->seccion_nombre; ?></option>
                      <?php endforeach ?>
                  </select>
                  <!-- <span class="help-block">*Seleccione la Sección correspondiente</span>   -->
                </div>
              </div>
              <!-- Select Basic -->
              <div class="form-group">
                <div class="col-md-12">
                <label class="control-label" for="tipo_user">Perfil</label>
                  <select id="tipo_user" name="tipo_user" class="form-control" required>
                    <option value="">Seleccione...</option>
                    <option value="2">Patinador</option>
                    <option value="3">Almacén</option>
                    <option value="5">Materiales</option>
                    <option value="4">Producción</option>
                    <option value="1">Administrador</option>
                  </select>
                  <!-- <span class="help-block">*Seleccione el Nivel de Permisos</span>   -->
                </div>
              </div>

             
            </div>    
            <div class="col-md-4">
              <!-- Text input-->
              <div class="form-group">
                <div class="col-md-12">
                <label class="control-label" for="usuario">Usuario</label>  
                <input id="usuario" name="usuario" type="text" placeholder="Digite el nombre de usuario" class="form-control input-md" required>
                <!-- <span class="help-block">*Digite un Usuario de Acceso</span>   -->
                </div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <div class="col-md-12">
                <label class="control-label" for="clave">Clave</label>  
                <input id="clave" name="clave" type="password" placeholder="Ingrese la clave" class="form-control input-md" required>
                <!-- <span class="help-block">*Se sugiere sea mínimo de 8 dígitos</span>   -->
                </div>
              </div>
            </div>

          
          </fieldset>
          <fieldset>
            <div class="col-md-12">
              <span class="help-block">Asegurese de llenar todos los campos antes de intentar añadir el registro a la base de datos.</span>  
            </div>
            <div class="col-md-12">
              <!-- Button -->
              <div class="form-group">
                <label class="control-label" for="reg_new_user"></label>
                <div class="col-md-12 text-center">
                  <button id="btn_reg_new_user" name="btn_reg_new_user" class="btn btn-primary btn-block">Registrar</button>
                </div>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
