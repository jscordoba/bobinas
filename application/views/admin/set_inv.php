<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header righteous">Listado de Inventario</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Ajustar Inventario
		                <a data-toggle="modal" data-target="#modal-id" class="pull-right btn btn-success btn-sm"><i class="fa fa-plus" aria-hidden="true"></i></a>
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                        <thead>
		                            <tr>
		                            	<th>#</th>
		                                <th>Codigo Material</th>
		                                <th>Descripción</th>
		                                <th>Cantidad</th>
		                                <th class="hidden"> </th>
		                                <th>Bodega</th>
		                                <th>Cambiar?</th>
		                                <th>Editar</th>

		                            </tr>
		                        </thead>
		                        <tbody>
		                        <?php 
	                            $i=1;
				            	foreach ($list_inv->result() as $item): 
				            		if ($item->bodega_id == 2) {
				            		?>
					                <tr>
					                    <td><?= $i++; ?></td>
					                    <td><?= $item->material_id; ?></td>
					                    <td><?= $item->material_descripcion; ?></td>
					                    <td contenteditable class="cantidad"><?= $item->stockm_cantidad; ?></td>
					                    <td class="hidden bodegaid"><?= $item->bodega_id; ?></td>
					                    <td><?= $item->bodega_nombre; ?></td>
					                    <td><div class="checkbox">
										  <label><input type="checkbox" value="2">Bod.Prod</label>
										</div></td>

					                    <td><button class="btn btn-primary edit_inv btn-xs" data-id="<?=$item->stockm_id; ?>">Editar</button></td>
					                
					                </tr>
					            <?php
					        }else{?>
										<tr>
					                    <td><?= $i++; ?></td>
					                    <td><?= $item->material_id; ?></td>
					                    <td><?= $item->material_descripcion; ?></td>
					                    <td  class="cantidad"><?= $item->stockm_cantidad; ?></td>
					                    <td class="hidden bodegaid"><?= $item->bodega_id; ?></td>
					                    <td><?= $item->bodega_nombre; ?></td>
					                    <td><div class="checkbox">
										  <label><input type="checkbox" value="2">Bod.Prod</label>
										</div></td>

					                    <td><button class="btn btn-primary edit_inv btn-xs" data-id="<?=$item->stockm_id; ?>">Editar</button></td>
					                
					                </tr>
							<?php
					        }

					             endforeach ?>
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>


<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title righteous">Agregar Material</h4>
			</div>
			<form class="form-horizontal" action="javascript:reg_material()" id="form_add_material">
				<div class="modal-body oswald">
					<div class="row">
				      <div class="col-md-12">
				          <fieldset>
				            <div class="col-md-12">
				 
				              <div class="form-group">
				                
				                <div class="col-md-6"><label for="codigo">Codigo Material</label>  
				                <input id="codigo" name="codigo" type="number" placeholder="Digite Codigo Material" class="form-control input-md" required>
				                </div>
				                <div class="col-md-6"><label for="nombre_material">Nombre Material</label>  
				                <input id="nombre_material" name="nombre_material" type="text" placeholder="Digite el Nombre del Material" class="form-control input-md" required>
				                </div>
				              </div>
				            </div>
				          </fieldset>
				      </div>
				    </div>
				</div>
				<div class="modal-footer oswald">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Guardar</button>
				</div>
			</form>

		</div>
	</div>
</div>

