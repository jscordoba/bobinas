<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header righteous">Listado de Codigos</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<div class="row oswald">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                Ajustar Codigos y cantidades
		                <a data-toggle="modal" data-target="#modal-id1" class="pull-right">Añadir</a>
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body">
		                <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                        <thead>
		                            <tr>
		                            	<th>#</th>
		                                <th>Material</th>
		                                <th>Presentacion</th>
		                                <th>Cantidad</th>
		                                <th>Codigo de Barras</th>
		                                <th colspan="2">Acción</th>
		                                <th>Imprimir</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	<?php
									$i = 1;
									foreach ($list_codigos->result() as $item):
									?>
						                <tr>
						                    <td><?=$i++;?></td>
						                    <td><?=$item->material_id . ' - ' . $item->material_descripcion;?></td>
						                    <td><?=$item->presentacion;?></td>
						                    <td contenteditable class="cantidad"><?=$item->sc_cantidad;?></td>
						                    <td><?=$item->codbar;?></td>
						                    <td><button class="btn btn-primary edit_mat btn-xs" data-id="<?=$item->id;?>">Editar</button></td>
						                    <td><button class="btn btn-danger delete_mat btn-xs" data-id="<?=$item->id;?>">Eliminar</button></td>

						                    <td><a href="<?=base_url();?>ajax/codbar_pdf?codbar=<?=$item->codbar?>&veces=<?=1;?>" class="btn btn-edefault print_mat btn-xs" data-id="<?=$item->codbar;?>">Imprimir</a></td>
						                </tr>
									<?php
									endforeach?>

		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>

<!-- <div class="modal fade" id="modal-id2">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title righteous">Nuevo Codigo</h4>
			</div>
			<form class="" action="javascript:reg_codigo()" id="form_add_codigo" data-toggle="validator">
			<div class="modal-body oswald">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group ">
							<label for="material">Material:</label>
							<input type="number" class="form-control" name="material1" id="material1" onkeyup="existenciaMaterial(this)">
							<div class="help-block hidden">El material no existe</div>
							<input type="number" name="material" class="form-control hidden" id="material" value="">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group ">
							<label for="presentacion">Presentacion:</label>
							<select name="presentacion" id="presentacion" class="form-control" required>
								<option value="" selected disabled>Elegir presentacion</option>
								<option value="Estiba">Estiba</option>
								<option value="Chipa">Chipa</option>
								<option value="Rollo">Rollo</option>
								<option value="Carreto">Carreto</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					<label for="cantidad">Cantidad:</label>
					<input type="number" class="form-control" name="cantidad" id="cantidad" min="0" step=".01" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label for="codigo">Codigo:</label>
					<input type="text" name="codigo" class="form-control" id="codigo" readonly required>
				</div>
					</div>
				</div>
		</div>

			<div class="modal-footer oswald">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary btn-save">Guardar</button>
			</div>
			</form>
		</div>
	</div>
</div> -->
<div class="modal fade" id="modal-id1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title righteous">Nuevo Codigo</h4>
			</div>
			<form class="" action="javascript:reg_codigo()" id="form_add_codigo" data-toggle="validator">
			<div class="modal-body oswald">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered table-hover table-striped">
							<thead>
								<tr>
									<th>Material</th>
									<th>Presentación</th>
									<th>Cantidad</th>
									<th>Codigo</th>
									<th>Opciones</th>
								</tr>
							</thead>
							<tbody id="TrCodigos">
								<tr>
									<td class="hidden">
										<input type="number" name="material[]" class="form-control material" id="material" value="">
									</td>
									<td>
										<input type="number" min="0" class="form-control" name="material1[]" id="material1" onkeyup="existenciaMaterial(this)">
									</td>
									<td>
										<select name="presentacion[]" id="presentacion" class="form-control" onchange="changePresentacion()" required>
											<option value="" selected disabled>Elegir presentacion</option>
											<option value="Estiba">Estiba</option>
											<option value="Chipa">Chipa</option>
											<option value="Rollo">Rollo</option>
											<option value="Carreto">Carreto</option>
										</select>
									</td>
									<td>
										<input type="number" class="form-control cantidad" name="cantidad[]" id="cantidad" min="0" step=".01" onkeyup="changeCantidad(this)" required>
									</td>
									<td>
										<input type="text" name="codigo[]" class="form-control codigo" id="codigo" readonly required>
									</td>
									<td><button onclick="addcodigos()" type="button" class="btn btn-sm btn-default">Añadir</button></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer oswald">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary btn-save">Guardar</button>
			</div>
			</form>
		</div>
	</div>
</div>

<script src="<?=base_url('styles/bower_components/jquery/dist/jquery.js')?>"></script>

<script>
	var material = '0';
	var cantidad = '0';
	var fecha = '';
	var hora = '';

	function tiemecodebar(){
		
		var hoy = new Date();
	    var dd = hoy.getDate();
	    var mm = hoy.getMonth() + 1; /*hoy es 0!*/
	    var yyyy = hoy.getFullYear();
	    
	    if (dd < 10) {
	        dd = '0' + dd
	    }

	    if (mm < 10) {
	        mm = '0' + mm
	    }

	    fecha = yyyy + mm  + dd;

	    var f = new Date();
	    h = f.getHours();
	    m = f.getMinutes();
	    s = f.getSeconds();
	    
	    if (h < 10) {
	        h = '0' + h
	    }

	    if (m < 10) {
	        m = '0' + m
	    }

	    if (s < 10) {
	        s = '0' + s
	    }

	    hora = h  + m  + s;
	}

	function changeMaterial(object){
		tiemecodebar();
		material = $(object).parents('tr').find('input.material').val();

		// codebar_reload(material,cantidad,fecha,hora);
		$(object).parents('tr').find('input.codigo').val(material+''+cantidad+''+fecha+''+hora);
		//$(object).parents('tr').find('input.codigo').val(material);
	}

	function changeCantidad(object){
		tiemecodebar();
		cantidad = $(object).parents('tr').find('input.cantidad').val();
		
		// codebar_reload(material,cantidad,fecha,hora);
		$(object).parents('tr').find('input.codigo').val(material+''+cantidad+''+fecha+''+hora);
	}

	function changePresentacion(object){
		tiemecodebar();
		presentacion = $(object).parents('tr').find('input.presentacion[]').val();
		
		// codebar_reload(material,cantidad,fecha,hora);
		$(object).parents('tr').find('input.codigo').val(material+''+cantidad+''+fecha+''+hora);
	}

	function codebar_reload(a,b,c,d) {
		var codebar = a+b+c+d;
		$(o).parents('tr').find('input.codigos').val(codebar);
	}

</script>

