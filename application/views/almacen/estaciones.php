<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

    	 <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Estaciones</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>


		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
                    
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#estacion" data-toggle="tab">Estaciones / OT</a>

                            <!-- <li><a href="#otro" data-toggle="tab">Otro</a></li> -->
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="estacion">
                                <h4>Estaciones / OT</h4>
                                <div class="table-responsive col-md-8">
				                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
				                        <thead>
				                            <tr>
				                                <th>ID</th>
				                                <th>Nombre</th>
				                                <th>Sección</th>
				                               
				                            </tr>
				                        </thead>
				                        <tbody>
				                            <?php 
							            	foreach ($estaciones->result() as $estacioner): ?>
								                <tr>
								                    <td><?= $estacioner->maquina_id; ?></td>
								                    <td><?= $estacioner->maquina_nombre; ?></td>
								                    <td><?= $estacioner->seccion_nombre; ?></td>
								                    <td><a href="" class="btn btn-info btn-xs btn_detail_maquina" data-toggle="modal" data-target="#detail_maquina" data-maq_id="<?= $estacioner->maquina_id; ?>" data-maq_nombre="<?= $estacioner->maquina_nombre; ?>">Detalles</a></td>
								                </tr>
								            <?php endforeach ?>
				                        </tbody>
				                    </table>
		                		</div>
                            </div>
                            <!-- <div class="tab-pane fade" id="otro">
                                <h4>Otro</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div> -->
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
		    </div>
    <!-- /.col-lg-6 -->
		</div>
		<!-- /.row -->
	</div>
</div>
			
			<!-- Modal Nueva solicitud-->
            <div class="modal fade" id="detail_maquina" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document" id="modal_detail">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><!-- Form Name -->
                        Detalle de Maquina <b><span id="head_nombre_maquina"></span></b>
                    </h4>
                  </div>
                  <div class="modal-body">
                    <div class="table-responsive">
	                    <table class="table table-striped table-bordered table-hover">
	                        <thead>
	                            <tr>
	                                <th>#</th>

	                                <th>OT</th>
	                                <th>Item</th>
	                                <th>Descripción</th>
	                                <th>Cantidad Kgs</th>
	                                <th>Fecha Asignación</th>
	                                <th>Estado</th>
	                                <th>CodBar</th>
	                                <th>Imprimir</th>
	                                <th>Acción</th>

	                            </tr>
	                        </thead>
	                        <tbody id="tbody_detail_maq">
	                            
	                        </tbody>
	                    </table>
	        		</div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    <!-- generar codigo de barras y mostrar para impresion, al dar clic en asignar -->
                  </div>
                </div>
              </div>
            </div>
          <!-- Fin Modal -->
          	<!-- Modal Nueva solicitud-->

<div class="modal fade" id="detail_ot" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" id="modal_detail">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><!-- Form Name -->
				Detalle <b><span id="head_nombre_maquina"></span></b>
			</h4>
		</div>

			<div class="modal-body">
				<!-- <div class="table-responsive"> -->
					<!-- <button class="new_detailot btn btn-info">Nuevo</button><br/><br/> -->
					<ul class="nav nav-tabs">
						<li id="activo2" class="active"><a href="#lista" id="lista2" data-toggle="tab">Lista</a></li>
						<li><a href="#agregar" data-toggle="tab">Agregar</a></li>
						<li id="activo"><a   data-toggle="tab">Editar</a></li>

					</ul>
					<div class="tab-content">
						<div class="tab-pane fade in active" id="lista">
							<div class="panel panel-default">
								<div class="panel-heading">
									Activas
								</div>
								<div class="panel-body">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>#</th>
												<th >OT</th>
												<th >Item</th>
												<th >Descripción</th>
												<th >Cantidad</th>
												<th >Fecha Asignación</th>
												<th >Estado</th>
												<th >CodBar</th>
												<th >Maquina</th>

												<th>Acción</th>
											</tr>
										</thead>
										<tbody id="tbody_detail_ot">
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- parte dos  -->
						<div class="tab-pane fade" id="agregar">
							<div class="panel panel-default">
								<div class="panel-heading">
									 Agregar
								</div>
								<div class="panel-body">
									<form action="reg_detailot">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<label for="id_usuario">Item</label>  
												<input id="item2" name="item" type="text" placeholder="Item" class="form-control input-md" required readonly>
											</div>
										</div>  
										<div class="col-md-4">
											<div class="form-group">
											<label for="id_usuario">Descripción</label>  
												<input id="descripcion2" name="descripcion" type="text" placeholder="Descripción" class="form-control input-md" required  readonly> 
											</div>
										</div>  
										<div class="col-md-4">
											<div class="form-group">
											<label for="id_usuario">Cantidad</label>  
												<input id="cantidad2" name="cantidad" min="1" type="number" placeholder="Cantidad" class="form-control input-md" required >
											</div>
										</div> 
										<div class="col-md-4">
											<div class="form-group">
												<label  for="id_usuario">Fecha Asignación</label>  
												<input id="fecha2" name="fecha" type="text" placeholder="Fecha Asignación" class="form-control input-md" required readonly="">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="id_usuario">Codigo Barras</label>  
												<input id="codbar2" name="codbar" type="text" placeholder="Codigo Barras" class="form-control input-md" required readonly="">
											</div>
										</div>
											<div class="col-md-4">
												<div class="form-group">
											    <label for="id_usuario">Maquina</label>  
												    <select id="maquina2" name="maquina" class="form-control">
															 <option value="">Seleccione...</option>
                    										 <?php 
                    										 $query_stock = $this->db->query( "SELECT * FROM maquina");
                      foreach ($query_stock->result() as $list_seccionr): ?>
                        <option value="<?= $list_seccionr->maquina_nombre; ?>"><?= $list_seccionr->maquina_nombre; ?></option>
                      <?php endforeach ?>
												    </select>
												</div>
										</div>
										<div class="col-md-12">
											<button type="submit"  class="get_values_ot btn btn-success">Guardar</button>
										</div>
									</div>

									</form>
								</div>
							</div>
						</div>
						<!-- parte 3 -->
	                         <div class="tab-pane fade" id="editar">
							<div class="panel panel-default">
								<div class="panel-heading">
									Editar
								</div>
								<div class="panel-body">
									<form action="editar_detailot">
									<input id="id_detailot" name="id_detailot" type="hidden" placeholder="Item" class="form-control input-md"  >

									<div class="row">
									
										<div class="col-md-4">
											<div class="form-group">
											<label for="id_usuario">Item</label>  
												<input id="item3" name="item" type="text" placeholder="Item" class="form-control input-md" required >
											</div>
										</div>  
										<div class="col-md-4">
											<div class="form-group">
											<label for="id_usuario">Descripción</label>  
												<input id="descripcion3" name="descripcion" type="text" placeholder="Descripción" class="form-control input-md" required > 
											</div>
										</div>  
										<div class="col-md-4">
											<div class="form-group">
											<label for="id_usuario">Cantidad</label>  
												<input id="cantidad3" name="cantidad" type="number" min="1" placeholder="Cantidad" class="form-control input-md" required >
											</div>
										</div> 
										<div class="col-md-4">
											<div class="form-group">
												<label  for="id_usuario">Fecha Asignación</label>  
												<input id="fecha3" name="fecha" type="text" placeholder="Fecha Asignación" class="form-control input-md" required >
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="id_usuario">Codigo Barras</label>  
												<input id="codbar3" name="codbar" type="text" placeholder="Codigo Barras" class="form-control input-md" required readonly>
											</div>
										</div>
											<div class="col-md-4">
												<div class="form-group">
											    <label for="id_usuario">Maquina</label>  
												    <select id="maquina3" name="maquina" class="form-control" required>
															 <option value="">Seleccione...</option>
                    										 <?php 
                    										 $query_stock = $this->db->query( "SELECT * FROM maquina");
                      foreach ($query_stock->result() as $list_seccionr): ?>
                        <option value="<?= $list_seccionr->maquina_nombre; ?>"><?= $list_seccionr->maquina_nombre; ?></option>
                      <?php endforeach ?>
												    </select>
												  
												</div>
										</div>
										<div class="col-md-4">
												<div class="form-group">
											    <label for="id_usuario">Estado</label>  
										   <select id="estado3" name="estado" class="form-control" required>
															 <option value="">Seleccione...</option>
															 <option value="ACTIVO">ACTIVO</option>
															 <option value="FINALIZADO">FINALIZADO</option>


                    							
												    </select>
												    	</div>
										</div>
										<div class="col-md-12">
											<button type="submit"  class="get_values_ot btn btn-success">Editar</button>
										</div>
									</div>

									</form>
								</div>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
						<!-- generar codigo de barras y mostrar para impresion, al dar clic en asignar -->
					</div>
				<!-- </div> -->
			</div>
		</div>
	</div>
</div>
  

<script >
	function editar_detailot(obj){
		
		$("#id_detailot").val($(obj).data('id'));
		$("#item3").val($(obj).data('item'));
		$("#id_detalle_ot3").val($(obj).data('id_detalle_ot'));


		$("#descripcion3").val($(obj).data('descripcion'));
		$("#cantidad3").val($(obj).data('cantidad'));
		$("#cantidad3").attr('max', $(obj).data('cantidad'));
		$("#codbar3").val($(obj).data('codbar'));
		$("#fecha3").val($(obj).data('fecha'));
		$("#maquina3").val($(obj).data('maquina'));



		$("#activo").attr('class','active');
		$("#activo2").attr('class','');

		

		 
	
	}
		function valores(obj){
		$( "#lista2" ).trigger( "click" );
		var obj = $(obj).data('codbar');
		$.post('../ajax/detail_ot_seguimiento', {obj: obj}, function(data, textStatus, xhr) {
   			//alert(data);

   			// console.log("success get info machines");
   		 	
   		 	if (data!="") {
   		 		$('#tbody_detail_ot').empty();
   		 		$('#tbody_detail_ot').append(data);
   		 	}else{
   		 		$('#tbody_detail_ot').html('<td colspan="7" class="text-center"><h4>--------- Undefined ---------</h4></td>');
   		 	}
   		});
	}
	function get_values_ot(obj){
	
		var d = new Date();
		$("#item2").val($(obj).data('item'));

		$("#descripcion2").val($(obj).data('descripcion'));
		$("#cantidad2").val($(obj).data('cantidad'));
		$("#cantidad2").attr('max', $(obj).data('cantidad'));

		$("#codbar2").val($(obj).data('codbar'));

		$("#fecha2").val(d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()+"  "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds());


	}
</script>

