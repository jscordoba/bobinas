<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Importar Materiales</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="col-md-12" >
            <!-- <form   enctype="multipart/form-data" action="import_file"> -->
            <?php echo form_open_multipart('c_almacen/import_file');?>
            	<input type="file" name="import_file" id="import_file" required>
            <br/>
        	<button type="submit" class="btn btn-success btn-block " id="btn-new-entry" ">Sincronizar Bandejas</button>
            </form>
            
        </div>
        <br/>
        <br/>
		<hr>
		<div class="tab-pane fade in active col-md-12">
		    <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover " id="dataTables-example">
		             <thead>
		                <tr>
		                    <th>Id Stock</th>
                            <th>Id Material</th>
                            <th>Descripción Material</th>
		                    <th>Cantidad</th>
		                    <th>Id Bodega</th>

		                </tr>
		            </thead>
		            <tbody>
		            	<?php foreach ($import->result() as $stockr): ?>
		                <tr>
		                    <td><?= $stockr->stockm_id; ?></td>
		                    <td><?= $stockr->material_id; ?></td>
		                    <td><?= $stockr->material_descripcion; ?></td>
	                        <td><?= $stockr->stockm_cantidad; ?></td>
	                        <td><?= $stockr->bodega_id; ?></td>


		                </tr>
		            <?php endforeach ?>
		            </tbody>
		        </table>
			</div>
		</div>
	</div>
</div>




