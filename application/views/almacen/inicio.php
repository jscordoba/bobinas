<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header righteous">Inicio</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- /.row  CASILLAS DE CONTADORES-->
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="righteous">Solicitudes</h4>
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge"><?=$indicador_pdt;?></div>
                                    <div>Pendientes</div>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge"><?=$indicador_asig;?></div>
                                    <div>Entregados</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=base_url();?>c_almacen/sol_pendientes">
                            <div class="panel-footer oswald">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <h4 class="righteous">Reintegros</h4>
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge"><?=$indicador_sol_rein;?></div>
                                    <div>Pendientes</div>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge"><?=$indicador_recib_rein;?></div>
                                    <div>Recibidos</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=base_url();?>c_almacen/sol_reintegros">
                            <div class="panel-footer oswald">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <!-- TABLA -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading righteous">
                            Últimos Movimientos
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body oswald">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Descripción</th>
                                        <th>Origen</th>
                                        <th>Destino</th>
                                        <th>Cantidad</th>
                                        <th>Fecha Movimiento</th>
                                        <th>Tipo</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($movimientos->result() as $movimientor):
                                        if ($movimientor->tipo == 1) {

                                            ?>
                                            <tr>
                                                <td><?=$i++;?></td>
                                                <td><?=$movimientor->material_id;?></td>
                                                <td><?=$movimientor->material_descripcion;?></td>
                                                <td><?=$movimientor->bodega_origen;?></td>
                                                <td><?=$movimientor->bodega_destino;?></td>
                                                <td><?=$movimientor->movimiento_cantidad;?> Kgs</td>
                                                <td><?=$movimientor->movimiento_date;?></td>
                                                <td style="background-color: #1565c0; color: white;">NORMAL</td>



                                            </tr>
                                        <?php
                                        } else {
                                        ?>
                                                <tr>
                                                <td><?=$i++;?></td>
                                                <td><?=$movimientor->material_id;?></td>
                                                <td><?=$movimientor->material_descripcion;?></td>
                                                <td><?=$movimientor->bodega_origen;?></td>
                                                <td><?=$movimientor->bodega_destino;?></td>
                                                <td><?=$movimientor->movimiento_cantidad;?> Uds</td>
                                                <td><?=$movimientor->movimiento_date;?></td>
                                                <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
                                                </tr>
                                        <?php
                                        }

                                    endforeach?>
                                </tbody>
                            </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
            <!-- /.row -->

    </div>



    <!-- /#wrapper -->
