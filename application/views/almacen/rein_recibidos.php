<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div id="wrapper">
    <div id="page-wrapper">

    	<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header righteous">Histórico de Reintegros Recibidos</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>


		<div class="tab-pane fade in active oswald">
		    <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		            <thead>
		                <tr>
		                    <th>Id</th>
		                    <th>Item</th>
		                    <th>Descripción</th>
		                    <th>Cantidad Recibida </th>
		                    <th>Origen</th>
		                    <th>Destino</th>


		                    <th>Fecha Reintegro</th>
		                    <th>Tipo</th>

		                </tr>
		            </thead>
		            <tbody>
		                <?php
$c = 1;
foreach ($hist_reintegros->result() as $hist_reintegror) {
    $this->db->where('bodega_id', $hist_reintegror->bodega_id_origen);
    $result = $this->db->get('bodega');
    foreach ($result->result() as $org) {
        $origen = $org->bodega_nombre;
    }
    $this->db->where('bodega_id', $hist_reintegror->bodega_id_destino);
    $result = $this->db->get('bodega');
    foreach ($result->result() as $org2) {
        $destino = $org2->bodega_nombre;
    }
    if ($hist_reintegror->tipo == 1) {
        ?>
		                <tr>
		                	<td><?=$c++;?></td>
		                    <td><?=$hist_reintegror->material_id;?></td>
		                    <td><?=$hist_reintegror->material_descripcion;?></td>
		                    <td><?=$hist_reintegror->movimiento_cantidad;?> Kgs</td>
		                    <td><?=$origen;?></td>
		                    <td><?=$destino;?></td>
		                    <td><?=$hist_reintegror->movimiento_date;?></td>
						    <td style="background-color: #1565c0; color: white;">NORMAL</td>
		                </tr>
		            <?php } else {
        ?>
		            	<tr>
		                	<td><?=$c++;?></td>
		                    <td><?=$hist_reintegror->material_id;?></td>
		                    <td><?=$hist_reintegror->material_descripcion;?></td>
		                    <td><?=$hist_reintegror->movimiento_cantidad;?> Uds</td>
		                     <td><?=$origen;?></td>
		                    <td><?=$destino;?></td>
		                    <td><?=$hist_reintegror->movimiento_date;?></td>
						    <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
		                </tr>
		            	<?php
}
}

?>
		            </tbody>
		        </table>
			</div>
		</div>
	</div>
</div>