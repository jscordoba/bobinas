<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div id="wrapper">
    <div id="page-wrapper">

    	<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header righteous">Historico de Entrega de Material</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>


		<div class="tab-pane fade in active oswald">
		    <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		            <thead>
		                <tr>
		                    <th>Id</th>
		                    <th>Item</th>
		                    <th>Descripción</th>
		                    <th>Fecha Solicitud</th>
		                    <th>Fecha Entrega</th>
		                    <th>Cantidad Entregada</th>
		                    <th>Imprevistos</th>
							<th>Tipo</th>
		                    <th>Detalles</th>
		                </tr>
		            </thead>
		            <tbody>
	                <?php
$c = 1;
foreach ($sol_entregados->result() as $sol_entregador):
    if ($sol_entregador->tipo == 1) {

        ?>
															                <tr>
															                	<td><?=$c++;?></td>
															                    <td><?=$sol_entregador->material_id;?></td>
															                    <td><?=$sol_entregador->material_descripcion;?></td>
															                    <td><?=$sol_entregador->fecha;?></td>
															                    <td><?=$sol_entregador->movimiento_date;?></td>
															                    <td><?=$sol_entregador->movimiento_cantidad;?> Kgs</td>
															                    <td><?=$sol_entregador->imprevistos;?></td>

																			    <td style="background-color: #1565c0; color: white;">NORMAL</td>
															                    <td class="text-center">
															                    	<button data-toggle="modal" data-target="#datail_und" data-movimiento_id="<?=$sol_entregador->movimiento_id;?>" data-bandeja="0"  class="btn btn-success btn-sm btn_detail_unds">Ver</button>
															                    </td>
															                </tr>
															            <?php
    } else {
        ?>
															        		<tr>
															                	<td><?=$c++;?></td>
															                    <td><?=$sol_entregador->material_id;?></td>
															                    <td><?=$sol_entregador->material_descripcion;?></td>
															                    <td><?=$sol_entregador->movimiento_date;?></td>
															                    <td><?=$sol_entregador->movimiento_date;?></td>
															                    <td><?=$sol_entregador->movimiento_cantidad;?> Uds</td>
															                    <td><?=$sol_entregador->imprevistos;?></td>

																			    <td style="background-color: #EF6C00; color: white;">BANDEJA</td>

															                    <td class="text-center">
															                    	<button data-toggle="modal" data-target="#datail_und2" data-movimiento_id="<?=$sol_entregador->movimiento_id;?>" data-bandeja="<?=$sol_entregador->material_id?>"   class="btn btn-success btn-sm btn_detail_unds">Ver</button>
															                    </td>
															                </tr>
															        				<?php
    }
endforeach?>
		            </tbody>
		        </table>
			</div>
		</div>
	</div>
</div>



<!-- Modal detalle de Kilogramos-->
<div class="modal fade" id="datail_und" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title righteous" id="myModalLabel"><!-- Form Name -->
            Detalle de Unidades en Stock
        </h4>
      </div>
      <div class="modal-body oswald">
        <form class="form-horizontal" id="">
            <fieldset>

            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	            <thead>
	                <tr>
	                    <th>#</th>
	                    <th>Unidades</th>
	                    <th>Presentacion</th>
	                    <th>Cantidad</th>
	                    <th>Subtotal</th>
	                    <th>Patinador</th>
		              <!--   <th>Codigo</th>
		                <th>PDF</th> -->
	                </tr>
	            </thead>
	            <tbody class="tb_detail_und">
	            </tbody>
	        </table>
            </fieldset>
        </form>
      </div>
      <div class="modal-footer oswald">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        <!-- <button type="button" class="btn btn-primary" id="btn_sol_material">Solcitar</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Fin Modal -->
<!-- Modal detalle de Unidades-->
<div class="modal fade" id="datail_und2" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title righteous" id="myModalLabel"><!-- Form Name -->
            Detalle de Unidades en Stock
        </h4>
      </div>
      <div class="modal-body oswald">
        <form class="form-horizontal" id="">
            <fieldset>

            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	            <thead>
	                <tr>
	                    <th>#</th>
	                    <th>Unidades</th>
	                    <th>Presentacion</th>
	                    <th>Cantidad</th>
	                    <th>Subtotal</th>

	                    <th>Patinador</th>
		                <th>Codigo</th>
		                <!-- <th>PDF</th> -->
	                </tr>
	            </thead>
	            <tbody class="tb_detail_und">
	            </tbody>
	        </table>
            </fieldset>
        </form>
        <table  class="table table-striped table-bordered table-hover">
        	<thead>
        		<tr>
        			<td colspan="10" class="righteous">DETALLES OT</td>
        		</tr>
        		<tr>
        			<td>#</td>
        			<th>Bandeja</th>
        			<th>Item</th>
        			<th>Descripción</th>
        			<th>Bodega</th>
        			<th>Cantidad</th>
        		</tr>
        	</thead>
        	<tbody class="td_detail_und_detail">
            <tr><td align="center" colspan="6">Datos no disponibles para tipo NORMAL</td> </tr>

        	</tbody>
        </table>
      </div>
      <div class="modal-footer oswald">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        <!-- <button type="button" class="btn btn-primary" id="btn_sol_material">Solcitar</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Fin Modal -->