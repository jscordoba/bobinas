<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div id="wrapper">
    <div id="page-wrapper">
    	<div class="row">

            <div class="col-lg-12">
                <h1 class="page-header righteous">Solicitudes de Material Por Preparar</h1>
            </div>
        </div>
		<div class="tab-pane fade in active oswald">
			<a href="#" id="sincronizar_sqlserver" class="btn btn-primary btn-xs">Realizar Carga</a><br><br>
		    <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		            <thead>
		                <tr>
		                    <th>ID</th>
		                    <th>Item</th>
		                    <th>Descripción</th>
		                    <th>Fecha</th>
		                    <th>Cantidad Disponible</th>
		                    <th>Tipo</th>
		                    <th>Cantidad Solicitada</th>
		                    <th>Traslado</th>
		                </tr>
		            </thead>
		            <tbody>
		                <?php foreach ($sol_pendientes->result() as $sol_pendienter):
    if ($sol_pendienter->tipo == 1) {
        ?>
									                <tr>
									                    <td><?=$sol_pendienter->solicitudm_id;?></td>
									                    <td><?=$sol_pendienter->material_id;?></td>
									                    <td><?=$sol_pendienter->material_descripcion;?></td>
									                    <td><?=$sol_pendienter->solicitudm_date;?></td>
									                    <td><?=$sol_pendienter->stockm_cantidad;?> Kgs</td>
													    <td style="background-color: #1565c0; color: white;">NORMAL</td>

									                    <?php
    if ($sol_pendienter->stockm_cantidad < $sol_pendienter->solicitudm_cantidad) {
            ?>
									                    	<td class="bg-danger"><?=$sol_pendienter->solicitudm_cantidad;?> Kgs</td>
									                    	<td><button class="btn btn-primary btn-xs btn_trasladar_prod" data-toggle="modal" data-target="#datail_und"
									                    		data-stockm_cantidad="<?=$sol_pendienter->stockm_cantidad;?>"
									                    		data-sol_id="<?=$sol_pendienter->solicitudm_id;?>"
									                    		data-mat_id="<?=$sol_pendienter->material_id;?>"
									                    		data-mat_descrip="<?=$sol_pendienter->material_descripcion;?>"
									                    		data-mat_cant="<?=$sol_pendienter->solicitudm_cantidad;?>"
									                    		data-tipo="<?=$sol_pendienter->tipo;?>"
									                    		data-imprevistos="<?=$sol_pendienter->imprevistos;?>">Preparar</button></td>
									                    	</td>
									                    <?php
    } else {?>
									                		<td class="bg-success"><?=$sol_pendienter->solicitudm_cantidad;?> Kgs</td>
									                    	<td><button class="btn btn-primary btn-xs btn_trasladar_prod" data-toggle="modal" data-target="#datail_und"
									                    		data-stockm_cantidad="<?=$sol_pendienter->stockm_cantidad;?>"
									                    		data-sol_id="<?=$sol_pendienter->solicitudm_id;?>"
									                    		data-mat_id="<?=$sol_pendienter->material_id;?>"
									                    		data-mat_descrip="<?=$sol_pendienter->material_descripcion;?>"
									                    		data-mat_cant="<?=$sol_pendienter->solicitudm_cantidad;?>"
									                    		data-tipo="<?=$sol_pendienter->tipo;?>"
									                    		data-imprevistos="<?=$sol_pendienter->imprevistos;?>">Preparar</button></td>
									                	<?php
    }
        ?>
									                </tr>
									                <?php
    } else {
        ?>
													        	    <tr>
									                    <td><?=$sol_pendienter->solicitudm_id;?></td>
									                    <td><?=$sol_pendienter->material_id;?></td>
									                    <td><?=$sol_pendienter->material_descripcion;?></td>
									                    <td><?=$sol_pendienter->solicitudm_date;?></td>
									                    <td><?=$sol_pendienter->stockm_cantidad;?> Uds</td>
													    <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
									                    <?php
    if ($sol_pendienter->stockm_cantidad < $sol_pendienter->solicitudm_cantidad) {
            ?>
									                    	<td class="bg-danger"><?=$sol_pendienter->solicitudm_cantidad;?> Uds</td>
									                    	<td><button class="btn btn-primary btn-xs btn_trasladar_prod2" data-toggle="modal" data-target="#modal-bandeja"
									                    		data-stockm_cantidad2="<?=$sol_pendienter->stockm_cantidad;?>"
									                    		data-sol_id2="<?=$sol_pendienter->solicitudm_id;?>"
									                    		data-mat_id2="<?=$sol_pendienter->material_id;?>"
									                    		data-mat_descrip2="<?=$sol_pendienter->material_descripcion;?>"
									                    		data-mat_cant2="<?=$sol_pendienter->solicitudm_cantidad;?>"
									                    		data-tipo2="<?=$sol_pendienter->tipo;?>"
									                    		data-imprevistos2="<?=$sol_pendienter->imprevistos;?>">Preparar</button></td></td>
									                    <?php
    } else {?>
									                		<td class="bg-success"><?=$sol_pendienter->solicitudm_cantidad;?> Uds</td>
									                    	<td><button class="btn btn-primary btn-xs btn_trasladar_prod2" data-toggle="modal" data-target="#modal-bandeja"
									                    		data-stockm_cantidad2="<?=$sol_pendienter->stockm_cantidad;?>"
									                    		data-sol_id2="<?=$sol_pendienter->solicitudm_id;?>"
									                    		data-mat_id2="<?=$sol_pendienter->material_id;?>"
									                    		data-mat_descrip2="<?=$sol_pendienter->material_descripcion;?>"
									                    		data-mat_cant2="<?=$sol_pendienter->solicitudm_cantidad;?>"
									                    		data-tipo2="<?=$sol_pendienter->tipo;?>"
									                    		data-imprevistos2="<?=$sol_pendienter->imprevistos;?>">Preparar</button></td>
									                	<?php
    }
        ?>
									                </tr>
													        <?php
    }
endforeach?>
		            </tbody>
		        </table>
			</div>
		</div>
		<div class="row">
    		 <div class="col-lg-12">
                <h1 class="page-header righteous">Solicitudes de Material Pendientes por Entregar</h1>
            </div>
        </div>
		<div class="tab-pane fade in active oswald">
		    <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables-exampl">
		            <thead>
		                <tr>
		                    <th>Codigo de solicitud</th>
		                    <th>Item</th>
		                    <th>Descripción</th>
		                    <th>Imprevistos</th>
		                    <th>Tipo</th>


		                </tr>
		            </thead>
		            <tbody>
		                <?php foreach ($sol_alistar->result() as $sol_pendienter):
    if ($sol_pendienter->tipo == 1) {
        ?>
									                <tr>
									                    <td><?=$sol_pendienter->id_detalle;?></td>
									                    <td><?=$sol_pendienter->material_id;?></td>
									                    <td><?=$sol_pendienter->material_descripcion;?></td>
									                    <td><?=$sol_pendienter->imprevistos;?></td>

													    <td style="background-color: #1565c0; color: white;">NORMAL</td>



									                </tr>
									                <?php
    } else {
        ?>
													        	    <tr>
									                    <td><?=$sol_pendienter->solicitudm_id;?></td>
									                    <td><?=$sol_pendienter->material_id;?></td>
									                    <td><?=$sol_pendienter->material_descripcion;?></td>
									                    <td><?=$sol_pendienter->imprevistos;?></td>
													    <td style="background-color: #EF6C00; color: white;">BANDEJA</td>

									                </tr>
													        <?php
    }
endforeach?>
		            </tbody>
		        </table>
			</div>
		</div>
	</div>
</div>
<!-- Modal Detalles Stock-->
<div class="modal fade" id="datail_und" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document" ><!-- id="modal_detail" -->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title righteous" id="myModalLabel"><!-- Form Name -->
            Asignar Unidades  <b><span id="tittle_detlles"></span><span> Kgs</span></b>
        </h4>
      	</div>
      	<div class="modal-body oswald">
        	<form id="form_traslado_prod">
        	<fieldset>
        		<div class="col-md-12">
        			<table class="table">
        				<table class="table table-striped table-bordered table-hover">
        					<thead>
        						<tr>
        							<th>ID</th>
        							<th>ITEM</th>
        							<th>DESCRIPCIÓN</th>
        							<th>CANTIDAD</th>
        							<th>SOLICITUD</th>
        						</tr>
        					</thead>
        					<tbody>
        						<tr>
        							<td id="idver"></td>
        							<td id="itemver"></td>
        							<td id="descripcionver"></td>
        							<!-- <td id="tipover"></td> -->
        							<td id="solicitudver"></td>
        							<td id="cantidadver"></td>

        						</tr>
        					</tbody>
        				</table>
        			</table>
        		</div>
	        	<div class="col-md-12">
		            <table class="table table-striped table-bordered table-hover" id="table_traslado">
			            <thead>
			                <tr>
			                    <th width="200">Codigo</th>
			                    <th width="300">Presentación</th>
			                    <th class="hidden" width="70">Unidades</th>
			                    <th>Kgs Disponibles</th>
			                    <th width="200">Cantidad en Kgs</th>
			                    <th width="200">Subtotal</th>

			                   <th class="text-center"><span title="Nuevo"  class="btn btn-success btn-sm btn_trasladar_prod_mod glyphicon glyphicon-plus"></span></th>
			                </tr>
			            </thead>


						<tbody id="add_traslado">
			            	<tr>
		            		<td>
		            			<input name = "codigo[]" class="form-control codigo"   required>
		            		</td>
							<td width="300">
								<input name="presentacion[]" id="3" value="<--- BUSCAR" class="form-control presentacion" readonly required>
							</td>
							<td class="hidden" width="70"><input type="number" id="1" name="und_nueva[]" min="1" value="1" class="form-control text-center inp_unds " required></td>
							<td width="200"><input type="number" value="0" class="form-control text-center inp_cantidad_disponible" readonly></td>
							<td width="200"><input type="number" id="2" name="cantidad_und[]" min="1" value="0" class="form-control text-center inp_cantidad" required></td>

							<td width="200"><label for="" class="form-control text-center lbl_subtotal">0</label></td>
						</tr>
			            </tbody>

						<input type="hidden" id="alm_input_sol_id" name="sol_id" required>
						<input type="hidden" id="alm_input_mat_id" name="mat_id" required>
						<input type="hidden" id="alm_input_mat_descrip" name="mat_descrip" required>
						<input type="hidden" id="alm_input_mat_cant" name="mat_cant" required>
						<input type="hidden" id="alm_input_stock_cantidad" name="stock_cantidad" required>
						<input type="hidden" id="tipo" name="tipo" required>


			        </table>

	        	</div>
				<div class="col-md-12">
					<label for="">Imprevistos</label>
					<textarea name="imprevistos" class="form-control imprevistos" id="imprevistos" rows="3" placeholder=""></textarea>
				</div>

	        	<div class="col-md-12">
	        		<div class="col-md-4 text-center">
			    		<label for="">Disponible</label><br>
			    		<label for="" class="text-success" id="cant_disponible"></label><span> Kgs</span>
	        		</div>
	        		<div class="col-md-4 text-center">
			    		<label for="">Queda</label><br>
			    		<label for="" class="text-danger" id="cant_uso">0</label><span> Kgs</span>
	        		</div>
	        		<div class="col-md-4 text-center" id="cualquiera">
			    		<label for="">Total</label><br>
			    		<label for="" class="text-primary" id="cant_total">0</label><span> Kgs</span>
			    		</div>
	        	</div>

			</div>

			<div class="modal-footer oswald">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" disabled class="btn btn-primary" id="btn_reg_traslado">Preparar</button>
			</div>

			</fieldset>
		</form>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-bandeja">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title righteous">Asignar Unidades <span id="tittle_detalles2"></span></h4>
			</div>
			<div class="modal-body oswald">
				<form id="form_traslado_prod2">
        	<fieldset>
	        	<div class="col-md-12">
		            <table class="table table-striped table-bordered table-hover" id="table_traslado2">
			            <thead>
			                <tr>
			                	<th>Codigo</th>
			                	<th>Descripción</th>
			                    <th >Unds.Disponibles</th>
			                    <th>Unidades</th>
			                    <th >Subtotal</th>

			                </tr>
			            </thead>
		            	<tr>
		            		<td>
						    <input name = "codigo2[]" class="form-control codigo2"   required>
						    </td>
						    <td width="200">
								<input name="presentacion2[]" class="form-control presentacion2" value="<--- Buscar Codigo" readonly required>
							</td>
							<td width="200"><input type="number" value="0" class="form-control text-center inp_cantidad_disponible2" readonly></td>
							<td width="70"><input type="number" id="2" name="und_nueva[]" min="1" value="0" class="form-control text-center inp_unds2" required></td>
							<td width="200"><label for="" class="form-control text-center lbl_subtotal2">0</label></td>

						</tr>
						<tbody id="add_traslado2">
			            </tbody>
						<input type="hidden" id="alm_input_sol_id2" name="sol_id" required>
						<input type="hidden" id="alm_input_mat_id2" name="mat_id" required>
						<input type="hidden" id="alm_input_mat_descrip2" name="mat_descrip" required>
						<input type="hidden" id="alm_input_mat_cant2" name="mat_cant" required>
						<input type="hidden" id="alm_input_stock_cantidad2" name="stock_cantidad" required>
						<input type="hidden" id="tipo" value="2" name="tipo2" required>
			        </table>
	        	</div>
	        	<div class="col-md-12">
	        		<label for="">Imprevistos</label>
					<textarea name="imprevistos" class="form-control imprevistos2" id="imprevistos2" rows="3" placeholder=""></textarea>
				</div>
	        	<div class="col-md.12">

	        		<div class="col-md-3">
			    		<label for="">Disponible</label>
			    		<label for="" class="text-success" id="cant_disponible2"></label><span> Uds</span>
	        		</div>
	        		<div class="col-md-3">
			    		<label for="">Queda</label>
			    		<label for="" class="text-danger" id="cant_uso2">0</label><span> Uds</span>
	        		</div>
	        		<div class="col-md-3" id="cualquiera">
			    		<label for="">Total</label>
			    		<label for="" class="text-primary" id="cant_total2">0</label><span> Uds</span>
			    		</div>
	        	</div>
			</div>
			<div class="modal-footer oswald">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" disabled class="btn btn-primary" id="btn_reg_traslado2">Preparar</button>
			</div>
			</fieldset>
		</form>
			</div>
		</div>
	</div>
</div>


