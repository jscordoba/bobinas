<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div id="wrapper">
    <div id="page-wrapper">

    	<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header righteous">Solicitudes de Reintegro Pendientes</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <form id="form_recibir_reintegro">
	        <div class="dataTable_wrapper oswald">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		            <thead>
		                <tr>
		                    <th>ID</th>
		                    <th>Item</th>
		                    <th>Descripción</th>
		                    <th>Cantidad Reintegro</th>
		                    <th>Fecha Solicitud</th>
		                    <th>Observación</th>
		                    <th >Tipo</th>
		                    <td>Detalles</td>
		                    <th>Confirmar</th>
		                </tr>
		            </thead>
		            <tbody>
	                <?php foreach ($sol_reintegros->result() as $sol_reintegror) {
    if ($sol_reintegror->tipo == 1) {
        ?>
		                <tr>
		                	<td><?=$sol_reintegror->solicitudr_id;?></td>
		                    <td><?=$sol_reintegror->material_id;?></td>
		                    <td><?=$sol_reintegror->material_descripcion;?></td>
		                    <td><?=$sol_reintegror->solicitudr_cantidad;?> Kgs</td>
		                    <td><?=$sol_reintegror->solicitudr_date;?></td>
		                    <td><?=$sol_reintegror->solicitudr_observacion;?></td>
						    <td style="background-color: #1565c0; color: white;">NORMAL</td>
						    <td> <button type="button" class="btn btn-default" id="btn-detalles-reintegros" data-movimiento = "<?=$sol_reintegror->movimiento_id;?>"  data-toggle="modal" data-target="#modal-id" >Detalles</button></td>
		                    <td><button id="btn_recibir_reintegro" type="button" onclick="recibir_reintegro(this)" class="btn btn-success btn-xs" data-soliditudr_id="<?=$sol_reintegror->solicitudr_id;?>" data-material_id="<?=$sol_reintegror->material_id;?>" data-material_descripcion="<?=$sol_reintegror->material_descripcion;?>" data-codigo="<?=$sol_reintegror->numero_reintegro;?>" data-solicitudr_cantidad="<?=$sol_reintegror->solicitudr_cantidad;?>" data-bodega="<?=$sol_reintegror->bodega_id_destino?>" data-tipo = "1" data-patinador="<?=$sol_reintegror->patinador;?>">Recibido</button></td>
		                </tr>
		            <?php } else {
        ?>
		            	    <tr>
		                	<td><?=$sol_reintegror->solicitudr_id;?></td>
		                    <td><?=$sol_reintegror->material_id;?></td>
		                    <td><?=$sol_reintegror->material_descripcion;?></td>
		                    <td><?=$sol_reintegror->solicitudr_cantidad;?> Uds</td>
		                    <td><?=$sol_reintegror->solicitudr_date;?></td>
		                    <td><?=$sol_reintegror->solicitudr_observacion;?></td>
		                    <td><?=$sol_reintegror->numero_reintegro;?></td>
						    <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
						    <td> <button type="button" class="btn btn-default" id="btn-detalles-reintegros" data-movimiento = "<?=$sol_reintegror->movimiento_id;?>"  data-toggle="modal" data-target="#modal-id" data-bandeja="<?=$sol_reintegror->material_id;?>">Detalles</button></td>
		                    <td><button id="btn_recibir_reintegro" type="button" onclick="recibir_reintegro(this)" class="btn btn-success btn-xs" data-soliditudr_id="<?=$sol_reintegror->solicitudr_id;?>" data-material_id="<?=$sol_reintegror->material_id;?>" data-material_descripcion="<?=$sol_reintegror->material_descripcion;?>" data-codigo="<?=$sol_reintegror->numero_reintegro;?>" data-solicitudr_cantidad="<?=$sol_reintegror->solicitudr_cantidad;?>" data-bodega="<?=$sol_reintegror->bodega_id_destino?>" data-tipo="2" data-patinador="<?=$sol_reintegror->patinador;?>">Recibido</button></td>
		                </tr>
		            	<?php
}
}
?>
		                    <input type="text" class="hidden" id="inp_solicitudr_id" name="inp_solicitudr_id">
		                    <input type="text" class="hidden" id="inp_material_id" name="inp_material_id">
		                    <input type="text" class="hidden" id="inp_mmaterial_descripcion" name="inp_mmaterial_descripcion">
		                    <input type="text" class="hidden" id="inp_solicitudr_cantidad" name="inp_solicitudr_cantidad">
		                    <input type="text" class="hidden" id="codigo" name="codigo">
		                    <input type="text" class="hidden" id="tipo" name="tipo">
		                    <input type="text" class="hidden" id="bodega" name="bodega">
		                    <input type="text" class="hidden" id="patinador" name="patinador">


		            </tbody>
		        </table>
		    </div>
	    </form>
	</div>

</div>
<div class="modal fade" id="modal-id">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title righteous">Presentaciones</h4>
      </div>
      <div class="modal-body oswald">
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table table-hover">

            <thead>
              <tr>
                <th>Presentacion</th>
                <th>Cantidad</th>
                <th>Codigo de Barras</th>
              </tr>
            </thead>
            <tbody id="detalles-reintegros">

            </tbody>
          </table>
                  <table  class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <td>#</td>
              <th>Bandeja</th>
              <th>Item</th>
              <th>Descripción</th>
              <th>Bodega</th>
              <th>Cantidad</th>
            </tr>
          </thead>
          <tbody class="td_detail_und_detail">
            <tr><td align="center" colspan="6">Datos no disponibles para tipo NORMAL</td> </tr>

          </tbody>
        </table>
        </div>
      </div>
    </div>

      </div>
      <div class="modal-footer oswald">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

      </div>
    </div>
  </div>
</div>

<script >
 function recibir_reintegro(obj) {
		var res=confirm("¿Esta seguro de Realizar el traslado?");
		if (res) {
			$('#form_recibir_reintegro').attr({
				action: 'traslado_prod',
				method: 'POST'
			});
			var soliditudr_id=$(obj).data('soliditudr_id');
			var material_id=$(obj).data('material_id');
			var material_descripcion=$(obj).data('material_descripcion');
			var solicitudr_cantidad=$(obj).data('solicitudr_cantidad');
			var codigo=$(obj).data('codigo');
			var tipo = $(obj).data('tipo');
			var bodega = $(obj).data('bodega');
			var patinador = $(obj).data('patinador');
			$('#inp_solicitudr_id').val(soliditudr_id);
			$('#inp_material_id').val(material_id);
			$('#inp_mmaterial_descripcion').val(material_descripcion);
			$('#inp_solicitudr_cantidad').val(solicitudr_cantidad);
			$("#patinador").val(patinador);
			$('#codigo').val(codigo);
			$("#tipo").val(tipo);
			$("#bodega").val(bodega);
			$('#form_recibir_reintegro').submit();

		}else{

		}
	}
</script>