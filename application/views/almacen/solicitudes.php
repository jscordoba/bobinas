<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="wrapper">
    <div id="page-wrapper">

    	<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Solicitudes</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>


		<div class="tab-pane fade in active">
		    <div class="table-responsive">
		        <table class="table table-striped table-bordered table-hover">
		            <thead>
		                <tr>
		                    <th>Item</th>
		                    <th>Descripción</th>
		                    <th>Cantidad Disponible Kgs</th>
		                    <th>Cantidad Entregada  Kgs</th>
		                </tr>
		            </thead>
		            <tbody>
		                <tr>
		                    <td>1</td>
		                    <td>Mark</td>
		                    <td>Otto</td>
		                    <td>@mdo</td>
		                </tr>
		                <tr>
		                    <td>2</td>
		                    <td>Jacob</td>
		                    <td>Thornton</td>
		                    <td>@fat</td>
		                </tr>
		                <tr>
		                    <td>3</td>
		                    <td>Larry</td>
		                    <td>the Bird</td>
		                    <td>@twitter</td>
		                </tr>
		            </tbody>
		        </table>
			</div>
		</div>
	</div>
</div>