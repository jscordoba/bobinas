<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header righteous">Stock de Materiales</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <!-- /.row  CASILLAS DE CONTADORES-->
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="righteous">Stock</h4>
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge"><?=$indicador_mat;?></div>
                                    <div>Materiales</div>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge"><?=$indicador_mov;?></div>
                                    <div>Movimientos</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=base_url();?>c_almacen/stock_detail">
                            <div class="panel-footer oswald">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>


                <!-- <a href="" data-toggle="modal" data-target="#cargar_stock"> -->
                    <div class="col-lg-6 col-md-6 ">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <h4 class="righteous">Sincronizar</h4>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-8 text-right oswald">
                                        <div class="huge"><?=$indicador_date;?></div>
                                        <div>Última Carga</div>
                                    <div class="huge"><?=$indicador_sinc;?></div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?=base_url();?>c_almacen/Sincronizar">
                                <div class="panel-footer oswald">
                                    <span class="pull-left">Cargar</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>

                        </div>
                    </div>
                <!-- </a> -->

                      <div class="col-lg-12 col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="righteous">Importar Archivos Planos Material tipo Bandeja</h4>
                            <div class="row">
                                <div class="col-xs-2">
                                    <i class="fa fa-floppy-o fa-5x"></i>
                                </div>

                            </div>
                        </div>
                        <a href="<?=base_url();?>c_almacen/import_stock">
                            <div class="panel-footer">
                                <span class="pull-left oswald">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

        </div>

            <!-- /.row -->

    </div>

    <div id="morris-area-chart" style="display:none;"></div>
    <div id="morris-bar-chart" style="display:none;"></div>
    <div id="morris-donut-chart" style="display:none;"></div>
    <!-- /#wrapper -->
