<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div id="wrapper">
    <div id="page-wrapper">

      <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header righteous">Stock de Materiales</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="col-md-12">
          <button class="btn btn-success btn-sm oswald" id="btn-new-entry" data-toggle="modal" data-target="#new-entry">Nueva Entrada</button>
        </div>

    <hr>
    <div class="tab-pane fade in active col-md-12">
        <div class="dataTable_wrapper oswald">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Descripción</th>
                        <th>Cantidad Disponible </th>
                    </tr>
                </thead>
                <tbody>
                  <?php foreach ($stock->result() as $stockr): ?>
                    <tr>
                        <td><?=$stockr->material_id;?></td>
                        <td><?=$stockr->material_descripcion;?></td>
                        <td><?=$stockr->stockm_cantidad;?> </td>
                    </tr>
                <?php endforeach?>
                </tbody>
            </table>
      </div>
    </div>
  </div>
</div>



<!-- Modal detalle de Unidades-->
<div class="modal fade" id="new-entry" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title righteous" id="myModalLabel"><!-- Form Name -->
      Ingresar nueva entrada de Material
        </h4>
      </div>
      <div class="modal-body oswald">
        <form class="form-horizontal" action="n_entrada">
            <fieldset>

            <div class="form-group">
              <div class="btn-group col-xs-12 col-md-12 col-sm-12" data-toggle="buttons">
              <label for="year" class="control-label">Item *</label>
                <input id="stock_material_id" name="stock_material_id" type="stock_material_id" placeholder="Id/# Item" class="form-control input-md" required>
              </div>
            </div>

            <div class="form-group">
              <div class="btn-group col-xs-12 col-md-12 col-sm-12" data-toggle="buttons">
                 <label for="year" class="control-label">Descripción</label>
                <input id="stock_material_desc" name="stock_material_desc" type="stock_material_desc" placeholder="Descripción de Item" class="form-control input-md" required>
              </div>
            </div>

            <div class="form-group">
              <div class="btn-group col-xs-12 col-md-12 col-sm-12"
              data-toggle="buttons">
              <label for="year" class="control-label">Cantidad *</label>

                <input id="stock_material_cant" name="stock_material_cant" type="stock_material_cant" placeholder="Cantidad en Kgs" class="form-control input-md" required>
              </div>
            </div>

            </fieldset>


      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" id="reg-new-entry">Registrar</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Fin Modal -->