<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <!-- jQuery -->
    <script src="<?= base_url(); ?>styles/bower_components/jquery/dist/jquery.min.js"></script>
    
    <!-- SCRIPT FUNCIONES PROPIAS -->
    <script src="<?= base_url(); ?>styles/js/functions.js"></script>


    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url(); ?>styles/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= base_url(); ?>styles/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?= base_url(); ?>styles/bower_components/raphael/raphael-min.js"></script>
    <script src="<?= base_url(); ?>styles/bower_components/morrisjs/morris.min.js"></script>
    <script src="<?= base_url(); ?>styles/js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url(); ?>styles/dist/js/sb-admin-2.js"></script>


    <!-- DataTables JavaScript -->
    <script src="<?= base_url(); ?>styles/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>styles/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
     <script src="<?= base_url(); ?>styles/bower_components/datatables-plugins/print/print_function.js"></script>
     <script src="<?= base_url(); ?>styles/bower_components/datatables-plugins/print/printButton.js"></script>



    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                dom: 'Bfrtip',
                destroy : true,
       
         "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
        });

        $('#dataTables-SolReit').DataTable({
                responsive: true,
                dom: 'Bfrtip',
                destroy : true,
       
         "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
        });

        $('#dataTables-SolPend').DataTable({
                responsive: true,
                dom: 'Bfrtip',
                destroy : true,
       
         "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
        });

        $('div.dataTables_filter input').focus();


      
    });
    </script>

</body>

</html>
