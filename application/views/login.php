<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default shadow-login">
                <div class="panel-heading">
                    <h3 class="panel-title righteous">Control de Materiales - Bobinas</h3>
                </div>
                <div class="panel-body oswald">
                    <form role="form" action="<?=base_url();?>main/login" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Usuario" name="usuario" type="usuario" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                            <!-- <div class="checkbox">
                                <label>
                                    <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                </label>
                            </div> -->
                            <!-- Change this to a button or input when using this as a form -->
                            <button class="btn btn-success btn-block">Ingresar</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>