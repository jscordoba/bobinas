<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header righteous">Listado de Codigos RR</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<div class="row oswald">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                Ajustar Codigos y cantidades
		                <a data-toggle="modal" data-target="#modal-id" class="pull-right">Añadir</a>
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body">
		                <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                        <thead>
		                            <tr>
		                            	<th>#</th>
		                                <th>Material</th>
		                                <th>Presentacion</th>
		                                <th>Cantidad</th>
		                                <th>Codigo de Barras</th>
		                                <th colspan="2">Acción</th>
		                                <th>Imprimir</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	<?php 
	                            $i=1;
				            	foreach ($list_codigos->result() as $item): 
				            		?>
					                <tr>
					                    <td><?= $i++; ?></td>
					                    <td><?= $item->material_id.' - '.$item->material_descripcion; ?></td>
					                    <td><?= $item->presentacion; ?></td>
					                    <td contenteditable class="cantidad"><?= $item->sc_cantidad; ?></td>
					                    <td><?= $item->codbar; ?></td>
					                    <td><button class="btn btn-primary edit_mat btn-xs" data-id="<?=$item->id; ?>">Editar</button></td>
					                    <td><button class="btn btn-danger delete_mat btn-xs" data-id="<?=$item->id; ?>">Eliminar</button></td>

					                    <td><a target="_blank" href="<?= base_url(); ?>ajax/codbar_pdf?codbar=<?= $item->codbar ?>&veces=<?=  1; ?>" class="btn btn-edefault print_mat btn-xs" data-id="<?=$item->codbar; ?>">Imprimir</a></td>
					                </tr>
							<?php
					             endforeach ?>
		                       
		                        </tbody>
		                    </table>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>


<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title righteous">Nuevo Codigo RR</h4>
			</div>
			<form class="" action="javascript:reg_codigo()" id="form_add_codigo">
			<div class="modal-body oswald">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group ">
							<label for="material">Material:</label>
							<select name="material" id="material" class="form-control" required>
								<option value="" selected disabled>Elegir material</option>
								<?php 
											$i=1;
											foreach ($list_materiales->result() as $item): 
												
												?>
												<option value="<?= $item->stockm_id; ?>"><?= $item->material_id." - ".$item->material_descripcion; ?></option>
										<?php
										
											endforeach ?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group ">
							<label for="presentacion">Presentacion:</label>
							<select name="presentacion" id="presentacion" class="form-control" required>
								<option value="" selected disabled>Elegir presentacion</option>
								<option value="Estiba">Estiba</option>
								<option value="Chipa">Chipa</option>
								<option value="Rollo">Rollo</option>
								<option value="Carreto">Carreto</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					<label for="cantidad">Cantidad:</label>
					<input type="number" class="form-control" name="cantidad" id="cantidad" required>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label for="codigo">Codigo:</label>
					<input type="number" name="codigo" class="form-control" id="codigo" required>
				</div>
					</div>
				</div>
		</div>
	
			<div class="modal-footer oswald">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			</form>
		</div>
	</div>
</div>