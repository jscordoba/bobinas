<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header righteous">Inicio</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <!-- /.row  CASILLAS DE CONTADORES-->
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="righteous">Solicitudes</h4>
                            <div class="row">
                                <!-- <div class="col-xs-2">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div> -->
                                <div class="col-xs-6 text-center oswald">
                                    <div class="huge"><?=$indicador_pdt;?></div>
                                    <div>Pendientes</div>
                                </div>
                                <div class="col-xs-6 text-center oswald">
                                    <div class="huge"><?=$indicador_asig;?></div>
                                    <div>Respondidas</div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer oswald">
                            <a href="<?=base_url();?>c_material/stock_solicitudes"><span class="pull-left">Ver Detalles <i class="fa fa-arrow-circle-right"></i></span></a>
                            <a href="#"><span class="pull-right" data-toggle="modal" data-target="#nueva_solicitud">Nueva Solicitud <i class="fa fa-arrow-circle-right"></i></span></a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <h4 class="righteous">Solicitud Materiales</h4>
                            <div class="row">
                                <!-- <div class="col-xs-2">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div> -->
                                <div class="col-xs-6 text-center oswald">
                                    <div class="huge"><?=$indicador_sol_mat;?></div>
                                    <div>Enviadas</div>
                                </div>
                                <div class="col-xs-6 text-center oswald">
                                    <div class="huge"><?=$indicador_sol_resp;?></div>
                                    <div>Respondidas</div>
                                </div>
                            </div>
                        </div>

                        <a href="<?=base_url();?>c_material/sol_pendientes">
                            <div class="panel-footer oswald">
                                <span class="pull-left">Ver Detalles <i class="fa fa-arrow-circle-right"></i></span></a>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <h4 class="righteous">Reintegros</h4>
                            <div class="row">
                                <!-- <div class="col-xs-2">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div> -->
                                <div class="col-xs-6 text-center oswald">
                                    <div class="huge"><?=$indicador_maq_reint;?></div>
                                    <div>Maquinas</div>
                                </div>
                                <div class="col-xs-6 text-center oswald">
                                    <div class="huge"><?=$indicador_prod_reint;?></div>
                                    <div>Producción</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=base_url();?>c_material/list_reintegros">
                            <div class="panel-footer oswald">
                                <a href="<?=base_url();?>c_material/list_reintegros_ppal"><span class="pull-left">Ver Detalles <i class="fa fa-arrow-circle-right"></i></span></a>
                                <a href="<?=base_url();?>c_material/new_reintegro"><span class="pull-right">Nuevo Reintegro <i class="fa fa-arrow-circle-right"></i></span></a>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <h4 class="righteous">Labores</h4>
                            <div class="row">
                                <!-- <div class="col-xs-2">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div> -->
                                <div class="col-xs-6 text-center oswald">
                                    <div class="huge"><?=$indicador_ot_act;?></div>
                                    <div>Activas</div>
                                </div>
                                <div class="col-xs-6 text-center oswald">
                                    <div class="huge"><?=$indicador_ot_fin;?></div>
                                    <div>Finalizadas</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=base_url();?>c_material/ordenes_trabajo">
                            <div class="panel-footer oswald">
                                <span class="pull-left">Ver Detalles <i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>



            <!-- Modal Nueva solicitud-->
            <div class="modal fade" id="nueva_solicitud" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title righteous" id="myModalLabel">
                        Solicitud de Material
                    </h4>
                  </div>
                <div class="modal-body">
            <div class="panel-body">
                <div role="tabpanel">
                    <ul class="nav nav-tabs nav-justified nav-pills" role="tablist">
                        <li role="presentation" class="active righteous">
                            <a href="#ei" aria-controls="home" role="tab" data-toggle="tab" title="tipo_normal">Tipo Normal</a>
                        </li>
                        <li role="presentation" class="righteous">
                            <a href="#af" aria-controls="tab" role="tab" data-toggle="tab" title="tipo_bandeja">Tipo Bandeja</a>
                        </li>
                        <li role="presentation" class="righteous">
                            <a href="#mav" aria-controls="tab" role="tab" data-toggle="tab" title="">Tipo Masivo</a>
                        </li>
                    </ul>
                </div>
            <div class="tab-content oswald">
                <div role="tabpanel" class="tab-pane active" id="ei">
                    <div class="panel-body">
                        <div class="row">
                            <form class="form-horizontal" id="form_sol_material">
                                <fieldset>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="control-label" for="textinput ">Material / Item</label>
                                            <input id="material_id" name="material_id" type="number" placeholder="Número de Item o Nombre de Item" class="form-control" required>
                                            <!-- <span class="help-block">*Número de Item o Nombre de Item.</span>   -->
                                            </div>
                                            <div class="col-md-6">
                                            <label class="control-label" for="textinput ">Descripción</label> <div class="form-control" id="material_descripcion">
                                                    Undefined
                                                </div>
                                                <input type="hidden" id="input_material_descripcion" name="input_material_descripcion">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label" for="textinput">Cantidad</label>
                                            <input id="material_cantidad" name="material_cantidad" type="number" placeholder="Cantidad en Kgs" class="form-control" required>
                                            <!-- <span class="help-block">*Cantidad en Kgs</span>  -->
                                        </div>
                                        <div class="col-md-6">
                                            <label class="ontrol-label" for="textinput">Observación</label>
                                            <input id="observacion" name="observacion" type="text" placeholder="Digitar Observaciones" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-primary btn-block" id="btn_sol_material">Solcitar</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane " id="af">
                    <div class="panel-body">
                        <div class="row">
                        <form class="form-horizontal" id="form_sol_material2">
                        <fieldset>
                        <div class="form-group">
                              <div class="col-md-6">
                                <label class="control-label" for="textinput ">OT / Item</label>
                                  <input id="material_id2" name="material_id2" type="number" placeholder="Número de OT o Nombre de OT" class="form-control input-md" required>
                              </div>
                              <div class="col-md-6">
                              <label class="control-label" for="textinput ">Descripción</label>
                                <div class="form-control" id="material_descripcion2">
                                    Undefined
                                </div>
                                <input type="hidden" id="input_material_descripcion2" name="input_material_descripcion2">

                            </div>
                          <div class="col-md-6">
                          <label class="control-label" for="textinput">Cantidad</label>
                              <input id="material_cantidad2" name="material_cantidad2" type="number" placeholder="Cantidad en Uds" class="form-control input-md" required>
                          </div>
                          <div class="col-md-6">
                            <label class="control-label" for="textinput">Observación</label>
                              <input id="observacion2" name="observacion2" type="text" placeholder="Digitar Observaciones" class="form-control input-md" required>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-block" id="btn_sol_material2" >Solcitar</button>
                            </div>
                        </div>


                        </fieldset>
                    </form>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane " id="mav">
                    <div class="panel-body">
                       <form action="<?php echo base_url('c_material/import_file_material_maxivo'); ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="file" name="file" class="filecsvmaxivo" id="file">
                                </div><br><br>
                                <div class="col-md-6 col-sm-6">
                                    <button type="submit" class="btn btn-primary btn-block" id="btn_sol_material2">Solcitar</button>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <a href="<?=base_url('solicitudes/Manual plantilla.docx')?>" class="btn btn-primary btn-block">Descargar plantilla</a>
                                </div>
                            </div>
                       </form>
                    </div>
                </div>
            </div>


                </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
                </div>
              </div>
            </div>
            </div>

            <!-- Fin Modal -->



            <!-- TABLA -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading righteous">
                            Últimos Movimientos
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body oswald">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Descripción</th>
                                        <th>Origen</th>
                                        <th>Destino</th>
                                        <th>Cantidad</th>
                                        <th>Fecha Movimiento</th>
                                        <th>tipo</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
$i = 1;
foreach ($movimientos->result() as $movimientor):
    if ($movimientor->tipo == 1) {
        ?>
                                                                                                                                                        <tr>
                                                                                                                                                            <td><?=$i++;?></td>
                                                                                                                                                            <td><?=$movimientor->material_id;?></td>
                                                                                                                                                            <td><?=$movimientor->material_descripcion;?></td>
                                                                                                                                                            <td><?=$movimientor->bodega_origen;?></td>
                                                                                                                                                            <td><?=$movimientor->bodega_destino;?></td>
                                                                                                                                                            <td><?=$movimientor->movimiento_cantidad;?> Kgs</td>
                                                                                                                                                            <td><?=$movimientor->movimiento_date;?></td>
                                                                                                                                                            <td style="background-color: #1565c0; color: white;">NORMAL</td>

                                                                                                                                                        </tr>
                                                                                                                                                    <?php
    } else {
        ?>
                                                                                                                                                            <tr>
                                                                                                                                                            <td><?=$i++;?></td>
                                                                                                                                                            <td><?=$movimientor->material_id;?></td>
                                                                                                                                                            <td><?=$movimientor->material_descripcion;?></td>
                                                                                                                                                            <td><?=$movimientor->bodega_origen;?></td>
                                                                                                                                                            <td><?=$movimientor->bodega_destino;?></td>
                                                                                                                                                            <td><?=$movimientor->movimiento_cantidad;?> Uds</td>
                                                                                                                                                            <td><?=$movimientor->movimiento_date;?></td>
                                                                                                                                                            <td style="background-color: #EF6C00; color: white;">BANDEJA</td>

                                                                                                                                                        </tr>
                                                                                                                                                                <?php
    }
endforeach?>
                                </tbody>
                            </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>
            <!-- /.row -->

    </div>


    <!-- /#wrapper -->
