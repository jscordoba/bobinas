<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header righteous">Reintegros desde las Máquinas</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Reintegros
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                        <thead>
		                            <tr>
		                                <th>#</th>
		                                <th>Item</th>
		                                <th>Descripción</th>
		                                <th>Cantidad</th>
		                                <th>Máquina Origen</th>
		                                <th>Fecha</th>
		                                <th>Tipo</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php 
		                            $i=1;
					            	foreach ($reintegros_mq->result() as $reintegros_mqr){ 
					            		if ($reintegros_mqr->tipo==1) {
					            		
					            		?>
						                <tr>
						                    <td><?= $i++; ?></td>
						                    <td><?= $reintegros_mqr->material_id; ?></td>
						                    <td><?= $reintegros_mqr->material_descripcion; ?></td>
						                    <td><?= $reintegros_mqr->movimiento_cantidad; ?> Kgs</td>
						                    <td><?= $reintegros_mqr->bodega_nombre; ?></td>
						                    <td><?= $reintegros_mqr->movimiento_date; ?></td>
						                    <td style="background-color: #1565c0; color: white;">NORMAL</td>
						                    

						                </tr>
						            <?php }else{
						            	?>
						                 <tr>
						                    <td><?= $i++; ?></td>
						                    <td><?= $reintegros_mqr->material_id; ?></td>
						                    <td><?= $reintegros_mqr->material_descripcion; ?></td>
						                    <td><?= $reintegros_mqr->movimiento_cantidad; ?> Uds</td>
						                    <td><?= $reintegros_mqr->bodega_nombre; ?></td>
						                    <td><?= $reintegros_mqr->movimiento_date; ?></td>
						                    <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
						                </tr>
						            	<?php
						            }
						        }

						             ?>
		                        </tbody>
		                    </table>
		                </div>
		                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
		</div>
	</div>
</div>