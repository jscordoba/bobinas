<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header righteous">Reintegros desde Materiales</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Reintegros
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="dataTable_wrapper">
                         <table class="table table-striped table-bordered table-hover" id="dataTables-example">
				            <thead>
				                <tr>
				                    <th>Item</th>
				                    <th>Descripción</th>
				                    <th>Cantidad Kgs</th>
				                    <th>Fecha Reintegro</th>
				                    <th>Tipo</th>
				                </tr>
				            </thead>
				            <tbody>
				                <?php foreach ($reintegros_ppal->result() as $reintegros_ppalr){ 
				                	if ($reintegros_ppalr->tipo==1) {
				                	?>
				                <tr>
				                    <td><?= $reintegros_ppalr->material_id; ?></td>
				                    <td><?= $reintegros_ppalr->material_descripcion; ?></td>
				                    <td><?= $reintegros_ppalr->movimiento_cantidad; ?> Kgs</td>
				                    <td><?= $reintegros_ppalr->movimiento_date; ?></td>
						            <td style="background-color: #1565c0; color: white;">NORMAL</td>
				                </tr>
				            <?php }else{
				            	?>
				            	<tr>
				                    <td><?= $reintegros_ppalr->material_id; ?></td>
				                    <td><?= $reintegros_ppalr->material_descripcion; ?></td>
				                    <td><?= $reintegros_ppalr->movimiento_cantidad; ?> Uds</td>
				                    <td><?= $reintegros_ppalr->movimiento_date; ?></td>
						            <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
				                </tr>
				            	<?php
				            }
				            }
				            ?>
				            </tbody>
				        </table>
		                </div>
		                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
		</div>
	</div>
</div>