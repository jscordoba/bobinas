<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand righteous" href="<?=base_url();?>">Control Bobinas</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

            <li class="dropdown">
                <a class="dropdown-toggle righteous" data-toggle="dropdown" href="#">
                    <?=$this->session->userdata('tipo_perfil');?>
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user righteous">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?=base_url();?>main/logout"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="navbar-default sidebar oswald" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                        </div>
                    </li>
                    <li>
                        <a href="<?=base_url();?>c_material/"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Solicitudes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?=base_url();?>c_material/sol_pendientes">Pendientes</a>
                            </li>

                            <li>
                                <a href="<?=base_url();?>c_material/stock_solicitudes">Solicitudes y Stock</a>
                            </li>
                        </ul>
                    </li>
                                        <li>
                        <a href="<?=base_url();?>c_material/estaciones"><i class="fa fa-edit fa-fw"></i> Recursos</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i>Reintegros<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?=base_url();?>c_material/new_reintegro">Reintegrar a Almacén</a>
                            </li>
                            <li>
                                <a href="<?=base_url();?>c_material/list_reintegros_ppal">Reintegros de Materiales</a>
                            </li>
                            <li>
                                <a href="<?=base_url();?>c_material/list_reintegros_mq">Reintegros de Maquinas</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="<?=base_url();?>c_material/ordenes_trabajo"><i class="fa fa-sitemap fa-fw"></i>Labores
                        </span></a>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-edit fa-fw"></i> Reportes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?=base_url();?>c_material/reporte">Almacen/Patinador</a>
                            </li>
                            <li>
                                <a href="<?=base_url();?>c_material/reporte2">Patinador/Maquina</a>
                            </li>
                            <li>
                                <a href="<?=base_url();?>c_material/reporte3">Maquina/Patinador</a>
                            </li>
                             <li>
                                <a href="<?=base_url();?>c_material/reporte4">Patinador/Almacen</a>
                            </li>
                        </ul>
                    </li>
                  <!--   <li>
                        <a href="<?=base_url();?>c_material/codigos"><i class="fa fa-edit fa-fw"></i> Presentaciones y Codigo<span class="fa arrow"></span></a>

                    </li> -->
                </ul>
            </div>
        </div>
    </nav>