<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

       <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header righteous">Reintegrar a Almacén</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>


    <div class="row">
        <!-- /.col-lg-6 -->
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading righteous">
                    Stock
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body oswald">
                    <div class="dataTable_wrapper">
                      <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Descripción</th>
                                <th>Disponible</th>
                                <th>Tipo</th>
                                <th>Detalles</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $i=1;
                          foreach ($stock_reintegro->result() as $stockr){ 
                            if ($stockr->tipo==1) {
                            ?>
                          
                                <td><?= $i++; ?></td>
                                <td><?= $stockr->material_id; ?></td>
                                <td><?= $stockr->material_descripcion; ?></td>
                                <td><?= $stockr->stockm_cantidad; ?> Kgs</td>
                                <td style="background-color: #1565c0; color: white;">NORMAL</td>
                                
                                <td> <button class="btn btn-sm btn-default" id="btn-detalles-reintegros" data-movimiento = "<?= $stockr->movimiento_id; ?>" data-material = "<?= $stockr->material_id; ?>" data-descripcion = "<?= $stockr->material_descripcion; ?>" data-toggle="modal" data-target="#modal-id" data-bandeja="0">Detalles</button></td>
                                <!-- <?= $option; ?> -->
                                <td>No Disponible</td>
                            </tr>
                          <?php }else{
                            ?>
                           <?php if ($stockr->solicitudr_estado=='PENDIENTE'){ ?>
                                <tr class="bg-warning">
                          <?php $option='<td><button class="btn btn-sm  btn-info">En Espera...</button></td>';
                                }else{ ?>
                                <tr>
                          <?php $option='<td><button data-target="#sol_reintegro_modal" data-toggle="modal" data-stock_rein_cant="'.$stockr->stockm_cantidad.'" data-mat_rein_id="'.$stockr->material_id.'" data-movimiento="'.$stockr->movimiento_id.'" data-mat_rein_desc="'.$stockr->material_descripcion.'" data-tipo="2" class="reintegrar_mat_modal btn btn-sm btn-success">Reintegro</button></td>';
                                } ?>
                                <td><?= $i++; ?></td>
                                <td><?= $stockr->material_id; ?></td>
                                <td><?= $stockr->material_descripcion; ?></td>
                                <td><?= $stockr->stockm_cantidad; ?> Uds</td>
                              
                                <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
                                
                                <td> <button class="btn btn-sm btn-default" id="btn-detalles-reintegros" data-movimiento = "<?= $stockr->movimiento_id; ?>"  data-toggle="modal" data-target="#modal-id" data-bandeja="<?= $stockr->material_id; ?>">Detalles</button></td>
                                <?= $option; ?>
                            </tr>
                            <?php
                          }

                          } ?>
                        </tbody>
                      </table>
                    </div> 
                    <!-- /.table-responsive -->

                    <div class="modal fade" id="modal-id">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title righteous">Presentaciones</h4>
                          </div>
                            <form class="form-horizontal" id="form_rein_mat_normal" action="reintegrar" method="POST">
                          <div class="modal-body oswald">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="table-responsive">
                              <table class="table table-hover table-bordered table-striped">
                                
                                <thead>
                                  <tr>
                                    <th>Sel</th>
                                    <th>Presentacion</th>
                                    <th>Cantidad</th>
                                    <th>Codigo de Barras</th>
                                  </tr>
                                </thead>
                                <tbody id="detalles-reintegros">
                                
                                </tbody>

                              </table>
                              <textarea name="observacion_ralm" placeholder="Describa la razon del reintegro"  cols="30" rows="5" class="form-control observacion_ralm" ></textarea>
                              <br>
                              <input type="hidden" name="material_rein_normal" id="material_rein_normal">
                              <input type="hidden" name="descripcion_rein_normal" id="descripcion_rein_normal">
                              <input type="hidden" name="movimiento_rein_normal" id="movimiento_rein_normal">
                                      <table  class="table table-striped table-bordered table-hover">
                              <thead>
                                <tr>
                                  <td>#</td>
                                  <th>Bandeja</th>
                                  <th>Item</th>
                                  <th>Descripción</th>
                                  <th>Bodega</th>
                                  <th>Cantidad</th>
                                </tr>
                              </thead>
                              <tbody class="td_detail_und_detail">
                                <tr><td align="center" colspan="6">Datos no disponibles para tipo NORMAL</td> </tr>
                                
                              </tbody>
                            </table>
                            </div>
                          </div>
                        </div>
                            
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-success" id="btn_rein_alm_normal" >Solicitar Reintegro</button> 

                          
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>

                   <!-- Modal Nueva solicitud-->
                    <div class="modal fade" id="sol_reintegro_modal" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title righteous" id="myModalLabel"><!-- Form Name -->
                                Reintegro de Material <b><span id="head_material_r"></span></b>
                            </h4>
                          </div>
                          <div class="modal-body oswald">
                            <form class="form-horizontal" id="form_rein_alm">
                                <fieldset>

                                  <div class="form-group">
                                      
                                    <div class="col-md-12">
                                      <label class="control-label" for="textinput">Material en Stock</label>
                                      <span class="form-control col-md-12" id="text_mat_disp_alm"></span>
                                      <!-- <span class="help-block">*Cantidad a Reintegrar</span> -->
                                      <input type="hidden" id="input_matid_rein_alm" name="input_matid_rein_alm">
                                      <input type="hidden" id="input_cantmat_rein_alm" name="input_cantmat_rein_alm">
                                    </div>
                                  </div>
                                  
                                  <input type="hidden"  id="input_cant_rein_alm" name="cantidad_rein_alm" placeholder="# Kgs" class="form-control input-md">
                                  <input type="hidden" id="movimiento" name="movimiento">
                                  <input type="hidden" id="codigo" name="codigo">
                                  <input type="hidden" id="tipo" name="tipo">


                                  <div class="form-group">
                                    
                                    <div class="btn-group col-md-12" data-toggle="buttons">
                                      <label for="year" class="control-label">Observaciones</label>
                                      <textarea name="observacion_ralm" placeholder="Describa la razon del reintegro" id="obs_rein_alm" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                  </div>

                                </fieldset>
                            </form>

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <!-- generar codigo de barras y mostrar para impresion, al dar clic en asignar -->
                            <button type="button" class="btn btn-primary" id="btn_rein_alm">Solicitar Reintegro</button> 
                          </div>
                        </div>
                      </div>
                    </div>
                  <!-- Fin Modal -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->
  </div>
</div>
