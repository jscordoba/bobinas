<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

    	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header righteous">Labores</h1>
        </div>
        <!-- /.col-lg-12 -->
      </div>

		<div class="row">
	    <div class="col-lg-12">
        <div class="panel panel-default">
          
          <!-- /.panel-heading -->
          <div class="panel-body">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs righteous">
                  <li class="active"><a href="#activas" data-toggle="tab">Activas</a>
                  </li>
                  <li><a href="#finalizadas" data-toggle="tab">Finalizadas</a>
                  </li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content oswald">
                  <div class="tab-pane fade in active" id="activas">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                          Labores Activas
                      </div>
                      <div class="panel-body">
                        <div class="dataTable_wrapper">
                        <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="dataTables-example2">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item</th>
                                    <th>Descripción</th>
                                    <th>Cantidad Asignada</th>
                                    <th>Máquina</th>
                                    <th>Fecha Asignación</th>
                                    <th>Estado</th>
                                    <th>Tipo</th>
                                    <th>Detalles</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $i=1;
                                foreach ($ordenes_activas->result() as $ordenes_activar){ 
                                  if ($ordenes_activar->tipo==1) {
                                  ?>
                                    <tr>
                                        <td><?= $i++; ?></td>
                                        <td><?= $ordenes_activar->material_id; ?></td>
                                        <td><?= $ordenes_activar->material_descripcion; ?></td>
                                        <td><?= $ordenes_activar->maquinao_cantidad; ?> Kgs</td>
                                        <td><?= $ordenes_activar->maquina_nombre; ?></td>
                                        <td><?= $ordenes_activar->movimiento_date; ?></td>
                                        <td class="bg-success"><?= $ordenes_activar->maquinao_estado; ?></td>
                                        <td style="background-color: #1565c0; color: white;">NORMAL</td>
                                        <td class="text-center">
                                          <a data-toggle="modal" data-target="#datail_und" data-material_id="<?= $ordenes_activar->material_id; ?>" data-maquina_bod_id="<?= $ordenes_activar->maquina_bod_id; ?>" data-movimiento_id="<?= $ordenes_activar->movimiento_id; ?>" class=" btn_detail_unds">Ver</a>
                                        </td>
                                    </tr>
                                <?php }else{
                                  ?>
                                        <tr>
                                        <td><?= $i++; ?></td>
                                        <td><?= $ordenes_activar->material_id; ?></td>
                                        <td><?= $ordenes_activar->material_descripcion; ?></td>
                                        <td><?= $ordenes_activar->maquinao_cantidad; ?> Uds</td>
                                        <td><?= $ordenes_activar->maquina_nombre; ?></td>
                                        <td><?= $ordenes_activar->movimiento_date; ?></td>
                                        <td class="bg-success"><?= $ordenes_activar->maquinao_estado; ?></td>
                                        <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
                                        <td class="text-center">
                                          <a data-toggle="modal" data-target="#datail_und" data-material_id="<?= $ordenes_activar->material_id; ?>" data-maquina_bod_id="<?= $ordenes_activar->maquina_bod_id; ?>" data-movimiento_id="<?= $ordenes_activar->movimiento_id; ?>" data-bandeja="<?= $ordenes_activar->material_id; ?>" class="btn_detail_unds">Ver</a>
                                        </td>
                                    </tr>
                                  <?php
                                }
                              }
                                 ?>
                            </tbody>
                          </table>
                          </div>
                        </div>
                      </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                  </div><!-- /.panel-fade -->
                  <div class="clearfix"></div>
                  <div class="tab-pane fade" id="finalizadas">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                          Labores Finalizadas
                      </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                              <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item</th>
                                    <th>Descripción</th>
                                    <th>Cantidad Asignada</th>
                                    <th>Cantidad Reintegrado</th>
                                    <th>Máquina</th>
                                    <th>Máquina Destino</th>
                                    <th>Fecha Finalización</th>
                                    <th>Tipo</th>
                                    <th>Estado</th>
                                </tr>
                              </thead>
                            <tbody>
                              <?php 
                              $i=1;
                              foreach ($ordenes_finalizadas->result() as $ordenes_finalizadar){
                                $bodega = $ordenes_finalizadar->bodega_id_destino;
                                $sql = $this->db->query("SELECT bodega_nombre from bodega where bodega_id = '$bodega'");
                               
                                foreach ($sql->result() as $dato ) {
                                  $destino = $dato->bodega_nombre;
                                  break;
                                }
                                if ($ordenes_finalizadar->tipo==1) {
                                ?>
                                  <tr>
                                      <td><?= $i++; ?></td>
                                      <td><?= $ordenes_finalizadar->material_id; ?></td>
                                      <td><?= $ordenes_finalizadar->material_descripcion; ?></td>
                                      <td><?= $ordenes_finalizadar->maquinao_cantidad; ?> Kgs</td>
                                      <td class="bg-warning"><?= $ordenes_finalizadar->movimiento_cantidad; ?> Kgs</td>
                                      <td><?= $ordenes_finalizadar->maquina_nombre; ?></td>
                                      <td><?= $destino ?></td>
                                      <td><?= $ordenes_finalizadar->movimiento_date; ?></td>
                                      <td style="background-color: #1565c0; color: white;">NORMAL</td>
                                      

                                      <td  class="bg-danger"><?= $ordenes_finalizadar->maquinao_estado; ?></td>
                                  </tr>
                              <?php 
                            }else{
                              ?>
                              <tr>
                                      <td><?= $i++; ?></td>
                                      <td><?= $ordenes_finalizadar->material_id; ?></td>
                                      <td><?= $ordenes_finalizadar->material_descripcion; ?></td>
                                      <td><?= $ordenes_finalizadar->maquinao_cantidad; ?> Uds</td>
                                      <td class="bg-warning"><?= $ordenes_finalizadar->movimiento_cantidad; ?> Uds</td>
                                      <td><?= $ordenes_finalizadar->maquina_nombre; ?></td>
                                      <td><?= $destino ?></td>
                                      <td><?= $ordenes_finalizadar->movimiento_date; ?></td>
                                      <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
                                      <td  class="bg-danger"><?= $ordenes_finalizadar->maquinao_estado; ?></td>
                                  </tr>
                              <?php
                            }
                          }
                               ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                        </div>
                    <!-- /.panel-body -->
                  </div>
              </div>

          </div>
          <!-- /.panel-body -->
        </div>
	    </div>
		    <!-- /.col-lg-6 -->
		</div>
		<!-- /.row -->
	</div>
</div>



<!-- Modal detalle de Unidades-->
<div class="modal fade" id="datail_und" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title righteous" id="myModalLabel"><!-- Form Name -->
            Detalle de Unidades en Stock
        </h4>
      </div>
      <div class="modal-body oswald">
        <form class="form-horizontal" id="">
            <fieldset>

            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>Unidades</th>
                      <th>Presentación</th>
                      <th>Cantidad</th>
                      <th>Subtotal</th>
                      <th>Patinador</th>
                      <th>Codigo de Barras</th>
                  </tr>
              </thead>
              <tbody class="tb_detail_und">

              </tbody>
          </table>

            </fieldset>
        </form>
          Detalle OT
        <table  class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <td>#</td>
              <th>Bandeja</th>
              <th>Item</th>
              <th>Descripción</th>
              <th>Bodega</th>
              <th>Cantidad</th>
            </tr>
          </thead>
          <tbody class="td_detail_und_detail">
            <tr><td align="center" colspan="6">Datos no disponibles para tipo NORMAL</td> </tr>
            
          </tbody>
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        <!-- <button type="button" class="btn btn-primary" id="btn_sol_material">Solcitar</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Fin Modal -->