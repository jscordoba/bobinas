<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
            <center>
                <h1 class="page-header righteous">Reporte Modulo entregas Patinador/Maquina</h1>
                </center>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="tab-pane fade in active oswald">
            <div class="dataTable_wrapper">
            
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Carreto</th>
                            <th>Item</th>
                            <th>Descripción</th>
                            <th>Cod. Operario Patinador</th>
                            <th>Cant. Conductor Entregado</th>
                            <th>Horario Entrega</th>
                            <th>Maquina Entregada</th>
                            <th>Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $c=1;
                    foreach ($reporte2->result() as $sol_entregador){ 
                        if ($sol_entregador->tipo==1) {
                        
                        ?>
                        <tr>
                            <td><?= $c++; ?></td>
                            <td><?=$sol_entregador->dm_codbar ?></td>
                            <td><?= $sol_entregador->material_id; ?></td>
                            <td><?= $sol_entregador->material_descripcion; ?></td>
                            <td><?= $sol_entregador->usuario_id ?></td>
                            <td><?= $sol_entregador->movimiento_cantidad ?> Kgs</td>
                            <td><?= $sol_entregador->movimiento_date; ?></td>
                            <td><?= $sol_entregador->bodega_nombre; ?></td>
                            <td style="background-color: #1565c0; color: white;">NORMAL</td>

                        </tr>
                    <?php }else{
                        ?>
                         <tr>
                            <td><?= $c++; ?></td>
                            <td><?=$sol_entregador->dm_codbar ?></td>
                            <td><?= $sol_entregador->material_id; ?></td>
                            <td><?= $sol_entregador->material_descripcion; ?></td>
                            <td><?= $sol_entregador->usuario_id ?></td>
                            <td><?= $sol_entregador->movimiento_cantidad ?> Uds</td>
                            <td><?= $sol_entregador->movimiento_date; ?></td>
                            <td><?= $sol_entregador->bodega_nombre; ?></td>
                            <td style="background-color: #EF6C00; color: white;">BANDEJA</td>

                        </tr>
                        <?php

                    }
                    } ?>
                    </tbody>
                </table>
                
            </div>
        </div>
       
       
    </div>
</div>