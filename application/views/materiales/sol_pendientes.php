<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header righteous">Solicitudes</h1>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Lista de Solicitudes
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                        <thead>
		                            <tr>
		                                <th>ID</th>
		                                <th>Item</th>
		                                <th>Descripción</th>
		                                <th>Cantidad Kgs</th>
		                                <th>Estado</th>
		                                <th>Fecha Solicitud</th>
		                                <th>Tipo</th>

		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php 
					            	$i=1;
					            	foreach ($stock_solicitudes->result() as $stock_solicituder):
					            		if ($stock_solicituder->tipo == 1) {
					            	 ?>
						                <tr>
						                    <td><?= $i++; ?></td>
						                    <td><?= $stock_solicituder->material_id; ?></td>
						                    <td><?= $stock_solicituder->material_descripcion; ?></td>
						                    <td><?= $stock_solicituder->solicitudm_cantidad; ?> Kgs</td>
						                    <td><?= $stock_solicituder->solicitudm_estado; ?></td>
						                    <td><?= $stock_solicituder->solicitudm_date; ?></td>
						                    <td style="background-color: #1565c0; color: white;">NORMAL</td>

						                </tr>
						            <?php
						        }else{
						        	?>
						                <tr>
						                    <td><?= $i++; ?></td>
						                    <td><?= $stock_solicituder->material_id; ?></td>
						                    <td><?= $stock_solicituder->material_descripcion; ?></td>
						                    <td><?= $stock_solicituder->solicitudm_cantidad; ?> uds</td>
						                    <td><?= $stock_solicituder->solicitudm_estado; ?></td>
						                    <td><?= $stock_solicituder->solicitudm_date; ?></td>
						                    <td style="background-color: #EF6C00; color: white;">BANDEJA</td>

						                </tr>
						            <?php
						        }
						             endforeach ?>
		                        </tbody>
		                    </table>
		                </div>
		                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
		</div>
	</div>
</div>