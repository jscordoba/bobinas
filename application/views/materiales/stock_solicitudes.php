<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <div id="wrapper">
    <div id="page-wrapper">

    	 <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header righteous">Stock y Solicitudes</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>


		<div class="row">
		    <div class="col-lg-6">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Listado de Pendientes
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="table-responsive">
		                    <table class="table table-striped table-bordered table-hover" id="dataTables-SolPend">
		                        <thead>
		                            <tr>
		                                <th>ID</th>
		                                <th>Item</th>
		                                <th>Descripción</th>
		                                <th>Cantidad Kgs</th>
		                                <th>Fecha Solicitud</th>
		                                <th>Tipo</th>

		                            </tr>
		                        </thead>
		                        <tbody>
		                            <tbody>
				            	<?php 
				            	$i=1;
				            	foreach ($stock_solicitudes->result() as $stock_solicituder):
					            		if ($stock_solicituder->tipo == 1) {
					            	 ?>
						                <tr>
						                    <td><?= $i++; ?></td>
						                    <td><?= $stock_solicituder->material_descripcion; ?></td>
						                    <td><?= $stock_solicituder->solicitudm_cantidad; ?> Kgs</td>
						                    <td><?= $stock_solicituder->solicitudm_estado; ?></td>
						                    <td><?= $stock_solicituder->solicitudm_date; ?></td>
						                    <td style="background-color: #1565c0; color: white;">NORMAL</td>

						                </tr>
						            <?php
						        }else{
						        	?>
						                <tr>
						                    <td><?= $i++; ?></td>
						                    <td><?= $stock_solicituder->material_descripcion; ?></td>
						                    <td><?= $stock_solicituder->solicitudm_cantidad; ?> uds</td>
						                    <td><?= $stock_solicituder->solicitudm_estado; ?></td>
						                    <td><?= $stock_solicituder->solicitudm_date; ?></td>
						                    <td style="background-color: #EF6C00; color: white;">BANDEJA</td>

						                </tr>
						            <?php
						        }
						             endforeach ?>
		                        </tbody>
		                    </table>
		                </div>
		                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		        </div>
		     
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Solicitudes de Reintegro
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="table-responsive">
		                    <table class="table table-hover table-bordered table-striped" id="dataTables-SolReit">
	                            <thead>
	                                <tr>
	                                    <th>#</th>
	                                    <th>Item</th>
	                                    <th>Descripción</th>
	                                    <th>Disponible</th>
	                                    <th>Tipo</th>
	                                    <th>Acción</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                              <?php 
	                              $i=1;
	                              foreach ($stock_reintegro->result() as $stockr){ 
	                              	if ($stockr->tipo==1) {
	                              	?>
	                              <?php if ($stockr->solicitudr_estado=="PENDIENTE"){ ?>
	                                    <tr class="bg-warning">
	                              <?php $option='<td><button class="btn btn-xs btn-info">En Espera...</button></td>'; ?>
	                              			<td><?= $i++; ?></td>
		                                    <td><?= $stockr->material_id; ?></td>
		                                    <td><?= $stockr->material_descripcion; ?></td>
		                                    <td><?= $stockr->stockm_cantidad; ?> Kgs</td>
						                    <td style="background-color: #1565c0; color: white;">NORMAL</td>
		                                    

		                                    <?= $option; 
	                                    }elseif ($stockr->solicitudr_estado=="FINALIZADO") {
	                                    	# code...
	                                    } ?>
	                                
	                                   
	                                </tr>
	                              <?php }else{
	                              	?>
					                    <?php if ($stockr->solicitudr_estado=="PENDIENTE"){ ?>
	                                    <tr class="bg-warning">
	                                    <?php $option='<td><button class="btn btn-xs btn-info">En Espera...</button></td>'; ?>
	                              			<td><?= $i++; ?></td>
		                                    <td><?= $stockr->material_id; ?></td>
		                                    <td><?= $stockr->material_descripcion; ?></td>
		                                    <td><?= $stockr->stockm_cantidad; ?> Uds</td>
						                    <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
		                                    

		                                    <?= $option; 
	                                    }elseif ($stockr->solicitudr_estado=="FINALIZADO") {
	                                    	# code...
	                                    } ?>
	                                
	                                   
	                                </tr>
	                              	<?php
	                              }
	                              } ?>
	                            </tbody>
	                        </table>
		                </div>
		                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		      
		        <!-- /.panel -->
		    </div>
		        <!-- /.panel -->
		    </div>
		    <!-- /.col-lg-6 -->
		    <div class="col-lg-6">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Stock por Asignar
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
		                        <thead>
		                            <tr>
		                                <th>Item</th>
		                                <th>Descripción</th>
		                                <th>Total Stock</th>
		                                <th>Tipo</th>

		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php 
					            	$i=1;
					            	foreach ($stock_reintegro->result() as $stockr){ 
					            		if ($stockr->tipo== 1) {
					            		?>
						                <tr>
						                    <td><?= $stockr->material_id; ?></td>
						                    <td><?= $stockr->material_descripcion; ?></td>
						                    <td><?= $stockr->stockm_cantidad; ?> Kgs</td>
						                    <td style="background-color: #1565c0; color: white;">NORMAL</td>
						                    

						                </tr>
					            <?php }
					             else{
					             	?>
					             	  <tr>
						                    <td><?= $stockr->material_id; ?></td>
						                    <td><?= $stockr->material_descripcion; ?></td>
						                    <td><?= $stockr->stockm_cantidad; ?> Uds</td>
						                    <td style="background-color: #EF6C00; color: white;">BANDEJA</td>

						                </tr
					             	<?php
					             }
					         }

					             ?>

		                        </tbody>
		                    </table>
		                </div>
		                <!-- /.table-responsive -->

		                 <!-- Modal ASIGNAR MATERIAL Y OT A MAQUINA-->
			            <div class="modal fade" id="asignar" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
			              <div class="modal-dialog" role="document">
			                <div class="modal-content">
			                  <div class="modal-header">
			                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			                    <h4 class="modal-title righteous" id="title_material"><!-- Form Name -->
			                        Undefiend
			                    </h4>
			                  </div>
			                  <div class="modal-body oswald">
			                    <form class="form-horizontal" id="form_asignar_mat">
			                        <fieldset>

									<!-- Text input-->
                                    <div class="form-group">
                                      <div class="col-xs-12 col-md-12 col-sm-12">
                                      <label class="control-label" for="textinput">Material en Kgs</label>  
                                      <div class="form-control" id="mat_disponible">Undefiend</div>
                                      <input type="hidden" name="mat_descripcion" id="input_mat_descripcion">
                                      <input type="hidden" name="mat_id" id="input_mat_id">
                                      <input type="hidden" name="mat_disponible" id="input_mat_disponible">
                                      <!-- <span class="help-block">*Cantidad Disponible en Kgs</span>   -->
                                      </div>
                                    </div>

                                    <!-- Select Basic -->
									<div class="form-group">
									  <div class="col-xs-12 col-md-12 col-sm-12">
									  <label class="control-label" for="selectbasic">Maquina/Estación</label>
									    <select id="sel_asig_maquina" name="asig_maquina" class="form-control">
									    	<?php 
									    	foreach ($maquinas->result() as $maquinar): ?>
									    		<option value="<?= $maquinar->maquina_id; ?>"><?= $maquinar->maquina_nombre; ?></option>
								            <?php endforeach ?>
									    </select>
									  </div>
									</div>

                                    <div class="form-group">
                                      <div class="btn-group col-xs-12 col-md-12 col-sm-12" data-toggle="buttons">
                                      <label for="year" class="control-label">Asignar</label>
                                        <input id="mat_cantidad" name="mat_cantidad" type="mat_cantidad" placeholder="# Kgs" class="form-control input-md">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <div class="btn-group col-xs-12 col-md-12 col-sm-12" data-toggle="buttons">
                                      <label for="year" class="control-label">Orden de Trabajo1 (OT)</label>
                                        <input id="ot_asignar" name="ot_asignar" type="ot_asignar" placeholder="Número de OT" class="form-control input-md">
                                      </div>
                                    </div>

			                        </fieldset>
			                    </form>

			                  </div>
			                  <div class="modal-footer">
			                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			                    <!-- generar codigo de barras y mostrar para impresion, al dar clic en asignar -->
			                    <button type="button" class="btn btn-primary" id="btn_asignar_material">Asignar</button> 
			                  </div>
			                </div>
			              </div>
			            </div>
			            <!-- Fin Modal -->

			            
		            </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
		    <!-- /.col-lg-6 -->

		   


		</div>
	</div>
</div>
