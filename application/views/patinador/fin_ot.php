<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>

 <div id="wrapper">
    <div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header righteous">Finalizar Labor</h1>
        </div>
      </div>

      <div class="tab-pane col-md-12 col-lg-6" id="finalizar">
        <form class="form-horizontal" id="form_reintegro_prod">
            <fieldset class="text-left">

            <div class="row">
              <div class="panel panel-default">
                <div class="panel-heading righteous">
                   <b> Finalización de Labor  </b>
                </div>
                
                <div class="panel-body oswald">
                  <div class="col-sm-12 col-md-12">
                    <!-- Text input-->
                    <div class="form-group">
                      <div class="col-md-6">
                        <label for="textinput">Codigo único</label>  
                        <input id="input_codbar" name="input_codbar" type="text" placeholder="Capture el Código de barras" class="form-control input-md" required>
                        <input type="hidden" name="input_codbar_rein" id="input_codbar_rein" required>
                        <input type="hidden" name="bodega_reintegro_origen" id="bodega_reintegro_origen" required>
                        <input type="hidden" name="mat_cantidad" id="mat_cantidad" required>
                        <input type="hidden" name="id" id="id" required>
                        <input type="hidden" name="movimiento" id="movimiento" required>
                        <input type="hidden" name="tipo" id="tipo" required>
                      </div>
                      <div class="col-md-6">
                        <label for="year">¿Reintegro?</label><br/>
                        <div class="btn-group col-md" data-toggle="buttons">
                          <label class="btn btn-default chk_reintegro" ><input id="cambiar" name="chk_reintegro" value="si" type="radio">SI</label>
                          <label class="btn btn-default active chk_reintegro"><input checked  name="chk_reintegro" value="no" type="radio">No</label>
                        </div>
                      </div>
                    </div>
                  
                    <!-- Text input-->
                    <div class="form-group">
                      <div class="col-md-6">
                          <label for="textinput">Sección</label>  
                        <div class="well well-sm autocodbar" id="codbar_seccion">Sección</div>
                      </div>
                    
                      <div class="col-md-6">
                          <label for="textinput">Item</label>  
                        <div class="well well-sm autocodbar" id="codbar_item">Item</div>
                        <input type="hidden" name="input_mat_rid" id="input_mat_rid">
                      </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                      <div class="col-md-12">
                      <label for="textinput">Descripción</label>  
                        <div class="well well-sm autocodbar" id="codbar_descripcion">Descripción</div>
                        <input type="hidden" name="input_mat_rdescripcion" id="input_mat_rdescripcion">
                      </div>
                    
                      <div class="col-md-6 hidden">
                      <label for="textinput">OT</label>  
                        <div class="well well-sm autocodbar" id="codbar_ot">Número de OT</div>
                        <input type="hidden" name="input_codbar_ot" id="input_codbar_ot">
                      </div>
                    </div>  
                     <div class="form-group">
                      <div class="col-md-6">
                        <label for="textinput">Bodega</label>  
                        <div class="well well-sm autocodbar" id="bodega">Bodega</div>
                      </div>
                      <div class="col-md-6">
                        <label for="textinput">Total</label>  
                          <div class="well well-sm autocodbar" id="codbar_cantidad">Total Kgs</div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="col-md-12 well well-sm" >
                            <button id="btn_finalizar_ot" name="btn_finalizar_ot" class="btn btn-block  btn-primary ">Enviar</button>
                          </div>
                        </div>
                      </div>
                    <!-- Text input-->
                  <!--   <div class="form-group">
                      <label class="col-md-4 control-label" for="textinput">Maquina</label>  
                      <div class="col-md-8">
                        <div class="well well-sm autocodbar" id="codbar_maquina">Maquina</div>
                      </div>
                    </div>   -->
     
                  </div>
       

                </div>

              </div>
  
            </div>

            </fieldset>

        </form>
      </div>

        <div class="panel panel-default">
            <div class="panel-heading righteous">
                <b>Detalles Labor</b>
            </div>
            <div class="panel-body table-responsive oswald">
              <table class="table table-striped table-bordered table-hover table-condensed" id="">
                  <thead>
                    <tr>
                        <!-- <th>Sección</th>
                        <th>Item</th>
                        <th>Descripción</th> -->
                        <th class="hidden">Ot</th>
                        <th>Total </th>
                        <th>Bodega</th>
                        <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody id="tabla_detalleot">
                    
                  </tbody>
              </table>
            </div>
        </div>
 
  </div>
</div>
<!-- Modal ASIGNAR MATERIAL Y OT A MAQUINA-->
<div class="modal fade" id="reasig_maq" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" id="modal_detail"> <!-- id=modal_detail -->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="title_reintegro"><!-- Form Name -->
            Undefiend
        </h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="form_reintegrar_mat">
          <fieldset>
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table_rein" id="">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>Und.</th>
                      <th>Tipo</th>
                      <th>Cantidad Kgs</th>
                      <th class="text-center">Und. Trasl.</th>
                      <th class="text-center">Cantidad Kgs</th>
                  </tr>
              </thead>
              <tbody id="tb_detail_reasignar">
              </tbody>
              <tr>
                <td colspan="2"><b>Total</b></td><td colspan="4"></td>
              </tr>
            </table>
          </div>
          <div class="form-group">
            <label for="year" class="col-md-1 control-label">¿Dónde?</label>
            <div class="btn-group col-md-4" data-toggle="buttons">
              <label class="btn btn-success chk_bodega"><input checked name="chk_bodega" value="bodega" type="radio"  required>Bod. Producción</label>
              <label class="btn btn-success chk_bodega"><input  name="chk_bodega" value="maquina" type="radio" required>Maquina</label>
            </div>
          </div>
          <!-- Select Basic -->
          <div class="form-group in_traslado_mq">
            <label for="year" class="col-xs-12 col-md-7 col-sm-7 control-label">Máquina</label>
            <div class="btn-group col-xs-12 col-md-5 col-sm-5" data-toggle="buttons">
              <select id="sel_asig_maquina" name="bodega_reintegro_destino" class="form-control" required>
                <option value="">Seleccione...</option>
                <?php 
                foreach ($maquinas->result() as $maquinar): ?>
                  <option value="<?= $maquinar->bodega_id; ?>"><?= $maquinar->maquina_nombre; ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="form-group in_traslado_mq hidden">
            <label for="year" class="col-xs-12 col-md-7 col-sm-7 control-label">Orden de Trabajo (OT)</label>
            <div class="btn-group col-xs-12 col-md-5 col-sm-5" data-toggle="buttons">
              <input id="ot_asignar" value="1" name="ot_asignar" type="ot_asignar" placeholder="Número de OT" class="form-control input-md" required>
            </div>
          </div>

          </fieldset>
        </form>


      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <!-- generar codigo de barras y mostrar para impresion, al dar clic en asignar -->
        <button type="button" class="btn btn-primary" id="btn_guardar_reintegro">Reintegrar</button> 
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="reasig_maq2" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" id="modal_detail"> <!-- id=modal_detail -->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="title_reintegro2"><!-- Form Name -->
            Undefiend
        </h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="form_reintegrar_mat2">
          <fieldset>
          <div class="col-md-6">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table_rein" id="">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>Und.</th>
                      <th>Tipo</th>
                      <th class="text-center">Und. Trasl.</th>
                  </tr>
              </thead>
              <tbody id="tb_detail_reasignar2">
              </tbody>
              <tr>
                <td colspan="2"><b>Total</b></td><td colspan="4"></td>
              </tr>
            </table>
          </div>
          </div>
          <div class="col-md-6">
            <label for="" >¿Dónde?</label>
            <div class="btn-group" data-toggle="buttons">
              <label class="btn  btn-success chk_bodega2"><input checked name="chk_bodega2" value="bodega" type="radio"  required>Bod. Producción</label>
              <label class="btn btn-success chk_bodega2"><input  name="chk_bodega2" value="maquina" type="radio" required>Maquina</label>
            </div>
         
          <!-- Select Basic -->
          <div class="in_traslado_mq2">
           
            <div class="btn-group col-xs-12 col-md-5 col-sm-5" data-toggle="buttons"> 
            <label for="">Máquina</label>
              <select id="sel_asig_maquina2" name="bodega_reintegro_destino" class="form-control" required>
                <option value="">Seleccione...</option>
                <?php 
                foreach ($maquinas->result() as $maquinar): ?>
                  <option value="<?= $maquinar->bodega_id; ?>"><?= $maquinar->maquina_nombre; ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div> 
          <div class="in_traslado_mq2 hidden">
            
            <div class="btn-group col-xs-12 col-md-5 col-sm-5" data-toggle="buttons">
            <label for="">Orden de Trabajo (OT)</label>
              <input id="ot_asignar2" name="ot_asignar" value="1" type="ot_asignar" placeholder="Número de OT" class="form-control input-md" required>
            </div>
          </div>
</div>
          </fieldset>
        </form>
        <div class="table-responsive">
           <table  class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <td>#</td>
              <th>Bandeja</th>
              <th>Item</th>
              <th>Descripción</th>
              <th>Bodega</th>
              <th>Cantidad</th>
            </tr>
          </thead>
          <tbody class="td_detail_und_detail">
            <tr><td align="center" colspan="6">Datos no disponibles para tipo NORMAL</td> </tr>
            
          </tbody>
        </table>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn_guardar_reintegro2">Reintegrar</button> 
      </div>
    </div>
  </div>
</div>
<!-- Fin Modal -->
<div id="snackbar">Habilitado para Finalizar..</div>