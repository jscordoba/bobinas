<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                <h1 class="page-header righteous">Inicio</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           

            <!-- /.row  CASILLAS DE CONTADORES-->
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <h4 class="righteous">Asignación de Material</h4>
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge"><?= $indicador_ot_act; ?></div>
                                    <div>Asignado</div>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge"><?= $indicador_ot_fin; ?></div>
                                    <div>Finalizadas</div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= base_url(); ?>c_patinador/finalizar_ot">
                            <div class="panel-footer oswald">
                                <span class="pull-left">Finalizar</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        	<h4 class="righteous">Entregas y Stock</h4>
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge"><?= $indicador_pdt; ?></div>
                                    <div>Pendientes</div>
                                </div>
                                <div class="col-xs-4 text-center oswald">
                                    <div class="huge"><?= $indicador_asig; ?></div>
                                    <div>Disponible</div>
                                </div>
                            </div>
                        </div>
                        
                        <a href="<?= base_url(); ?>c_patinador/stock_solicitudes">
                        <div class="panel-footer">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                        </a>
                    </div>
                </div>

            </div>



            <!-- Modal Nueva solicitud-->
            <div class="modal fade" id="nueva_solicitud" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title righteous" id="myModalLabel"><!-- Form Name -->
                        Solicitud de Material
                    </h4>
                  </div>
                  <div class="modal-body">
                    <form class="form-horizontal" id="form_sol_material">
                        <fieldset>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="textinput ">Material / Item</label>  
                          <div class="col-md-6">
                              <input id="material_id" name="material_id" type="number" placeholder="Número de Item o Nombre de Item" class="form-control input-md">
                              <span class="help-block">*Número de Item o Nombre de Item.</span>  
                          </div>
                        </div>

                         <!-- Text input-->
                        <div class="form-group">
                              <label class="col-md-4 control-label" for="textinput ">Descripción</label>  
                              <div class="col-md-6">
                                <div class="well well-sm" id="material_descripcion">
                                    Undefined
                                    <input type="hidden" value="input_material_descripcion" name="input_material_descripcion">
                                </div>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="textinput">Cantidad</label>  
                          <div class="col-md-6">
                              <input id="material_cantidad" name="material_cantidad" type="number" placeholder="Cantidad en Kgs" class="form-control input-md">
                              <span class="help-block">*Cantidad en Kgs</span>  
                          </div>
                        </div>

                        </fieldset>
                    </form>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btn_sol_material">Solcitar</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Fin Modal -->

        </div>
            <!-- /.row -->

    </div>


    <!-- /#wrapper -->
