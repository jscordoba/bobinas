<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
 <div id="wrapper" class="body-min">
    <div id="page-wrapper">
    	 <div class="row">
            <div class="col-lg-12">
			<h1 class="page-header righteous">Entrega y Stock</h1>
            </div>
        </div>
		<div class="row">
		    <div class="col-sm-12 col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Stock por Entregar
		            </div>
		            <div class="panel-body table-responsive oswald">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-exampl">
	                        <thead>
	                            <tr>
	                            	<th>ID</th>
	                                <th>Item</th>
	                                <th>Descripción</th>
	                                <th>Stock</th>
	                                <th>Stock Disp.</th>
	                                <th>Imprevistos</th>
	                                <th>Tipo</th>
	                                <th>Acción</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            <?php
$i = 1;
foreach ($stock_reintegro->result() as $stockr) {
    if ($stockr->solicitudr_estado == "PENDIENTE") {
    } else {
        if ($stockr->tipo == 1) {
            ?>
					                <tr>
					                	<td><?=$stockr->stockm_id?></td>
					                    <td><?=$stockr->material_id;?></td>
					                    <td><?=$stockr->material_descripcion;?></td>
					                    <td><?=$stockr->stockm_cantidad;?> Kgs</td>
					                    <?php
$this->load->model('M_patinador');
            $result_ver_stock = $this->M_patinador->ver_stock($stockr->material_id);
            foreach ($result_ver_stock->result() as $row_ver_stock) {
                ?>
            	<td><?=$row_ver_stock->stockm_cantidad?></td>
            	<?php
}
            ?>
					                    <td><?=$stockr->imprevistos;?></td>
						                <td style="background-color: #1565c0; color: white;">NORMAL</td>
					            		<td><button class="asignar_mat_modal btn btn-xs btn-success" data-material_descripcion="<?=$stockr->material_descripcion;?>" data-stockm_cantidad="<?=$stockr->stockm_cantidad;?>" data-mat_id="<?=$stockr->material_id;?>" data-id="<?=$stockr->stockm_id;?>"  data-movimiento="<?=$stockr->movimiento_id;?>" data-tipo="1">Entregar</button></td>
					                </tr>
				            <?php } else {
            ?>
									   <tr>
					                    <td><?=$stockr->material_id;?></td>
					                    <td><?=$stockr->material_descripcion;?></td>
					                    <td><?=$stockr->stockm_cantidad;?> Uds</td>
					                    <td><?=$stockr->imprevistos;?></td>
						                <td style="background-color: #EF6C00; color: white;">BANDEJA</td>
					            		<td><button class="asignar_mat_modal btn btn-xs btn-success" data-material_descripcion="<?=$stockr->material_descripcion;?>" data-target="#asignar" data-toggle="modal" data-stockm_cantidad="<?=$stockr->stockm_cantidad;?>" data-id="<?=$stockr->stockm_id;?>" data-mat_id="<?=$stockr->material_id;?>" data-movimiento="<?=$stockr->movimiento_id;?>" data-tipo="2">Entregar</button></td>
					                </tr>
				            	<?php
}
    }
}
?>
	                        </tbody>
	                    </table>
	                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
		    <!-- <div class="col-sm-12 col-lg-12"> -->

		    <div class="tab-pane fade in active">
		    <div class="dataTable_wrapper">
		        <div class="panel-body table-responsive">
                	<table class="table table-striped table-bordered table-hover oswald" id="dataTables-exampl">
						<thead>
							<tr>
								<th>ID</th>
								<th>Item</th>
								<th>Descripción</th>
								<th>Imprevistos</th>
								<th>Tipo</th>
								<th>Traslado</th>
								<th>Detalles</th>

							</tr>
						</thead>
						<tbody>
							<?php foreach ($sol_alistar->result() as $sol_pendienter):
    if ($sol_pendienter->tipo == 1) {
        ?>
																		<tr>
																			<td><?=$sol_pendienter->solicitudm_id;?></td>
																			<td><?=$sol_pendienter->material_id;?></td>
																			<td><?=$sol_pendienter->material_descripcion;?></td>
																			<td><?=$sol_pendienter->imprevistos;?></td>

																			<td style="background-color: #1565c0; color: white;">NORMAL</td>
																				<td><button class="btn btn-primary btn-xs btn_alistar_prod_modal" d
																					data-sol_id="<?=$sol_pendienter->solicitudm_id;?>" data-material = "<?=$sol_pendienter->material_id;?>" data-descripcion = "<?=$sol_pendienter->material_descripcion;?>" data-imprevistos="<?=$sol_pendienter->imprevistos;?>" data-movimiento="<?=$sol_pendienter->id_detalle;?>" data-tipo="1"
																					>Trasladar</button></td>
																					<td><button class="btn btn-success btn-xs btn_detail_al" data-cant = "<?=$sol_pendienter->stockm_cantidad;?>" data-material = "<?=$sol_pendienter->material_id;?>"
																					data-sol_id="<?=$sol_pendienter->solicitudm_id;?>"  data-toggle="modal" data-target="#detalles_listo"
																					>Ver</button></td>


																		</tr>
																		<?php
    } else {
        ?>
																						<tr>
																			<td><?=$sol_pendienter->solicitudm_id;?></td>
																			<td><?=$sol_pendienter->material_id;?></td>
																			<td><?=$sol_pendienter->material_descripcion;?></td>
																			<td><?=$sol_pendienter->imprevistos;?></td>
																			<td style="background-color: #EF6C00; color: white;">BANDEJA</td>
																				<td><button class="btn btn-primary btn-xs btn_alistar_prod_modal" d
																					data-sol_id="<?=$sol_pendienter->solicitudm_id;?>"  data-imprevistos="<?=$sol_pendienter->imprevistos;?>" data-material = "<?=$sol_pendienter->material_id;?>" data-descripcion = "<?=$sol_pendienter->material_descripcion;?>" data-movimiento="<?=$sol_pendienter->id_detalle;?>" data-tipo="2"
																					>Trasladar</button></td>
																					<td><button class="btn btn-success btn-xs btn_detail_al2" data-cant = "<?=$sol_pendienter->stockm_cantidad;?>"
																					data-sol_id="<?=$sol_pendienter->solicitudm_id;?>" data-toggle="modal" data-target="#detalles_listo2"
																					>Ver</button></td>
																		</tr>
																				<?php
    }
endforeach?>
						</tbody>
		        	</table>
		    	</div>
			</div>
			</div>
		<!-- </div> -->
		    <!-- seccion de Listado de Pendientes -->
		    <div class="col-sm-12 col-lg-6">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Listado de Pendientes
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body">
		                <div class="table-responsive">
		                    <table class="table table-striped table-bordered table-hover oswald">
		                        <thead>
		                            <tr>
		                                <th>ID</th>
		                                <th>Item</th>
		                                <th>Descripción</th>
		                                <th>Cantidad Kgs</th>
		                                <th>Fecha Solicitud</th>
		                                <th>Tipo</th>

		                            </tr>
		                        </thead>
		                        <tbody>
		                            <tbody>
				            	<?php
$i = 1;
foreach ($stock_solicitudes->result() as $stock_solicituder):
    if ($stock_solicituder->tipo == 1) {

        ?>
																                <tr>
																                    <td><?=$i++;?></td>
																                    <td><?=$stock_solicituder->material_id;?></td>
																                    <td><?=$stock_solicituder->material_descripcion;?></td>
																                    <td><?=$stock_solicituder->solicitudm_cantidad;?> Kgs</td>
																                    <td><?=$stock_solicituder->solicitudm_date;?></td>
																	                <td style="background-color: #1565c0; color: white;">NORMAL</td>

																                </tr>
																            <?php
    } else {
        ?>
																       				<tr>
																                    <td><?=$i++;?></td>
																                    <td><?=$stock_solicituder->material_id;?></td>
																                    <td><?=$stock_solicituder->material_descripcion;?></td>
																                    <td><?=$stock_solicituder->solicitudm_cantidad;?> Uds</td>
																                    <td><?=$stock_solicituder->solicitudm_date;?></td>
																	                <td style="background-color: #EF6C00; color: white;">BANDEJA</td>

																                </tr>
																        			<?php
    }
endforeach?>
		                        </tbody>
		                    </table>
		                </div>
		                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>
		    <!-- seccion de Solicitudes de reintegro pendientes -->
		    <div class="col-sm-12 col-lg-6">
		        <div class="panel panel-default">
		            <div class="panel-heading righteous">
		                Solicitudes de Reintegro
		            </div>
		            <!-- /.panel-heading -->
		            <div class="panel-body oswald">
		                <div class="table-responsive">
		                    <table class="table table-striped table-bordered table-hover">
	                            <thead>
	                                <tr>
	                                    <th>#</th>
	                                    <th>Item</th>
	                                    <th>Descripción</th>
	                                    <th>Disponible</th>
	                                    <th>Tipo</th>
	                                    <th>Acción</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                              <?php
$i = 1;
foreach ($stock_reintegro2->result() as $stockr) {
    if ($stockr->tipo == 1) {
        ?>
	                              <?php if ($stockr->solicitudr_estado == "PENDIENTE") {
            ?>
	                                    <tr class="bg-warning">
	                              <?php $option = '<td><button class="btn btn-xs btn-info">En Espera...</button></td>';?>
	                              			<td><?=$i++;?></td>
		                                    <td><?=$stockr->material_id;?></td>
		                                    <td><?=$stockr->material_descripcion;?></td>
		                                    <td><?=$stockr->stockm_cantidad;?> Kgs</td>
						                <td style="background-color: #1565c0; color: white;">NORMAL</td>

		                                    <?=$option;
        } elseif ($stockr->solicitudr_estado == "FINALIZADO") {
            # code...
        }?>


	                                </tr>
	                              <?php } else {
        ?>
	                              <?php if ($stockr->solicitudr_estado == "PENDIENTE") {
            ?>

	                              	   <tr class="bg-warning">
	                              <?php $option = '<td><button class="btn btn-xs btn-info">En Espera...</button></td>';?>
	                              			<td><?=$i++;?></td>
		                                    <td><?=$stockr->material_id;?></td>
		                                    <td><?=$stockr->material_descripcion;?></td>
		                                    <td><?=$stockr->stockm_cantidad;?> Uds</td>
						                <td style="background-color: #EF6C00; color: white;">BANDEJA</td>

		                                    <?=$option;
        } elseif ($stockr->solicitudr_estado == "FINALIZADO") {
            # code...
        }?>


	                                </tr>
	                              	<?php
}
}?>
	                            </tbody>
	                        </table>
		                </div>
		                <!-- /.table-responsive -->
		            </div>
		            <!-- /.panel-body -->
		        </div>
		        <!-- /.panel -->
		    </div>

		</div>
	</div>
</div>






 <!-- Modal ASIGNAR MATERIAL Y OT A MAQUINA-->
<div class="modal fade" id="asignar" tabindex="1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" id=""> <!-- id=modal_detail -->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title righteous" id="title_material"><!-- Form Name -->
            Undefiend
        </h4>
      </div>
      <div class="modal-body oswald">
        <form class="form-horizontal" id="form_asignar_mat">
            <fieldset>
			<div class="table-responsive">
				<input type="text" placeholder="Buscar Codigo de barras" class="form-control" name="" id="search" onkeyup="Search()">
			 	<table class="table table-striped table-bordered table-hover" id="dataTables-examp">
		            <thead>
		                <tr>
		                	<th>Sel</th>
		                    <th>Codigo de barras</th>
		                    <th>Presentación</th>
		                    <th>Cant. </th>
		                    <th>Und. Entregar</th>
		                    <th>Patinador</th>
		                </tr>
		            </thead>
		            <tbody id="tb_detail_asignar">
		            </tbody>
		        </table>
			</div>
	        <div class="form-group">
	        	<label for="year" class="col-xs-12 col-md-7 col-sm-7 control-label">Máquina</label>
          		<div class="btn-group col-xs-12 col-md-5 col-sm-5" data-toggle="buttons">
					<select id="sel_asig_maquina" name="asig_maquina" class="form-control" required>
						<option value="">Seleccione...</option>
			    	<?php
foreach ($maquinas->result() as $maquinar): ?>
			    		<option value="<?=$maquinar->bodega_id;?>"><?=$maquinar->maquina_nombre;?></option>
		            <?php endforeach?>
					</select>
				</div>
			</div>
            <div class="form-group hidden">
              <label for="year" class="col-xs-12 col-md-7 col-sm-7 control-label">Orden de Trabajo (OT)</label>
              <div class="btn-group col-xs-12 col-md-5 col-sm-5" data-toggle="buttons">
                <input id="ot_asignar" value="1" name="ot_asignar" type="ot_asignar" placeholder="Número de OT" class="form-control input-md" required>
              </div>
            </div>

            </fieldset>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <!-- generar codigo de barras y mostrar para impresion, al dar clic en asignar -->
        <button type="button" class="btn btn-primary"  id="btn_asignar_material" >Entregar</button>
      </div>
    </div>
  </div>
</div>
<!-- Fin Modal -->


<div class="modal fade" id="detalles_listo">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title righteous">Preparación</h4>
			</div>
			<div class="modal-body oswald">
				<div class="row">
					<div class="col-md-12">


					<table class="table ">
					<caption class="text-success">Las cantidades de cada Material Puede ser editada</caption>
					<thead>
						<tr>
							<th>Presentación</th>
							<th>Cantidad en Kgs</th>
							<th>Subtotal</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody id="alistar_detalles">
					</tbody>
				</table>
				</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-3  text-center">
			    		<label for="">Cant. Preparada</label><br>
			    		<label for="" class="text-success" id="cant_preparada"></label><span> Kgs</span>
	        		</div>
	        		<div class="col-md-3 text-center">
			    		<label for="">Cant. Nueva</label><br>
			    		<label for="" class="text-success" id="cant_nueva">0</label><span>Kgs</span>
	        		</div>
	        		<div class="col-md-3 text-center">
			    		<label for="">Disponible</label><br>
			    		<label for="" class="text-success" id="cant_disponible3"></label><span> Kgs</span>
	        		</div>
	        		<div class="col-md-3 text-center">
			    		<label for="">Queda</label><br>
			    		<label for="" class="text-danger" id="cant_uso3">0</label><span> Kgs</span>
	        		</div>

	        	</div>
				</div>
				<input type="number" class="hidden tipo" >

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="detalles_listo2">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title righteous">Preparación</h4>
			</div>

			<div class="modal-body oswald">
				<div class="row">
					<div class="col-md-12">

				<table class="table">
					<caption class="text-success">Las Unidades de cada Bandeja Puede ser editada</caption>
					<thead>
						<tr>
							<th>Presentación</th>
							<th>Unidades</th>
							<th>Subtotal</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody id="alistar_detalles2">
					</tbody>
				</table>
			</div>



			</div>
			<div class="row">
					<div class="col-md-12">
						<div class="col-md-3  text-center">
			    		<label for="">Cant. Preparada</label><br>
			    		<label for="" class="text-success" id="cant_preparada2"></label><span> Uds</span>
	        		</div>
	        		<div class="col-md-3 text-center">
			    		<label for="">Cant. Nueva</label><br>
			    		<label for="" class="text-success" id="cant_nueva2">0</label><span>Uds</span>
	        		</div>
	        		<div class="col-md-3 text-center">
			    		<label for="">Disponible</label><br>
			    		<label for="" class="text-success" id="cant_disponible4"></label><span> Uds</span>
	        		</div>
	        		<div class="col-md-3 text-center">
			    		<label for="">Queda</label><br>
			    		<label for="" class="text-danger" id="cant_uso4">0</label><span> Uds</span>
	        		</div>

	        	</div>
				</div>
			<input type="number" class="hidden tipo" >
		</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>



