$(document).ready(function() { // ------ INICIO JQUERY ----------- //
    $('#material_id').focus();
    $('#material_id').keyup(function(e) {
        /* Act on the event */
        var mat_id = $('#material_id').val();
        //alert (mat_id);
        if (mat_id != "") {
            //alert(mat_id);
            $.post('../ajax/buscar_item', {
                query: mat_id
            }, function(data, textStatus, xhr) {
                //alert(data);
                console.log("success");
                $('#material_descripcion').text('Undefined');
                $('#input_material_descripcion').val('Undefined');
                if (data != "") {
                    $('#material_descripcion').empty();
                    $('#material_descripcion').append(data);
                    $('#input_material_descripcion').val(data);
                };
            });
        } else {
            $('#material_descripcion').text('Undefined');
            $('#input_material_descripcion').val('Undefined');
        }
    });
    jQuery(document).on("keyup", ".codigo", function() {
        var codigo = $(this).val();
        var presentacion = $(this).parents('tr').find('input.presentacion');
        var cantidad = $(this).parents('tr').find('input.inp_cantidad_disponible');
        var valor = $(this).parents('tr').find('input.inp_cantidad');
        var sub = $(this).parents('tr').find('label.lbl_subtotal');
        if (codigo != "") {
            $.post('../ajax/buscar_codigo', {
                query: codigo
            }, function(data, textStatus, xhr) {
                presentacion.val('Undefined');
                cantidad.val('0');
                valor.val('0');
                sub.text('0');
                var result = eval(data);
                if (data != "") {
                    presentacion.val(result[0]);
                    cantidad.val(result[1]);
                    valor.val(result[1]).trigger("change");
                };
            });
        } else {
            presentacion.val('Undefined');
            cantidad.val('0');
            valor.val('0');
            sub.text('0');
        }
    });
    jQuery(document).on("keyup", ".codigo2", function() {
        var codigo = $(this).val();
        var presentacion = $(this).parents('tr').find('input.presentacion2');
        var cantidad = $(this).parents('tr').find('input.inp_cantidad_disponible2');
        var valor = $(this).parents('tr').find('input.inp_unds2');
        var sub = $(this).parents('tr').find('label.lbl_subtotal2');
        if (codigo != "") {
            $.post('../ajax/buscar_codigo', {
                query: codigo
            }, function(data, textStatus, xhr) {
                presentacion.val('Undefined');
                cantidad.val('0');
                valor.val('0');
                sub.text('0');
                var result = eval(data);
                if (data != "") {
                    presentacion.val(result[0]);
                    cantidad.val(result[1]);
                    valor.val(result[1]).trigger("change");
                };
            });
        } else {
            presentacion.val('Undefined');
            cantidad.val('0');
            valor.val('0');
            sub.text('0');
        }
    });
    $('#material_id2').keyup(function(e) {
        /* Act on the event */
        var mat_id = $('#material_id2').val();
        //alert (mat_id);
        if (mat_id != "") {
            // alert(mat_id);
            $.post('../ajax/buscar_item', {
                query: mat_id
            }, function(data, textStatus, xhr) {
                //alert(data);
                console.log("success");
                $('#material_descripcion2').text('Undefined');
                $('#input_material_descripcion2').val('Undefined');
                if (data != "") {
                    $('#material_descripcion2').empty();
                    $('#material_descripcion2').append(data);
                    $('#input_material_descripcion2').val(data);
                };
            });
        } else {
            $('#material_descripcion2').text('Undefined');
            $('#input_material_descripcion2').val('Undefined');
        }
    });
    //--- SUBMIT PARA REGISTRO DE SOLICITUD DE MATERIAL ------------
    $('#btn_sol_material').click(function(event) {
        var material = $("#material_id").val();
        var descripcion = $("#input_material_descripcion").val();
        var cantidad = $("#material_cantidad").val();
        var observacion = $("#observacion").val();
        if ((material != "") && (descripcion != "Undefined" && descripcion != "") && (cantidad != 0 && cantidad != "") && (observacion != "")) {
            $('#form_sol_material').attr({
                action: 'reg_material/1',
                method: 'POST'
            });
            $('#form_sol_material').submit();
        } else {
            alert("Debe completar los campos con valores válidos.");
        }
    });
    $('#btn_sol_material2').click(function(event) {
        var material = $("#material_id2").val();
        var descripcion = $("#input_material_descripcion2").val();
        var cantidad = $("#material_cantidad2").val();
        var observacion = $("#observacion2").val();
        if ((material != "") && (descripcion != "Undefined" && descripcion != "") && (cantidad != 0 && cantidad != "") && (observacion != "")) {
            $('#form_sol_material2').attr({
                action: 'reg_material/2',
                method: 'POST'
            });
            $('#form_sol_material2').submit();
        } else {
            alert("Debe completar los campos con valores válidos.");
        }
    });
    $("#btn-new-entry").click(function(event) {
        $("#reg-new-entry").attr('disabled', 'true');
    });
    $('#stock_material_desc').change(function(event) {
        var material_desc = $('#stock_material_desc').val();
        if (material_desc != "Undefined" && material_desc != "") {
            $("#reg-new-entry").removeAttr('disabled');
        };
    });
    $('#stock_material_id').focus();
    $('#stock_material_id').keyup(function(e) {
        /* Act on the event */
        var mat_id = $('#stock_material_id').val();
        var mat_cant = $('#stock_material_cant').val();
        var mat_desc = $('#stock_material_desc').val();
        //alert(mat_id);
        $.post('../ajax/buscar_item', {
            query: mat_id
        }, function(data) {
            //alert(data);
            console.log(data);
            $('#stock_material_desc').val('Undefined');
            if (data != "") {
                $('#stock_material_desc').val(data);
            };
        });
        if (mat_cant != "" && mat_desc != "" && mat_desc != "Undefined" && mat_id != "") {
            $("#reg-new-entry").removeAttr('disabled');
        } else {
            $("#reg-new-entry").attr('disabled', 'true');
        };
    });
    $('#stock_material_cant').keyup(function(e) {
        /* Act on the event */
        var mat_cant = $('#stock_material_cant').val();
        var mat_id = $('#stock_material_id').val();
        var mat_desc = $('#stock_material_desc').val();
        //alert (mat_id);
        if (mat_cant != "" && mat_desc != "" && mat_desc != "Undefined" && mat_id != "") {
            $("#reg-new-entry").removeAttr('disabled');
        } else {
            $("#reg-new-entry").attr('disabled', 'true');
        }
    });
    $('#stock_material_desc').keyup(function(e) {
        /* Act on the event */
        var mat_cant = $('#stock_material_cant').val();
        var mat_id = $('#stock_material_id').val();
        var mat_desc = $('#stock_material_desc').val();
        //alert (mat_id);
        if (mat_cant != "" && mat_desc != "" && mat_desc != "Undefined" && mat_id != "") {
            $("#reg-new-entry").removeAttr('disabled');
        } else {
            $("#reg-new-entry").attr('disabled', 'true');
        }
    });
    //--- CARGAR DATOS EN MODAL ANTES DE HACER SUBMIT DE REGISTRO TRASLADO DE BOD_PPAL A BOD_PROD ------------ NUEVO REGISTRO DE TRASLADO
    $('.btn_trasladar_prod').click(function(event) {
        $("#tipo").val('1');
        var sol_id = $(this).data('sol_id');
        var mat_id = $(this).data('mat_id');
        var mat_descrip = $(this).data('mat_descrip');
        var mat_cant = $(this).data('mat_cant');
        var stock_cantidad = $(this).data('stockm_cantidad');
        var cantidad = $(this).data('mat_cant');
        var imprevistos = $(this).data('imprevistos');
        $("#tittle_detlles").text(cantidad);
        $("#idver").text(sol_id);
        $("#itemver").text(mat_id);
        $("#descripcionver").text(mat_descrip);
        $("#cantidadver").text(mat_cant);
        $("#solicitudver").text(stock_cantidad);
        $("#imprevistos").val(imprevistos);
        var titulo = mat_descrip + " - <b class='text-primary'>" + mat_cant + " Kgs</b>";
        var total = $("#cant_total").text();
        if (total > 0 && total <= mat_cant) {
            $("#cant_total").removeClass('text-danger');
            $("#cant_total").addClass('text-primary');
            $("#btn_reg_traslado").attr('disabled', 'false');
        } else {
            $("#cant_total").removeClass('text-primary');
            $("#cant_total").addClass('text-danger');
            $("#btn_reg_traslado").attr('disabled', 'true');
        }
        $('#tittle_detalles').html(titulo);
        $('#alm_input_sol_id').val(sol_id);
        $('#alm_input_mat_id').val(mat_id);
        $('#alm_input_mat_descrip').val(mat_descrip);
        $('#alm_input_mat_cant').val(mat_cant); //cantidad solicitada por Materiales
        $('#alm_input_stock_cantidad').val(stock_cantidad); //cantidad en Kgs en STOCK
        $("#cant_disponible").text(stock_cantidad);
    });
    $('.btn_trasladar_prod2').click(function(event) {
        $("#tipo").val('2');
        var sol_id = $(this).data('sol_id2');
        var mat_id = $(this).data('mat_id2');
        var mat_descrip = $(this).data('mat_descrip2');
        var mat_cant = $(this).data('mat_cant2');
        var stock_cantidad = $(this).data('stockm_cantidad2');
        var cantidad = $(this).data('mat_cant2');
        var imprevistos = $(this).data('imprevistos2');
        $("#tittle_detlles2").text(cantidad);
        $("#imprevistos2").val(imprevistos);
        var titulo = mat_descrip + " - <b class='text-primary'>" + mat_cant + " Uds</b>";
        var total = $("#cant_total2").text();
        if (total > 0 && total <= mat_cant) {
            $("#cant_total2").removeClass('text-danger');
            $("#cant_total2").addClass('text-primary');
            $("#btn_reg_traslado2").attr('disabled', 'false');
        } else {
            $("#cant_total2").removeClass('text-primary');
            $("#cant_total2").addClass('text-danger');
            $("#btn_reg_traslado2").attr('disabled', 'true');
        }
        $('#tittle_detalles2').html(titulo);
        $('#alm_input_sol_id2').val(sol_id);
        $('#alm_input_mat_id2').val(mat_id);
        $('#alm_input_mat_descrip2').val(mat_descrip);
        $('#alm_input_mat_cant2').val(mat_cant); //cantidad solicitada por Materiales
        $('#alm_input_stock_cantidad2').val(stock_cantidad); //cantidad en Kgs en STOCK
        $("#cant_disponible2").text(stock_cantidad);
    });
    $(document).on('change', '#validar_entrega', function(event) {
        if ($(this).val() > $(this).data('valor')) {
            $("#btn_asignar_material").attr('disabled', true);
        } else {
            $("#btn_asignar_material").attr('disabled', false);
        }
    });
    // ---------------------- CARGAR DATOS AL MODAL  AJAX MODIFICADO 14-06-16 --------------------//
    $('.btn_trasladar_prod_mod').click(function(event) {
        var sol_id = $('#alm_input_sol_id').val();
        var mat_id = $('#alm_input_mat_id').val();
        var mat_descrip = $('#alm_input_mat_descrip').val();
        var mat_cant1 = $('#alm_input_mat_cant').val();
        var stock_cantidad = $('#alm_input_stock_cantidad').val();
        $.post('../ajax/get_detail_traslado_mod', {
            mat_id: mat_id
        }, function(data, textStatus, xhr) {
            $('#add_traslado').append(data);
        });
    });
    $('.btn_trasladar_prod_mod2').click(function(event) {
        var sol_id = $('#alm_input_sol_id2').val();
        var mat_id = $('#alm_input_mat_id2').val();
        var mat_descrip = $('#alm_input_mat_descrip2').val();
        var mat_cant1 = $('#alm_input_mat_cant2').val();
        var stock_cantidad = $('#alm_input_stock_cantidad2').val();
        // $.post('../ajax/get_detail_traslado_mod2', {
        //     mat_id: mat_id
        // }, function(data, textStatus, xhr) {
        //     $('#add_traslado2').append(data);
        // });
    });
    $("body").on("click", ".remove_detail", function(event) {
        $(this).parent().parent().remove();
        var mat_cant = $('#alm_input_mat_cant').val();
        var subtotal_gral = 0;
        $(".lbl_subtotal").each(function(index, el) {
            subtotal_gral = parseInt(subtotal_gral) + parseInt($(this).text()); //poner valor subtotal
        });
        if (subtotal_gral > 0 && subtotal_gral <= mat_cant) {
            $("#cant_total").removeClass('text-danger');
            $("#cant_total").addClass('text-primary');
            $("#btn_reg_traslado").removeAttr('disabled');
        } else {
            $("#cant_total").removeClass('text-primary');
            $("#cant_total").addClass('text-danger');
            $("#btn_reg_traslado").attr('disabled', 'true');
        }
        var cant_disponible = $("#cant_disponible").text();
        var new_disponible = cant_disponible - subtotal_gral;
        $("#cant_uso").text(new_disponible); //poner valor de la nueva cantidad Disponible
        $("#cant_total").text(subtotal_gral); //poner valor de la nueva cantidad total a trasladar
    });
    $("body").on("change", ".inp_unds", function(event) { //CHANGE FOR INPUT UNIDADES
        /* Act on the event */
        var mat_cant = $('#alm_input_mat_cant').val();
        var unidades = $(this).val();
        var cantidad = $(this).parent().next().children('input').val();
        var subtotal = unidades * cantidad;
        $(this).parent().next().next().children('label').text(subtotal); //poner valor subtotal
        var subtotal_gral = 0;
        $(".lbl_subtotal").each(function(index, el) {
            subtotal_gral = parseInt(subtotal_gral) + parseInt($(this).text()); //sacar valor subtotal
        });
        if (subtotal_gral > 0 && subtotal_gral <= mat_cant) {
            $("#cant_total").removeClass('text-danger');
            $("#cant_total").addClass('text-primary');
            $("#btn_reg_traslado").removeAttr('disabled');
        } else {
            $("#cant_total").removeClass('text-primary');
            $("#cant_total").addClass('text-danger');
            $("#btn_reg_traslado").attr('disabled', 'true');
        }
        var cant_disponible = $("#cant_disponible").text();
        var new_disponible = cant_disponible - subtotal_gral;
        $("#cant_uso").text(new_disponible); //poner valor de la nueva cantidad Disponible
        $("#cant_total").text(subtotal_gral); //poner valor de la nueva cantidad total a trasladar
    });
    $(document).on("change", ".inp_cantidad", function(event) { //CHANGE FOR INPUT CANTIDAD
        /* Act on the event */
        alert - ("Hola");
        var mat_cant = $('#alm_input_mat_cant').val();
        var unidades = $(this).val();
        var subtotal = unidades;
        $(this).parent().next().children('label').text(subtotal);
        var subtotal_gral = 0;
        $(".lbl_subtotal").each(function(index, el) {
            subtotal_gral = parseFloat(subtotal_gral) + parseFloat($(this).text()); //poner valor subtotal
            //alert(subtotal_gral);
        });
        if (subtotal_gral > 0 && subtotal_gral <= mat_cant) {
            $("#cant_total").removeClass('text-danger');
            $("#cant_total").addClass('text-primary');
            $("#btn_reg_traslado").removeAttr('disabled');
        } else {
            $("#btn_reg_traslado").removeClass('text-primary');
            $("#btn_reg_traslado").addClass('text-danger');
            $("#btn_reg_traslado").removeAttr('disabled');
        }
        var cant_disponible = $("#cant_disponible").text();
        var new_disponible = cant_disponible - subtotal_gral;
        $("#cant_uso").text(new_disponible); //poner valor de la nueva cantidad Disponible
        $("#cant_total").text(subtotal_gral); //poner valor de la nueva cantidad total a trasladar
        if ($("#cant_uso").text() < 0) {
            $("#btn_reg_traslado").attr('disabled', true);
            $("#btn_reg_traslado").text('Valor Excedido');
        } else {
            $("#btn_reg_traslado").attr('disabled', false);
            $("#btn_reg_traslado").text('Preparar');
        }
        var valor_maximo = $(this).parents('tr').find('.inp_cantidad_disponible').val();
        if (parseFloat(unidades) > parseFloat(valor_maximo)) {
            $(this).val(0);
            $(this).parents('tr').find('.lbl_subtotal').text(0);
        }
    });
    $("body").on("change", ".inp_unds2", function(event) { //CHANGE FOR INPUT UNIDADES
        var mat_cant = $('#alm_input_mat_cant2').val();
        var unidades = $(this).val();
        var subtotal = unidades;
        // alert(subtotal);
        $(this).parent().next().children('label').text(subtotal);
        var subtotal_gral = 0;
        $(".lbl_subtotal2").each(function(index, el) {
            subtotal_gral = parseInt(subtotal_gral) + parseInt($(this).text()); //poner valor subtotal
        });
        if (subtotal_gral > 0 && subtotal_gral <= mat_cant) {
            $("#cant_total2").removeClass('text-danger');
            $("#cant_total2").addClass('text-primary');
            $("#btn_reg_traslado2").removeAttr('disabled');
        } else {
            $("#btn_reg_traslado2").removeClass('text-primary');
            $("#btn_reg_traslado2").addClass('text-danger');
            $("#btn_reg_traslado2").attr('disabled');
        }
        var cant_disponible = $("#cant_disponible2").text();
        var new_disponible = cant_disponible - subtotal_gral;
        $("#cant_uso2").text(new_disponible); //poner valor de la nueva cantidad Disponible
        $("#cant_total2").text(subtotal_gral); //poner valor de la nueva cantidad total a trasladar
        if ($("#cant_uso2").text() < 0) {
            $("#btn_reg_traslado2").attr('disabled', true);
            $("#btn_reg_traslado2").text('Valor Excedido');
        } else {
            $("#btn_reg_traslado2").attr('disabled', false);
            $("#btn_reg_traslado2").text('Preparar');
        }
    });
    $('#btn_reg_traslado').click(function(event) {
        $('#form_traslado_prod').attr({
            action: 'traslado',
            method: 'POST'
        });
        $(".lbl_subtotal").each(function(index, ba) {
            if ($(this).text() == "0") {
                alert("Verifique los Valores");
                ba.preventDefault();
            }
        });
        $('#form_traslado_prod').submit();
    });
    //--- SUBMIT PARA TRASLADO DE BOD_PPAL A BOD_PROD ------------
    $('#btn_reg_traslado2').click(function(event) {
        $('#form_traslado_prod2').attr({
            action: 'traslado2',
            method: 'POST'
        });
        $(".lbl_subtotal2").each(function(index, ba) {
            if ($(this).text() == "0") {
                alert("Verifique los Valores");
                ba.preventDefault();
            }
        });
        $('#form_traslado_prod2').submit();
    });
    //--- ASIGNAR DATOS DE MATERIAL Y DISPONIBILIDAD A VENTANA MODAL ------------
    $('.asignar_mat_modal').click(function(event) {
        var tipo = $(this).data('tipo');
        $('#form_asignar_mat').attr({
            action: 'traslado',
            method: 'POST'
        });
        var mat_id = $(this).data('mat_id');
        var mat_stock = $(this).data('stockm_cantidad');
        var mat_descripcion = $(this).data('material_descripcion');
        var id = $(this).data('id');
        var movimiento = $(this).data('movimiento');
        $.post('../ajax/get_detail_traslado_prod', {
            mat_id: mat_id,
            mat_descripcion: mat_descripcion,
            mat_stock: mat_stock,
            id: id,
            movimiento: movimiento,
            tipo: tipo
        }, function(data, textStatus, xhr) {
            console.log(data);
            if (data != '' && data != null) {
                $('#tb_detail_asignar').empty();
                $('#tb_detail_asignar').append(data);
                $('#title_material').text(mat_descripcion);
                $('#mat_disponible').text(mat_stock + " Kgs");
                $("#asignar").modal("show");
            }
        });
    });
    //--- SUBMIT PARA TRASLADO DE BOD_PROD A MAQUINA ------------
    $('#btn_asignar_material').click(function(event) {
        if ($("#sel_asig_maquina").val() == "" || $("#ot_asignar").val() == "") {
            alert("Debes elegir una Maquina");
        } else if ($('#tb_detail_asignar input[type=checkbox]:checked').length === 0) {
            alert("Debes elegir al menos 1 Presentación");
        } else {
            $('#form_asignar_mat').submit();
        }
    });
    //--- RELLENAR TABLA DE DETALLES DE MAQUINAS ------------
    $('.btn_detail_maquina').click(function(event) {
        var maq_id = $(this).data('maq_id');
        var maq_nombre = $(this).data('maq_nombre');
        // alert(maq_id);
        // alert(mat_descrip);
        // alert(mat_cant);
        $('#head_nombre_maquina').text(maq_nombre);
        $.post('../ajax/detail_maquinao', {
            maq_id: maq_id
        }, function(data, textStatus, xhr) {
            //alert(data);
            console.log("success get info machines");
            if (data != "") {
                $('#tbody_detail_maq').empty();
                $('#tbody_detail_maq').append(data);
            } else {
                $('#tbody_detail_maq').html('<td colspan="7" class="text-center"><h4>--------- Undefined ---------</h4></td>');
            }
        });
    });

    function myFunction() {
        // Get the snackbar DIV
        var x = document.getElementById("snackbar")
        // Add the "show" class to DIV
        x.className = "show";
        // After 3 seconds, remove the show class from DIV
        setTimeout(function() {
            x.className = x.className.replace("show", "");
        }, 3000);
    }
    $(document).on('propertychange input', '#input_codbar', function(event) {
        //  event.preventDefault();
        //  /* Act on the event */
        // });
        // //--- AUTOCOMPLETAR CAMPOS PARA FINALIZAR OT DE UNA ESTACIÓN DE TRABAJO O MAQUINA ------------
        // $('#input_codbar').on('change', function() {
        /* Act on the event */
        var codbar = $('#input_codbar').val();
        //alert(codbar);
        if (codbar != "") {
            $.post('../ajax/ver_fin_ot', {
                codbar: codbar
            }, function(data, textStatus, xhr) {
                console.log("success get info codbar");
                if (data != "") {
                    var campos = data.split("|");
                    for (var i = 0; i < campos.length; i++) {
                        campos[i] = campos[i];
                    };
                    myFunction();
                    $('#input_mat_rid').val(campos[1]);
                    $('#input_mat_rdescripcion').val(campos[2]);
                    $('#input_codbar_ot').val(campos[3]);
                    $('#mat_cantidad').val(campos[4]);
                    $('#input_codbar_rein').val(codbar);
                    $('#bodega_reintegro_origen').val(campos[6]);
                };
            });
            $.post('../ajax/ver_fin_ot_detail', {
                codbar: codbar
            }, function(data, textStatus, xhr) {
                console.log("success get info codbar");
                if (data != "") {
                    var campos = data.split("|");
                    for (var i = 0; i < campos.length; i++) {
                        campos[i] = campos[i];
                    };
                    myFunction();
                    $("#tabla_detalleot").empty();
                    $("#tabla_detalleot").append(data);
                    $(".finalizar_ot_detalle").trigger('click');
                };
            });
        } else {
            $('#input_maq_origen').empty();
            $('#input_codbar').empty();
            $('#codbar_seccion').text('Undefined');
            $('#codbar_item').text('Undefined');
            $('#codbar_descripcion').text('Undefined');
            $('#codbar_ot').text('Undefined');
            $('#codbar_cantidad').text('Undefined');
        }
    });
    //--- HABILITAR Y DESACTIVAR INPUT DE CANTIDAD REINTEGRO ------------//
    //--- RELLENAR DETALLE DE MATERIAL PARA REINTEGRAR O REASIGNAR A OTRA MAQUINA EN MODAL------------//
    $(".in_traslado_mq").css({
        display: 'none'
    });
    $(".in_traslado_mq2").css({
        display: 'none'
    });
    $(document).on('click', '.finalizar_ot_detalle', function(event) {
        $("#cambiar").attr('checked', false);
        var seccion = $(this).data('seccion_nombre');
        var item = $(this).data('material_id');
        var descripcion = $(this).data('material_descripcion');
        var total = $(this).data('maquinao_cantidad');
        var bodega = $(this).data('bodega_id');
        var id = $(this).data('id');
        var ot = $(this).data('maquinao_ot');
        var moviemiento = $(this).data('movimiento');
        var bodega_nombre = $(this).data('bodega_nombre');
        var tipo = $(this).data('tipo');
        $("#codbar_seccion").empty();
        $("#codbar_seccion").append(seccion);
        $("#codbar_item").empty();
        $("#codbar_item").append(item);
        $("#codbar_descripcion").empty();
        $("#codbar_descripcion").append(descripcion);
        $("#codbar_ot").empty();
        $("#codbar_ot").append(ot);
        $("#codbar_cantidad").empty();
        $("#codbar_cantidad").append(total);
        $('#input_mat_rid').val(item);
        $('#input_mat_rdescripcion').val(descripcion);
        $('#input_codbar_ot').val(ot);
        $('#mat_cantidad').val(total);
        $('#id').val(id);
        $('#movimiento').val(moviemiento);
        $('#tipo').val(tipo);
        $("#bodega").empty();
        $("#bodega").append(bodega_nombre);
        $('#bodega_reintegro_origen').val(bodega);
    });
    $(document).on('change', '.chk_reintegro', function(event) {
        var radio_reintegro = $("input[name='chk_reintegro']:checked").val();
        var tipo = $("#tipo").val();
        var bandeja = $("#input_mat_rid").val();
        if (radio_reintegro == "si") {
            if (tipo == 2) {
                $("#reasig_maq2").modal('show');
                $("#btn_finalizar_ot").attr("disabled", true);
                $('#form_reintegrar_mat2').attr({
                    action: 'traslado_mq2',
                    method: 'POST'
                });
                var codbar = $("#input_codbar").val();
                var title = $("#codbar_descripcion").text();
                var ot = $("#codbar_ot").text();
                var movimiento = $("#movimiento").val();
                if (title == "Descripción") {
                    titulo = "Undefined";
                    $("#btn_guardar_reintegro").attr("disabled", true);
                } else {
                    titulo = title + " => OT " + ot;
                    $("#btn_guardar_reintegro").attr("disabled", false);
                }
                $("#title_reintegro2").text(titulo);
                $.post('../ajax/get_detail_traslado_mq2', {
                    codbar: codbar,
                    movimiento: movimiento
                }, function(data, textStatus, xhr) {
                    if (data != "") {
                        $('#tb_detail_reasignar2').empty();
                        $('#tb_detail_reasignar2').append(data);
                        $.post('../ajax/get_detail_und_detail', {
                            bandeja: bandeja
                        }, function(data, textStatus, xhr) {
                            $(".td_detail_und_detail").empty();
                            $(".td_detail_und_detail").append(data);
                        });
                    } else {
                        $('#tb_detail_reasignar2').html('<td colspan="7" class="text-center"><h4>--------- Undefined ---------</h4></td>');
                    }
                });
            } else {
                $("#reasig_maq").modal('show');
                $("#btn_finalizar_ot").attr("disabled", true);
                $('#form_reintegrar_mat').attr({
                    action: 'traslado_mq',
                    method: 'POST'
                });
                var codbar = $("#input_codbar").val();
                var title = $("#codbar_descripcion").text();
                var ot = $("#codbar_ot").text();
                var movimiento = $("#movimiento").val();
                if (title == "Descripción") {
                    titulo = "Undefined";
                    $("#btn_guardar_reintegro").attr("disabled", true);
                } else {
                    titulo = title + " => OT " + ot;
                    $("#btn_guardar_reintegro").attr("disabled", false);
                }
                $("#title_reintegro").text(titulo);
                $.post('../ajax/get_detail_traslado_mq', {
                    codbar: codbar,
                    movimiento: movimiento
                }, function(data, textStatus, xhr) {
                    console.log("success get detail");
                    if (data != "") {
                        $('#tb_detail_reasignar').empty();
                        $('#tb_detail_reasignar').append(data);
                    } else {
                        $('#tb_detail_reasignar').html('<td colspan="7" class="text-center"><h4>--------- Undefined ---------</h4></td>');
                    }
                });
            }
        } else if (radio_reintegro == "no") {
            $("#btn_finalizar_ot").attr("disabled", false);
        };
        /* Act on the event */
    });
    //--- SUBMIT PARA TRASLADO DE UNA MAQUINA A OTRA MAQUINA O BOD_PPAL ------------
    $('#btn_guardar_reintegro').click(function(e) {
        var radio_bodega = $("input[name='chk_bodega']:checked").val();
        if (radio_bodega == "bodega") {
            $("input[type=number]").each(function(index, el) {
                if ($(this).val() == "" || $(this).val() == 0) {
                    alert("Campos Invalidos, Revisar");
                    el.preventDefault();
                }
            });
        } else if (radio_bodega == "maquina") {
            $("input[type=number]").each(function(index, el) {
                if ($(this).val() == "" || $(this).val() == 0) {
                    alert("Campos Invalidos, Revisar");
                    el.preventDefault();
                }
            });
            if ($("#sel_asig_maquina").val() == "" || $("#ot_asignar").val() == "") {
                alert("Campos Invalidos , Revisar");
                return false;
            }
        };
        $('#form_reintegrar_mat').submit();
    });
    $('#btn_guardar_reintegro2').click(function(e) {
        var radio_bodega = $("input[name='chk_bodega2']:checked").val();
        if (radio_bodega == "bodega") {
            $("input[type=number]").each(function(index, el) {
                if ($(this).val() == "" || $(this).val() == 0) {
                    alert("Campos Invalidos, Revisar");
                    el.preventDefault();
                }
            });
        } else if (radio_bodega == "maquina") {
            $("input[type=number]").each(function(index, el) {
                if ($(this).val() == "" || $(this).val() == 0) {
                    alert("Campos Invalidos, Revisar");
                    el.preventDefault();
                }
            });
            if ($("#sel_asig_maquina2").val() == "" || $("#ot_asignar2").val() == "") {
                alert("Campos Invalidos , Revisar");
                return false;
            }
        };
        $('#form_reintegrar_mat2').submit();
    });
    //--- HABILITAR FORMULARIO PARA TRASLADO A UNA MAQUINA O DIRECTAMENTE A BOD_PPAL ------------
    $('.chk_bodega').change(function(event) {
        var radio_bodega = $("input[name='chk_bodega']:checked").val();
        if (radio_bodega == "bodega") {
            console.log("Select Bodega");
            $(".in_traslado_mq").css({
                display: 'none'
            });
        } else if (radio_bodega == "maquina") {
            console.log("Select Maquina");
            $(".in_traslado_mq").css({
                display: 'block'
            });
        };
    });
    $('.chk_bodega2').change(function(event) {
        var radio_bodega = $("input[name='chk_bodega2']:checked").val();
        if (radio_bodega == "bodega") {
            console.log("Select Bodega");
            $(".in_traslado_mq2").css({
                display: 'none'
            });
        } else if (radio_bodega == "maquina") {
            console.log("Select Maquina");
            $(".in_traslado_mq2").css({
                display: 'block'
            });
        };
    });
    //------------ ENVIAR FORM DE FINALIZACION SIN REINTEGRO PRODUCCION------------
    ///////bombon
    $('#btn_finalizar_ot').click(function(event) {
        var input_codbar = $("#input_codbar").val();
        var codbar_item = $("#codbar_item").text();
        if ((!input_codbar) || (codbar_item == "Item")) {
            alert("Sin Datos Validos para realizar Registro.");
        } else {
            $.post('../ajax/fin_ot_send', {
                codbar_item: codbar_item,
                input_codbar: input_codbar
            }, function(data, textStatus, xhr) {});
        }
    });
    $('#btn_finalizar_ot').click(function(event) {
        var input_codbar = $("#input_codbar").val();
        var codbar_item = $("#codbar_item").text();
        var id = $("#id").val();
        if ((!input_codbar) || (codbar_item == "Item") || (!id)) {
            alert("Sin Datos Validos para realizar Registro.");
        } else {
            $('#form_reintegro_prod').attr({
                action: 'traslado_prod_sinReintegro',
                method: 'POST'
            });
            $('#form_reintegro_prod').submit();
        }
    });
    //--- RELLENADO DE CAMPOS PARA REINTEGRO DE MAT A ALMACEN BOD_PROD ------------
    $('.reintegrar_mat_modal').click(function(event) {
        var mat_id = $(this).data('mat_rein_id');
        var mat_descrip = $(this).data('mat_rein_desc');
        var stockm_cantidad = $(this).data('stock_rein_cant');
        var movimiento = $(this).data('movimiento');
        var codigo = $(this).data('codigo');
        var tipo = $(this).data('tipo');
        $('#head_material_r').empty();
        $('#head_material_r').append(mat_descrip);
        $('#input_matid_rein_alm').empty();
        $('#input_matid_rein_alm').val(mat_id);
        $('#text_mat_disp_alm').empty();
        $("#tipo").val(tipo);
        if ($("#tipo").val() == 1) {
            $('#text_mat_disp_alm').text(stockm_cantidad + " Kgs");
        } else {
            $('#text_mat_disp_alm').text(stockm_cantidad + " Uds");
        }
        $('#input_cantmat_rein_alm').empty();
        $('#input_cantmat_rein_alm').val(stockm_cantidad);
        $('#input_cant_rein_alm').val(stockm_cantidad);
        $('#movimiento').val(movimiento);
        $('#codigo').val(codigo);
    });
    //--- SUBMIT PARA TRASLADO DE BOD_PRODUCCION A BOD_PRINCIPAL(ALMACEN) ------------
    $('#btn_rein_alm').click(function(event) {
        $('#form_rein_alm').attr({
            action: 'sol_reintegro_alm',
            method: 'POST'
        });
        $('#form_rein_alm').submit();
    });
    $(document).on('click', '#btn-detalles-reintegros', function(event) {
        var movimiento = $(this).data('movimiento');
        var bandeja = $(this).data('bandeja');
        var material = $(this).data('material');
        var descripcion = $(this).data('descripcion');
        $.post('../ajax/get_detail_reintegro', {
            movimiento: movimiento
        }, function(data, textStatus, xhr) {
            $('#detalles-reintegros').empty();
            $('#detalles-reintegros').append(data);
            $("#material_rein_normal").val(material);
            $("#descripcion_rein_normal").val(descripcion);
            $("#movimiento_rein_normal").val(movimiento);
            if (bandeja != 0) {
                $.post('../ajax/get_detail_und_detail', {
                    bandeja: bandeja
                }, function(data, textStatus, xhr) {
                    $(".td_detail_und_detail").empty();
                    $(".td_detail_und_detail").append(data);
                });
            }
        });
    });
    $(document).on('click', '.abrir_ventana', function(event) {
        event.preventDefault();
        $('#detail_ot').modal('show');
    });
    //--- RELLENADO DE CAMPOS PARA REINTEGRO DE MAT A ALMACEN BOD_PROD ------------
    $('.btn_detail_unds').click(function(event) {
        var movimiento_id = $(this).data('movimiento_id');
        var bandeja = $(this).data('bandeja');
        $.post('../ajax/get_detail_und', {
            movimiento_id: movimiento_id,
            bandeja: bandeja
        }, function(data, textStatus, xhr) {
            $('.tb_detail_und').empty();
            $('.tb_detail_und').append(data);
            console.log(data);
            if (bandeja != 0) {
                $.post('../ajax/get_detail_und_detail', {
                    bandeja: bandeja
                }, function(data, textStatus, xhr) {
                    $(".td_detail_und_detail").empty();
                    $(".td_detail_und_detail").append(data);
                });
            }
        });
    });
    $(document).on('click', '#sincronizar_sqlserver', function(event) {
        $.ajax({
            url: '../ajax/sincronizar_sqlserver',
            type: 'post',
            beforeSend: function(objeto) {
                $('#sincronizar_sqlserver').attr('disabled', 'disabled');
                $('#sincronizar_sqlserver').text('SINCRONIZANDO CON SQL SERVER......');
                $('#sincronizar_sqlserver').removeClass('btn-success');
                $('#sincronizar_sqlserver').addClass('btn-primary');
            },
        }).done(function(data) {
            console.log(data);
            alert(data);
            $('#sincronizar_sqlserver').text('Sincronizar Datos');
            $('#sincronizar_sqlserver').removeClass('btn-primary');
            $('#sincronizar_sqlserver').addClass('btn-success');
            $('#sincronizar_sqlserver').removeAttr('disabled');
            location.reload();
        }).fail(function(data_error) {
            console.log(data_error);
        });
    });
});
$(document).on('click', '.edit_inv', function(event) {
    var id = $(this).data('id');
    var cantidad = $(this).parents('tr').find('td.cantidad').text();
    var check = $(this).parents('tr').find('input');
    if ($(check).prop('checked')) {
        var valbog = $(this).parents('tr').find('input').val();
    } else {
        var valbog = $(this).parents('tr').find('td.bodegaid').text();
    }
    $.post('../ajax/edit_invt', {
        id: id,
        cantidad: cantidad,
        valbog: valbog
    }, function(data, textStatus, xhr) {
        if (data == 1) {
            alert("Editado");
            location.reload();
        } else {
            alert("No se edito");
        }
    });
});

function reg_material() {
    $.ajax({
        url: '../ajax/reg_material',
        type: 'POST',
        data: $("#form_add_material").serialize(),
    }).done(function(data) {
        console.log(data);
    }).fail(function(error) {
        console.log(error);
    });
}
$(document).on('click', '.btn_alistar_prod_modal', function(event) {
    var res = confirm("¿Esta seguro de Realizar el traslado?");
    if (res) {
        var intup1 = $(this).data('sol_id');
        var material = $(this).data('material');
        var descripcion = $(this).data('descripcion');
        var imprevistos = $(this).data('imprevistos');
        var movimiento = $(this).data('movimiento');
        var tipo = $(this).data('tipo');
        $.ajax({
            url: '../ajax/alistar_prod',
            type: 'POST',
            data: {
                id: intup1,
                material: material,
                descripcion: descripcion,
                imprevistos: imprevistos,
                movimiento: movimiento,
                tipo: tipo
            },
        }).done(function(data) {
            if (data == 1) {
                alert("Traslado Exitoso!");
                location.reload();
            } else {
                alert("Error en la petición");
            }
        }).fail(function(error) {
            console.log(error);
        });
    } else {}
});
$(document).on('click', '.btn_detail_al', function(event) {
    var movimiento = $(this).data('sol_id');
    var cantidad = $(this).data('cant');
    var material = $(this).data('material');
    var valor = 0.00;
    $("#cant_disponible3").text(cantidad.toLocaleString());
    $("#cant_uso3").text(cantidad);
    $.ajax({
        url: '../ajax/alistar_detalles',
        type: 'POST',
        data: {
            movimiento: movimiento,
            material: material
        },
    }).done(function(data) {
        $("#alistar_detalles").empty();
        $("#alistar_detalles").append(data);
        $(".tipo").val(1);
        $(".cantidad").each(function() {
            valor += parseFloat($(this).text());
        });
        $("#cant_preparada").text(valor.toLocaleString());
    }).fail(function(error) {
        console.log(error);
    });
});
$(document).on('click', '.btn_detail_al2', function(event) {
    var movimiento = $(this).data('sol_id');
    var cantidad = $(this).data('cant');
    var material = $(this).data('material');
    var valor = 0.00;
    $("#cant_disponible4").text(cantidad.toLocaleString());
    $("#cant_uso4").text(cantidad);
    $.ajax({
        url: '../ajax/alistar_detalles2',
        type: 'POST',
        data: {
            movimiento: movimiento,
            material: material
        },
    }).done(function(data) {
        $("#alistar_detalles2").empty();
        $("#alistar_detalles2").append(data);
        $(".tipo").val(2);
        $(".cantidad2").each(function() {
            valor += parseFloat($(this).text());
        });
        $("#cant_preparada2").text(valor.toLocaleString());
    }).fail(function(error) {
        console.log(error);
    });
});
$(document).on('click', '.edit_alist', function(event) {
    var id = $(this).data('id');
    var cantidad = $(this).parents('tr').find('td.cantidad').text();
    var tipo = $(".tipo").val();
    var material = $(this).data('material');
    var movimiento = $(this).data('movimiento');
    var stock = $("#cant_uso3").text();
    var stock_nuevo = $("#cant_nueva").text();
    $.post('../ajax/edit_alist', {
        id: id,
        cantidad: cantidad,
        tipo: tipo,
        movimiento: movimiento,
        material: material,
        stock: stock,
        stock_nuevo: stock_nuevo
    }, function(data, textStatus, xhr) {
        if (data == 1) {
            alert("Editado");
            $("#cant_nueva").text('0');
            location.reload();
        } else {
            alert("No se edito");
        }
    })
});
$(document).on('click', '.edit_alist2', function(event) {
    var id = $(this).data('id');
    var cantidad = $(this).parents('tr').find('td.cantidad2').text();
    var tipo = $(".tipo").val();
    var material = $(this).data('material');
    var movimiento = $(this).data('movimiento');
    var stock = $("#cant_uso4").text();
    var stock_nuevo = $("#cant_nueva2").text();
    $.post('../ajax/edit_alist', {
        id: id,
        cantidad: cantidad,
        tipo: tipo,
        movimiento: movimiento,
        material: material,
        stock: stock,
        stock_nuevo: stock_nuevo
    }, function(data, textStatus, xhr) {
        if (data == 1) {
            alert("Editado");
            $("#cant_nueva2").text('0');
            location.reload();
        } else {
            alert("No se edito");
        }
    })
});
$(document).on('keyup', '.cantidad', function(event) {
    var cantidad = $(this).text().toLocaleString();
    var valor = 0;
    var valor_final = 0;
    var calculo_final = 0;
    $(this).parents('tr').find('td.subtotal').text(cantidad);
    $(".cantidad").each(function() {
        valor += parseFloat($(this).text());
    });
    valor_final = valor - $("#cant_preparada").text();
    $("#cant_nueva").text(valor_final.toFixed(2));
    var v1 = parseFloat($("#cant_preparada").text());
    var v2 = parseFloat($("#cant_nueva").text());
    var resultado = parseFloat(v1 - v2);
    var calculo = resultado + parseFloat($("#cant_disponible3").text());
    calculo_final = calculo - $("#cant_preparada").text();
    $("#cant_uso3").text(calculo_final.toLocaleString());
    // $("#cant_disponible3").text(calculo_final.toLocaleString());
    if (calculo_final >= 0) {
        $("#alistar_detalles button").each(function() {
            $(this).attr('disabled', false);
        });
    } else {
        $("#alistar_detalles button").each(function() {
            $(this).attr('disabled', true);
        });
    }
});
$(document).on('keyup', '.cantidad2', function(event) {
    var cantidad = $(this).text().toLocaleString();
    var valor = 0;
    var valor_final = 0;
    var calculo_final = 0;
    $(this).parents('tr').find('td.subtotal2').text(cantidad);
    $(".cantidad2").each(function() {
        valor += parseFloat($(this).text());
    });
    valor_final = valor - $("#cant_preparada2").text();
    $("#cant_nueva2").text(valor_final.toFixed(2));
    var v1 = parseFloat($("#cant_preparada2").text());
    var v2 = parseFloat($("#cant_nueva2").text());
    var resultado = parseFloat(v1 - v2);
    var calculo = resultado + parseFloat($("#cant_disponible4").text());
    calculo_final = calculo - $("#cant_preparada2").text();
    $("#cant_uso4").text(calculo_final.toLocaleString());
    // $("#cant_disponible4").text(calculo_final.toLocaleString());
    if (calculo_final >= 0) {
        $("#alistar_detalles2 button").each(function() {
            $(this).attr('disabled', false);
        });
    } else {
        $("#alistar_detalles2 button").each(function() {
            $(this).attr('disabled', true);
        });
    }
});
$(document).on('keyup', '.table_rein input', function() {
    var valor = parseFloat($(this).val());
    var valor_maximo = $(this).parents('tr').find('.cantidad').text();
    if (valor > valor_maximo) {
        $(this).val(valor_maximo);
    } else {}
});

function Search() {
    var tabla = document.getElementById('dataTables-examp');
    var busqueda = document.getElementById('search').value.toLowerCase();
    var cellsOfRow = "";
    var found = false;
    var compareWith = "";
    for (var i = 1; i < tabla.rows.length; i++) {
        cellsOfRow = tabla.rows[i].getElementsByTagName('td');
        found = false;
        for (var j = 0; j < cellsOfRow.length; j++) {
            compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (busqueda.length == 0 || (compareWith.indexOf(busqueda) > -1)) {
                found = true;
            }
        }
        if (found) {
            tabla.rows[i].style.display = '';
        } else {
            tabla.rows[i].style.display = 'none';
        }
    }
}
$(document).on('change', '.selecionar', function() {
    if ($(this).prop('checked')) {
        $(this).parents('tr').find('input').removeAttr('disabled');
        $(this).attr('name', 'selecionado');
        // $(this).parents('table').find("input[name='noselecionado']").attr('disabled', true);
    } else {
        $(this).parents('tr').find('input[type=hidden]').attr('disabled', true);
        $(this).parents('tr').find('input[type=number]').attr('disabled', true);
        $(this).attr('name', 'noselecionado');
        // $(this).parents('table').find("input[name='noselecionado']").attr('disabled', false);
    }
});

function reg_codigo() {
    var formData = new FormData($("#form_add_codigo")[0]);
    $.ajax({
        url: '../ajax/reg_codigo',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        cache: false,
    }).done(function(data) {
        console.log(data);
        if (data != 0) {
            alert("Guardado con exito");
            location.reload();
        } else {
            alert("No Guardado Correctamente")
        }
    }).fail(function(error) {
        console.log(error);
    });
}
$(document).on('click', '.delete_mat ', function() {
    var res = confirm("¿Esta seguro de Eliminar el Codigo?");
    if (res) {
        var id = $(this).data('id');
        $.ajax({
            url: '../ajax/delete_codigo',
            type: 'POST',
            data: {
                id: id
            },
        }).done(function(data) {
            console.log(data);
            if (data == 2) {
                alert("Eliminado con exito");
                location.reload();
            } else {
                alert("No Eliminado Correctamente")
            }
        }).fail(function(error) {
            console.log(error);
        });
    } else {}
});
$(document).on('click', '.edit_mat ', function() {
    var cantidad = $(this).parents('tr').find('.cantidad').text();
    var id = $(this).data('id');
    $.ajax({
        url: '../ajax/edit_codigo',
        type: 'POST',
        data: {
            cantidad: cantidad,
            id: id
        },
    }).done(function(data) {
        console.log(data);
        if (data == 2) {
            alert("Editado con exito");
        } else {
            alert("No Editado Correctamente")
        }
    }).fail(function(error) {
        console.log(error);
    });
});
$(document).on('click', '#btn_rein_alm_normal ', function() {
    if ($(".observacion_ralm").val() == "") {
        alert("Debes escribir una Descripción");
    } else if ($('#detalles-reintegros input[type=checkbox]:checked').length === 0) {
        alert("Debes elegir al menos 1 Presentación");
    } else {
        $('#form_rein_mat_normal').submit();
    }
});

function existenciaMaterial(object) {
    $.ajax({
        url: '../ajax/existenciaMaterial',
        type: 'POST',
        // dataType: '',
        data: {
            material: $(object).val(),
        },
    }).done(function(data) {
        //console.log(data);

        if (data == "ErrorMat") {
            // $("#material").val('');
            $(object).parents('tr').find('input.material').val('');
            $(object).parents('tr').find('input.codigo').val('');
            $(".btn-save").prop('disabled', 'disabled');
            $(".help-block").removeClass('hidden');
        } else {
            // $("#material").val(data);
            console.log(data);
            $(object).parents('tr').find('input.material').val(data);
            $(".btn-save").removeAttr('disabled');
            $(".help-block").addClass('hidden');
        }
        changeMaterial(object);
    }).fail(function(err) {
        console.log(err);
    });
}

function addcodigos() {
    var tr = '<tr class="item"><td class="hidden"><input type="number" name="material[]" class="form-control material" id="material" value=""></td>';
    tr = tr + '<td><input type="number" class="form-control material1" name="material1[]" id="material1" onkeyup="existenciaMaterial(this)"></td>';
    tr = tr + '<td><select name="presentacion[]" id="presentacion" onchange="changePresentacion()" class="form-control presentacion" required><option value="" selected disabled>Elegir presentacion</option><option value="Estiba">Estiba</option><option value="Chipa">Chipa</option><option value="Rollo">Rollo</option><option value="Carreto">Carreto</option></select></td>';
    tr = tr + '<td><input type="number" class="form-control cantidad" name="cantidad[]" id="cantidad" min="0" step=".01" onkeyup="changeCantidad(this)" required></td>';
    tr = tr + '<td><input type="text" name="codigo[]" class="form-control codigo" id="codigo" readonly required></td>';
    tr = tr + '<td><button type="button" class="btn btn-sm btn-default  remove-item">Remover</button></td></tr>';
    $("#TrCodigos").append(tr);
    $('.remove-item').off().click(function(e) {
        $(this).parent('td').parent('tr').remove(); //En accion elimino el Producto de la Tabla
        if ($('#TrCodigos tr.item').length == 0) $('#TrCodigos .no-item').slideDown(300);
    });
}